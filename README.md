
#Hightrades JavaScript charting library (htc.js)
Version 0.0.1-alpha

## Overview

The Hightrades JavaScript library produces interactive, zoomable charts of time series. It's mainly developed to visualize technical anaylsis and price patterns.

You can also check out some if example pages.

 * Candlestick, bar and flag example: [Demo](http://htc-9660.onmodulus.net/examples/flags.html)

 * Bollinger Band and MACD: [Demo](http://htc-9660.onmodulus.net/examples/flags.html)

 * Candlestick chart with range slider [Demo](http://htc-9660.onmodulus.net/examples/hover.html).

 * Multiple yaxis and interactive tooltips: [Demo](http://htc-9660.onmodulus.net/examples/range.html)


 ---

# Current development focus

- Getting documentation up.
- Unifying common API functions between charts.
- Bug fixes that come up.

---

# Installation Instructions

`d3.v3.js` is a dependency of `htc.js`. Be sure to include in in your project, then:
Add a script tag to include `htc.js` OR `htc.min.js` in your project.
Also add a link to the `htc.css` file.


## Supported Browsers
NVD3 runs best on WebKit based browsers.

* **Google Chrome: latest version (preferred)**
* **Opera 15+ (preferred)**
* Safari: latest version
* Firefox: latest version
* Internet Explorer: 9 and 10




