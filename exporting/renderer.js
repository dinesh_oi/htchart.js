(function() {
    "use strict";

    var webpage  = require('webpage')
      , base64   = require('../lib/base64.js')
      , Renderer = function(options) { this.init(options); };

    var fs = require('fs');
    var chartCss =  fs.read('../src/htc.css');

    Renderer.prototype.init = function(options) {
        this.options = options;
        this.id      = Date.now() + (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1));
        this.page    = webpage.create();

        this.setOnRenderCallback(function() {});

        this.page.onCallback = this.onPhantomCallback.bind(this);
        this.page.onConsoleMessage = function(msg, lineNum, sourceId) {
            console.log('CONSOLE: ' + (typeof msg == 'string' ? msg : JSON.stringify(msg)));
        };
    };

    Renderer.prototype.setConfig = function(config) {
        this.config = config;
    };

    Renderer.prototype.setResponse = function(res) {
        this.response = res;
    };

    Renderer.prototype.loadPage = function() {
        this.page.open('about:blank', this.onPageReady.bind(this));
    };

    Renderer.prototype.setOnRenderCallback = function(cb) {
        this.onRenderCompleteCallback = cb;
    };

    Renderer.prototype.onRenderComplete = function(msg) {

        if(msg.viewportSize)
            this.page.viewportSize = msg.viewportSize;

        var data    = base64.decode(this.page.renderBase64('png'))
          , decoded = ''
          , j       = data.length
          , i;

        for (i = 0; i < j; i++) {
            decoded = decoded + String.fromCharCode(data[i]);
        }

        this.onRenderCompleteCallback(
            this.response,
            decoded
        );

        this.page.close();
    };

    Renderer.prototype.onPhantomCallback = function(msg) {
        if (msg.id != this.id) {
            return;
        }

        if (!this[msg.callback]) {
            return;
        }

        this[msg.callback](msg);
    };

    Renderer.prototype.onPageReady = function() {
        this.page.injectJs(this.config.scripts['jquery']);
        this.page.injectJs(this.config.scripts['d3']);
        this.page.injectJs(this.config.scripts['htc']);
        this.page.injectJs('../lib/bind-shim.js');
        this.page.injectJs('charter.js');

        var width  = this.options.chart.width || 580;
        var height = this.options.chart.height;

        if(!height){
            height = 0;
            var lastY = this.options.yAxis[this.options.yAxis.length-1];
            height = (lastY.top || 0) + lastY.height;
        }

        this.page.viewportSize = { width: width, height: height };
        console.log("width:", width, "height: ", height);

        var createChart = function(options, id, _css, cb) {
            var charter = new Charter();
            charter.setId(id);
            charter.setOptions(options);

            if(_css)
                charter.setCss(_css);

            var info = charter.render();

            return { 'height' : info.height, 'width'  : info.width };

        };

        this.page.evaluate(createChart, this.options, this.id, chartCss);
    };

    Renderer.prototype.render = function() {
        this.loadPage();
    };

    module.exports = Renderer;

})();