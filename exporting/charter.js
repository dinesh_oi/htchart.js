(function($, window) {
    "use strict";

    var Charter = function() { this.init(); };

    $.extend(Charter.prototype, {
        init: function() {
            $(document.body).css('margin', '0px');
            this.constr    = 'Chart';
            this.container = $('<div />').attr('id', 'chart-container').appendTo(document.body);
        },

        setId: function(id) {
            this.id = id;
        },

        setCss: function(css){
           $('<style />').attr('type', 'text/css')
                .html(css).appendTo(document.head);
        },

        pingback: function(callback, msg) {
            msg = msg || {};
            msg.callback = callback;
            msg.id = this.id;
            window.callPhantom(msg);
        },

        runScript: function(code) {
            $('<script />')
                .attr('type', 'text/javascript')
                .html(code)
                .appendTo(document.head);
        },

        setOptions: function(options) {
            if (!options.chart) {
                options.chart = {};
            }

            // DOM-target
            options.element = this.container.get(0);

            // Width/height
            options.chart.width  = (options.exporting && options.exporting.sourceWidth)  || options.chart.width  || 800;
            options.chart.height = (options.exporting && options.exporting.sourceHeight) || options.chart.height || 400;

            // disable navigator
            options.navigator.enabled = false;

            // disable navigation & zoom buttons
            options.navigation.buttonOptions.enabled = false;
            options.rangeSelector.enabled = false;

            this.options = options;
        },

        setConstructor: function(constr) {
            this.constr = constr;
        },

        onRenderComplete: function(msg) {
            this.pingback('onRenderComplete', msg);
        },

        render: function() {
            var context = this, svg;
            this.chart = new htc.Graph(this.options)

            this.chart.render();

            // Remove stroke-opacity paths, used by mouse-trackers,
            // they turn up as as fully opaque in the PDF
            var node, opacity;

            $('*[stroke-opacity]').each(function() {
                node = this;
                opacity = node.attr('stroke-opacity');
                node.attr('stroke-opacity', null);
                node.attr('opacity', opacity);
            });

            svg = $('svg');

            this.onRenderComplete();

            return {
                html  : this.container.parent().html(),
                width : svg.attr('width'),
                height: svg.attr('height')
            };
        }

    });

    window.Charter = Charter;

})(jQuery, window);
