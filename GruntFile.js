module.exports = function(grunt) {

    //Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        nodemon: {
          dev: {
            options: {
              logConcurrentOutput: true,
              file: 'server.js',
              args: ['3000'],
              nodeArgs: [],
              ignoredFiles: ['README.md', 'node_modules/**'],
              watchedExtensions: ['js'],
              watchedFolders: ['src'],
              delayTime: 1,
              legacyWatch: true,
              env: {
                PORT: '8181'
              },
              cwd: __dirname
            }
          },
          exec: {
            options: {
              exec: 'less'
            }
          }
        },
        concat: {
            options: {
                separator: '',
                logConcurrentOutput: true
            },
            dist: {
                src: [
                        // 'lib/d3.v3.js',
                        'src/intro.js',
                        'src/core.js',
                        'src/class.js',
                        'src/weekdays.js',
                        'src/utils.js',
                        'src/color.js',
                        'src/xaxis.js',
                        'src/yaxis.js',
                        'src/renderer.js',
                        'src/series.js',
                        'src/line.js',
                        'src/bar.js',
                        'src/marker.js',
                        'src/stack.js',
                        'src/candlestick.js',
                        'src/legend.js',
                        'src/brush.js',
                        'src/tooltip.js',
                        'src/zoom.js',
                        'src/interactive-layer.js',
                        'src/graph.js',
                        'src/coordinate-graph.js',
                        'src/talib.js',
                        'src/outro.js'
                     ],
                dest: 'htc.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            js: {
                files: {
                    'htc.min.js': ['htc.js']
                }
            }
        },
        jshint: {
            foo: {
                src: "src/**/*.js"
            },
            options: {
                jshintrc: '.jshintrc'
            }
        },
        watch: {
            js: {
                files: ["src/**/*.js"],
                tasks: ['concat']
            }
        },
        concurrent: {
          dev: {
            options: {
              logConcurrentOutput: true
            },
            tasks: ['watch', 'nodemon:dev']
          }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['concat', 'concurrent:dev']);
    grunt.registerTask('build', ['concat']);
    // grunt.registerTask('build', ['concat', 'uglify']);
    grunt.registerTask('lint', ['jshint']);
};
