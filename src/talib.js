
(function(htc){

    var statAnalysis = {
        // Calculate Simple Moving Average
        sma: function (data, smaPeriod) {
            var currSum = 0;
            var sma = [];
            for (var i = 0; i < data.length; i++) {
                currSum = 0;
                var k = 0;
                var sumArr = [];
                for (var j = i; j < data.length; j++) {
                    sumArr.push(data[i]);
                    if (k < smaPeriod) {
                        currSum += data[j];
                        k++;
                    }
                }
                var smaValue = currSum / smaPeriod;
                if (sumArr.length >= smaPeriod) {
                    sma.push(smaValue);
                }
            }
            return sma;
        },

        ema: function (data, period) {
            /*SMA: 10 period sum / 10
             Multiplier: (2 / (Time periods + 1) ) = (2 / (10 + 1) ) = 0.1818 (18.18%)
             EMA: {Close - EMA(previous day)} x multiplier + EMA(previous day). */
            var sma = [];
            var multiplier = [];
            var ema = [];
            var currSum = 0;

            var periodData = data.slice(period - 1, data.length);
            for (var i = 0; i < data.length; i++) {
                currSum = 0;
                var k = 0;
                var sumArr = [];
                for (var j = i; j < data.length; j++) {
                    sumArr.push(data[i]);

                    if (k < period) {

                        currSum += data[j];
                        k++;
                    }
                }
                var smaValue = currSum / period
                if (sumArr.length >= period) {
                    sma.push(smaValue);
                }
            }

            for (var i = 1; i < sma.length; i++) {
                multiplier.push(2 / (period + 1));
            }

            for (var i = 0; i < sma.length; i++) {
                if (i == 0) {
                    ema.push(sma[i]);
                } else {
                    ema.push(multiplier[i - 1] * (periodData[i] - ema[i - 1]) + ema[i - 1]);
                }
            }
            return ema;
        },

        // Calculate stdDev for BBands
        stdsma: function(data, sma, smaPeriod) {
            var startPoint = 0,
                prevData = [],
                stdDev = [],
                smaPeriod = smaPeriod;

            for (var i = smaPeriod; i <= data.length; i++) {
                prevData = data.slice(startPoint, i);
                var currSum = 0;
                for (var k = 0; k < prevData.length; k++) {
                    currSum = currSum + Math.pow(prevData[k] - sma[i - smaPeriod], 2);
                }
                stdDev.push(Math.sqrt(currSum / smaPeriod));
                startPoint++;
            }
            return stdDev;
        },

        // Calculate Bollinger bands data
        bbands: function(data, smaPeriod, std) {
            var upperBand = [],
                lowerBand = [],
                bbData = [],
                middleBand = [],
                smaPeriod = smaPeriod + 1,
                sma = this.sma(data, smaPeriod),
                stdDev = this.stdsma(data, sma, smaPeriod),
                stdFactor = std || 2;
            //console.log(sma);
            for (var i = 0; i < sma.length; i++) {
                upperBand.push(sma[i] + (stdDev[i] * stdFactor));
                middleBand = sma;
                lowerBand.push(sma[i] - (stdDev[i] * stdFactor));

            }
            bbData.push(upperBand, middleBand, lowerBand);
            return bbData;
        },

        wma: function(data, period) {
            var wma = [],
                currSum = 0,
                periodSum = 0,
                periodData = data.slice(period - 1, data.length);

            for (var i = 0; i < data.length; i++) {
                currSum = 0;
                var k = 0;
                var sumArr = [];
                for (var j = i; j < data.length; j++) {
                    sumArr.push(data[i]);
                    if (k < period) {
                        currSum += data[j] * (k + 1);
                        k++;
                    }
                }
                var wmaValue = currSum / ((period * (period + 1)) / 2);
                if (sumArr.length >= period) {
                    wma.push(wmaValue);
                }
            }
            return wma;
        },

        macd: function(data, fastPeriod, slowPeriod, signalPeriod) {
            //MACD Line: (12-day EMA - 26-day EMA)
            //Signal Line: 9-day EMA of MACD Line
            //MACD Histogram: MACD Line - Signal Line
            var macd = [],
                normalizedEmaFast = [],
                normalizedEmaSlow = [],
                normalizedMacdLine = [],
                emaFast = [],
                emaSlow = [],
                macdLine = [],
                signal = [],
                macdHistogram = [],
                emaFast = this.ema(data, fastPeriod), // count 47
                emaSlow = this.ema(data, slowPeriod), // count 33
                normalizedEmaFast = emaFast.slice(slowPeriod-fastPeriod, emaFast.length);

            for (var i = 0; i < normalizedEmaFast.length; i++) {
                macdLine.push(normalizedEmaFast[i] - emaSlow[i]);
            }
            signal = this.ema(macdLine, signalPeriod);
            normalizedMacdLine = macdLine.slice(signalPeriod-1, macdLine.length);
            for (var i = 0; i < normalizedMacdLine.length; i++) {
                macdHistogram.push(normalizedMacdLine[i] - signal[i]);
            }
            macd.push(macdHistogram, normalizedMacdLine, signal);
            return macd;
        },
        //<TICKER>,<PER>,<DATE>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>
        isFirstUptrend: function(data, checkTrendPointCount) {
            var upper = 0,
                lower = 0;
            for (var i = 0; i < checkTrendPointCount; i++) {
                if (data[i].close > data[i + 1].close) {
                    lower++;
                } else {
                    upper++;
                }
            }
            return upper > lower;
        },

        psar: function(data) {
            //console.log(data);
            var defaultInitAcceleration = 0.02,

            // Max value of acceleration
                defaultMaxAcceleration = 0.2,

            // Number of points to check before deside trend direction
                checkTrendPointCount = 5,

                initAcceleration = defaultInitAcceleration,
                uptrend = this.isFirstUptrend(data, checkTrendPointCount),
                prevSar = uptrend ? data[0].low : data[0].high,
                accelaration = initAcceleration,
                extremum = uptrend ? data[0].high : data[0].low,
                result = [];

            for (var i = 0; i < data.length; i++) {

                // extremum = highest period High for uptrend
                if (uptrend && (extremum < data[i].high)) {
                    extremum = data[i].high;
                    accelaration = _.min([defaultMaxAcceleration, accelaration + initAcceleration]);
                }

                // extremum = lowest period Low for downtrend
                else if (!uptrend && (extremum > data[i].low)) {
                    extremum = data[i].low;
                    accelaration = _.min([defaultMaxAcceleration, accelaration + initAcceleration]);
                }

                var probableSar = prevSar + accelaration * (extremum - prevSar);

                // trend switch
                if (data[i].low < probableSar && probableSar < data[i].high) {
                    uptrend = !uptrend;
                    accelaration = initAcceleration;
                    probableSar = extremum;
                    extremum = uptrend ? data[i].high : data[i].low;
                }

                result.push(probableSar);
                prevSar = probableSar;
            }
            return result;
        },


        // REFACTOR !!!
        calcBuyingPressure: function(data) {
            var buyPressure = [],
                prevCloseLowArr = [];
            for (var i = 1; i < data.length; i++) {
                prevCloseLowArr.push(data[i - 1].close);
                prevCloseLowArr.push(data[i].low);
                buyPressure.push(data[i].close - _.min(prevCloseLowArr));
                prevCloseLowArr = [];
            }
            return buyPressure;
        },

        atr: function(data) {
            var prevCloseHighArr = [],
                prevCloseLowArr = [],
                trueRange = [];

            for (var i = 1; i < data.length; i++) {
                prevCloseHighArr.push(data[i - 1].close);
                prevCloseHighArr.push(data[i].high);
                prevCloseLowArr.push(data[i - 1].close);
                prevCloseLowArr.push(data[i].low);
                trueRange.push(_.max(prevCloseHighArr) - _.min(prevCloseLowArr));
                prevCloseHighArr = [];
                prevCloseLowArr = [];
            }
            return trueRange;
        },

        avgForPeriod: function(data, period) {
            var startPoint = 0;
            var prevBuyPressure = [];
            var prevTrueRange = [];
            var buyingPressure = this.calcBuyingPressure(data);
            var trueRange = this.calcTrueRange(data);
            var avgForPeriod = [];

            for (var i = period; i < data.length; i++) {
                prevBuyPressure = buyingPressure.slice(startPoint, i);
                prevTrueRange = trueRange.slice(startPoint, i);
                var currSum = 0;
                var buyPressureSum = 0;
                var trueRangeSum = 0;
                for (var k = 0; k < prevBuyPressure.length; k++) {
                    buyPressureSum = buyPressureSum + prevBuyPressure[k];
                    trueRangeSum = trueRangeSum + prevTrueRange[k];
                }
                avgForPeriod.push(buyPressureSum / trueRangeSum);
                startPoint++;
            }
            return avgForPeriod;
        },

        ultimateOscillator: function(data) {


            var ultOsc = [],
                avgSevenPeriod = this.calcAvgForPeriod(data, 7),
                avgFourteenPeriod = this.calcAvgForPeriod(data, 14),
                avgTwentyEightPeriod = this.calcAvgForPeriod(data, 28);



            for (var i = 0; i < avgTwentyEightPeriod.length; i++) {
                ultOsc.push((100 * ( (4 * avgSevenPeriod[21 + i]) + (2 * avgFourteenPeriod[14 + i]) + avgTwentyEightPeriod[i] )) / (4 + 2 + 1));
            }

            return [ultOsc];
        },

    };

    htc.talib = statAnalysis;
    return statAnalysis;

})(htc);