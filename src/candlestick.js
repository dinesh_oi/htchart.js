htc.namespace('htc.Renderer.Candlestick')

htc.Renderer.Candlestick = htc.Class.create(htc.Renderer, {

    name: 'candlestick',

    render: function(args){
        var series = args.series[0],
            data             = series.stack || series.data,
            graph            = this.graph || args.graph,
            vis              = args.vis || graph.vis,
            X                = graph.x,
            Y                = graph.y,
            _                = series,
            margin           = this.padding,
            whiteCandleColor = '#6ba583' || series.palette.color(),
            whiteBorderColor = '#225437',
            blackCandleColor = '#d75442' || series.palette.color(),
            blackBorderColor = '#5b1a13'
            ;

        var p = data[0];
        var barWidth = 0.5*(graph.width-margin.left-margin.right)/data.length;

        this.bars = vis.selectAll('rect').data(data, function(d){ return d.x; });

        var mergeNodes = function(nodes){
           return nodes
                .attr("x", function(d){
                    return  X(d.x) - barWidth/2;
                })
                .attr("y", function(d){
                    return Y(Math.max(d.open, d.close))
                })
                .attr("width", function(d){
                    return barWidth;
                })
                .attr("height", function(d){
                    return Y(Math.min(d.open, d.close)) - Y(Math.max(d.open, d.close));
                })
        }

        mergeNodes(this.bars)

        mergeNodes(this.bars.enter().append('svg:rect'))
            .attr("fill",function(d) {
                return d.open > d.close ? whiteCandleColor : blackCandleColor
            })
            .attr("stroke", function(d) {
                return d.open > d.close ? whiteBorderColor : blackBorderColor
            })
            .on("mouseover", function(){
                d3.select(this).classed('hover', true);
            })
            .on("mouseout", function(){
                d3.select(this).classed('hover', false);
            });

        this.bars.exit().remove();

        this.stems = vis.selectAll("line.stem").data(data, function(d){ return d.x })

        var mergeStems = function(nodes){
            return nodes
                        .attr("x1", function(d) { return X(d.x); })
                        .attr("x2", function(d) { return X(d.x); })
                        .attr("y1", function(d) { return Y(d.high) })
                        .attr("y2", function(d) { return Y(d.low) })
        }

        // Update stems
        mergeStems(this.stems)

        // ENTER stems
        mergeStems(this.stems.enter().append("svg:line").attr("class", "stem"))
          .attr("stroke", function(d){
            return d.open > d.close ? whiteCandleColor : blackCandleColor
        });

        this.stems.exit().remove();

    }
});
