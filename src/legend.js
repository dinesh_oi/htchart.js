htc.namespace('htc.Legend');

htc.Legend = function(args) {
    var LABEL_GAP = 2;

    var _legend     = {},
        graph       = args.graph,
        _parent     = args.parent,
        _data       = args.data,
        _x          = 0,
        _y          = 0,
        width       = 960,
        height      = 20,
        _itemHeight = 12,
        _gap        = 5,
        _margin     = { top: -20, bottom: 0, right: 0, left: 0 },
        align       = false,
        rightAlign  = false;

    var _g;

    _legend.parent = function (p) {
        if (!arguments.length) return _parent;
        _parent = p;
        return _legend;
    };

    _legend.render = function () {

        width       = graph.width  || 960;
        height      = graph.height || 20;

        _g = _parent.append("g")
            .attr("class", "htc-legend")
            .attr("transform", "translate(" + _x + "," + _y + ")");

        var items = _g.selectAll('g.htc-legend-item').data(_data);

        var itemEnter = items.enter()
                .append("g")
                .attr("class", "htc-legend-item")
                .attr("transform", function (d, i) {
                    return "translate(0," + i * legendItemHeight() + ")";
                })
                .on("mouseover", function(d){
                        graph.legendHighlight(d);
                })
                .on("mouseout", function (d) {
                        graph.legendReset(d);
                });

        itemEnter.append("rect")
            .attr("width", _itemHeight)
            .attr("height", _itemHeight)
            .attr("fill", function(d){return d.color;});

        itemEnter.append("text")
            .text(function(d){return d.name;})
            .attr("x", _itemHeight + LABEL_GAP)
            .attr("y", function(){return _itemHeight / 2 + (this.clientHeight?this.clientHeight:13) / 2 - 2;});

        items.exit().remove();

        if(align){

        } else {
            var ypos = 5,  newxpos = 5, maxwidth = 0, xpos;

            items
                .attr('transform', function(d, i) {
                  var length = d3.select(this).select('text').node().getComputedTextLength() + 28;
                  xpos = newxpos;

                  if (width < _margin.left + _margin.right + xpos + length) {
                    newxpos = xpos = 5;
                    ypos += 20;
                  }

                  newxpos += length;
                  if (newxpos > maxwidth) maxwidth = newxpos;

                  return 'translate(' + xpos + ',' + ypos + ')';
                });

            //position legend as far right as possible within the total width
            _g.attr('transform', 'translate(' + (width - _margin.right - maxwidth) + ',' + _margin.top + ')');

            height = _margin.top + _margin.bottom + ypos + 15;
        }
    };

    function legendItemHeight() {
        return _gap + _itemHeight;
    }

    /**
    #### .x([value])
    Set or get x coordinate for legend widget. Default value: 0.
    **/
    _legend.x = function (x) {
        if (!arguments.length) return _x;
        _x = x;
        return _legend;
    };

    /**
    #### .y([value])
    Set or get y coordinate for legend widget. Default value: 0.
    **/
    _legend.y = function (y) {
        if (!arguments.length) return _y;
        _y = y;
        return _legend;
    };

    /**
    #### .gap([value])
    Set or get gap between legend items. Default value: 5.
    **/
    _legend.gap = function (gap) {
        if (!arguments.length) return _gap;
        _gap = gap;
        return _legend;
    };

    /**
    #### .itemHeight([value])
    Set or get legend item height. Default value: 12.
    **/
    _legend.itemHeight = function (h) {
        if (!arguments.length) return _itemHeight;
        _itemHeight = h;
        return _legend;
    };

    graph.onUpdate("legend-1", _legend.render);

    return _legend;
};
