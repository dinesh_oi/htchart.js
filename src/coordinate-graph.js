htc.namespace('htc.Graph.XY');

htc.Graph.XY = function(args) {
    if (!args.element) throw "htc.Graph needs a reference to an element";

    this.element = args.element;
    this.series  = args.series;
    this.name = args.name;

    this.defaults = {
        interpolation: 'cardinal',
        offset: 'zero',
        min: 'auto',
        max: undefined,
        preserve: false,
        margin: { left: 0, right: 0, top: 0, bottom: 0},
    };

    htc.keys(this.defaults).forEach( function(k) {
        this[k] = args[k] || this.defaults[k];
    }, this );

    this.window = {};

    this.updateCallbacks = {};

    this.mapDataX = args.mapDataX || function(d){
        if(d instanceof Date)
            return htc.discreteTime ? d : htc.weekday(d);
        else if(d instanceof Object && (d.x instanceof Date))
            return htc.discreteTime ? d.x : htc.weekday(d.x);
        else if(d instanceof Object && (d.time instanceof Date))
            return htc.discreteTime ? d.time : htc.weekday(d.time);
        else
            return d;
    };

    var self = this;

    var showXAxis      = true,
        showYAxis      = true,
        _showBrush     = args.showBrush || false,
        _showLegend    = true,
        _showTooltip   = true,
        _showZoom      = true,
        _legend_margin = 60;

    self.translateX    = 0;
    self.translateY    = 0;

    var offUpdate = function(id){
        delete self.updateCallbacks[id];
    }


    this.initialize = function(args) {

        this.validateSeries(args.series);
        this.series.active = function() { return self.series.filter( function(s) { return !s.disabled } ) };

        this.setSize(args);

        var wr = this.margin.left + this.margin.right,
            hr = this.margin.top  + this.margin.bottom;

        this.vis = d3.select(this.element)
            .attr('width', this.width)
            .attr('height', this.height);

        for (var name in htc.Renderer) {
            if (!name || !htc.Renderer.hasOwnProperty(name)) continue;
            var r = htc.Renderer[name];
            if (!r || !r.prototype || !r.prototype.render) continue;
            self.registerRenderer(new r( { graph: self } ));
        }

       this.xAxis = new htc.Axis.X(this, args.xAxis);
       this.yAxis = new htc.Axis.Y(this, args.yAxis);

       if(_showLegend)
           this.legendables = new htc.Legend({
                graph: this,
                parent: this.vis,
                data: args.series.filter(function(d){
                        // filter the showable legends which are markers or tooltips
                        return ['marker', 'tooltip'].indexOf(d.renderer) < 0;
                    }).map(function(d){
                        return {
                            name: (d.name || d.key).toUpperCase(),
                            color: d.color
                        }
                    })
            });

       if(args.showBrush){
            this.context = new htc.Brush({
                graph: this,
                width: this.width - this.margin.left,
                height: this.height,
                translateX: this.margin.left,
                translateY: 0
            });
        }

       this.setRenderer(args.renderer || 'stack', args);
       this.discoverRange();

    };

    this.showYAxis = function(_, options){
        if(!arguments.length) return showYAxis;
        showYAxis = _;
        if(showYAxis) {
           this.yAxis = new htc.Axis.Y(htc.extend({ graph: self }, options));
        } else {
            offUpdate(this.yAxis.id);
        }
        return this;
    }

    this.showXAxis = function(_, options){
        if(!arguments.length) return showXAxis;
        showXAxis = _;
        if(showXAxis) {
           this.xAxis = new htc.Axis.X(htc.extend({ graph: self }, options));
        } else {
            offUpdate(this.xAxis.id);
        }
        return this;
    }

    this.showBrush = function(_){
        if(!arguments.length) return _showBrush;
        _showBrush = _;
        return this;
    }

    this.xDomain = function(extent){
        var newX = this.x.copy().domain(extent);
        this.x = newX;
        this.xAxis.vis.select('.x_ticks_d3').remove();
        this.xAxis.vis.select('.x_grid_d3').remove();
        this.xAxis.render.call(this.xAxis);
    }

    this.yDomain = function(yextent){
        var extent = this.renderer.dataExtent({ x: [], y: yextent })
        var newY = this.y.copy().domain(extent.y);
        this.y = newY;
        this.yAxis.vis.select('.y_ticks').remove();
        this.yAxis.vis.select('.y_grid').remove();
        this.yAxis.render.call(this.yAxis);
    }

    this.onBrush = function(brush, extent, range){
        // UPDATE THE XDOMAIN
        this.xDomain(extent);

        var yMin = Infinity, yMax = -Infinity;

        var startD = extent[0], endD = extent[1];

        this.renderer.groups.forEach(function(group){
            var series = group.series;
            series.active = function() { return series };

            series.forEach(function(item){

                var data = item.data.filter(function(d){
                    return (startD <= d.x && d.x <= endD)
                });

                var y1 = d3.min(data, function(d){ return d.y + d.y0 }),
                    y2 = d3.max(data, function(d){ return d.y + d.y0 });

                if(y1) yMin = Math.min(yMin, y1);
                if(y2) yMax = Math.max(yMax, y2);
                item.stack = data;
            })
        }, this);

        // UPDATE Y AXIS DOMAIN IF NECESSARY
        if(!(yMax < 0))
            this.yDomain([yMin, yMax]);

        this.renderer.groups.forEach(function(group){
            var series = group.series;
            group.renderer.render({
                series: series,
                vis: group.vis
            });
        });

    }

    this.moveTo = function(x,y){
        this.translateX = x, this.translateY = y;
        this.vis && this.vis
            .attr("transform", "translate(" + x + "," + y + ")")
        return this;
    }

    this.validateSeries = function(series) {
        if (!Array.isArray(series) && !(series instanceof htc.Series)) {
            var seriesSignature = Object.prototype.toString.apply(series);
            throw "series is not an array: " + seriesSignature;
        }
    };

    this.dropLineOrigin = function(){
        return { x: this.x(0), y: this.y(0) }
    }

    this.discoverRange = function() {

        var domain = this.renderer.domain();
        var margin = this.margin;
        var xDomain = [domain.x[0], domain.x[1]];

        this.x = d3.scale.linear().domain(xDomain)
                    .range([margin.left, this.width]);

        this.y = d3.scale.linear().domain(domain.y)
                    .range([this.height, 0]);

        this.y.magnitude = d3.scale.linear()
            .domain([domain.y[0] - domain.y[0], domain.y[1] - domain.y[0]])
            .range([0, this.height]);
    };

    this.render = function() {

        var stackedData = this.stackData();
        this.discoverRange();

        this.renderer.render();

        if(this.zoomer)
            this.zoomer.extent(this.x.domain()).buttons(_zoomButtons);

        for(var i in this.updateCallbacks) {
            var callback = this.updateCallbacks[i];
            callback();
        }
    };

    this.legendHighlight = function(d){};
    this.legendReset     = function(d){};

    this.update = this.render;

    this.stackData = function() {

        var data = this.series.active()
            .map( function(d) { return d.data } )
            .map( function(d) { return d.filter( function(d) { return this._slice(d) }, this ) }, this);

        var preserve = this.preserve;
        if (!preserve) {
            this.series.forEach( function(series) {
                if (series.scale) {
                    // data must be preserved when a scale is used
                    preserve = true;
                }
            } );
        }

        data = preserve ? htc.clone(data) : data;

        this.series.active().forEach( function(series, index) {
            if (series.scale) {
                // apply scale to each series
                var seriesData = data[index];
                if(seriesData) {
                    seriesData.forEach( function(d) {
                        d.y = series.scale(d.y);
                    } );
                }
            }
        } );

        this.stackData.hooks.data.forEach( function(entry) {
            data = entry.f.apply(self, [data]);
        } );

        var stackedData;

        if (!this.renderer.unstack) {

            this._validateStackable();

            var layout = d3.layout.stack();
            layout.offset( self.offset );
            stackedData = layout(data);
        }

        stackedData = stackedData || data;

        this.stackData.hooks.after.forEach( function(entry) {
            stackedData = entry.f.apply(self, [data]);
        } );


        var i = 0;
        this.series.forEach( function(series) {
            if (series.disabled) return;
            series.stack = stackedData[i++];
        } );

        this.stackedData = stackedData;
        return stackedData;
    };

    this._validateStackable = function() {

        var series = this.series;
        var pointsCount;

        series.forEach( function(s) {

            pointsCount = pointsCount || s.data.length;

            if (pointsCount && s.data.length != pointsCount) {
                throw "stacked series cannot have differing numbers of points: " +
                    pointsCount + " vs " + s.data.length + "; see htc.Series.fill()";
            }

        }, this );
    };

    this.stackData.hooks = { data: [], after: [] };

    this._slice = function(d) {

        if (this.window.xMin || this.window.xMax) {

            var isInRange = true;

            if (this.window.xMin && d.x < this.window.xMin) isInRange = false;
            if (this.window.xMax && d.x > this.window.xMax) isInRange = false;

            return isInRange;
        }

        return true;
    };

    this.onUpdate = function(id, callback) {
        if(!id) throw "id should be empty for render callback."
        if(!callback) throw " callback is empty for " + id;
        this.updateCallbacks[id] = callback;
    };

    this.registerRenderer = function(renderer) {
        this._renderers = this._renderers || {};
        this._renderers[renderer.name] = renderer;
    };

    this.configure = function(args) {

        if (args.width || args.height) {
            this.setSize(args);
        }

        htc.keys(this.defaults).forEach( function(k) {
            this[k] = k in args ? args[k]
                : k in this ? this[k]
                : this.defaults[k];
        }, this );

        this.setRenderer(args.renderer || this.renderer.name, args);
    };

    this.setRenderer = function(r, args) {
        if (typeof r == 'function') {
            this.renderer = new r( { graph: self } );
            this.registerRenderer(this.renderer);
        } else {
            if (!this._renderers[r]) {
                throw "couldn't find renderer " + r;
            }
            this.renderer = this._renderers[r];
        }

        if (typeof args == 'object') {
            args.width = this.width - this.margin.left - this.margin.left;
            args.height = this.height - this.margin.bottom - this.margin.top;
            this.renderer.configure(args);
        }
    };

    this.setSize = function(args) {

        args = args || {};

        if (typeof window !== undefined) {
            var style = window.getComputedStyle(this.element, null);
            if(style){
                var elementWidth = parseInt(style.getPropertyValue('width'), 10);
                var elementHeight = parseInt(style.getPropertyValue('height'), 10);
            }
        }

        var hplus =  (showXAxis ? ( args.xAxis ? args.xAxis.height : this.xAxis.height) : 0) || 0,
            wplus =  (this.margin.left + this.margin.right) || 0,
            wplus = 0;

        this.width  = (args.width || elementWidth || 400) - wplus;
        this.height = (args.height || elementHeight || 250) - hplus;

        this.vis && this.vis
            .attr('width', this.width + wplus)
            .attr('height', this.height + hplus);

        return this;
    };

    return this.initialize(args);
};
