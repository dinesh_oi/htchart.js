htc.namespace('htc.Axis.Y');

htc.Axis.Y = function(graph, args){

    var _showGrid = true,
        _showLabel = true,
        _showRef = true,
        _highlightZero = false;

    this.graph = graph;

    this.initialize = function(args) {

        this.orientation = args.orientation || 'left';
        this.opposite = args.opposite || false;

        this.margin = args.margin || { top: 0, bottom: 0, right: 0, left: 0};

        this.pixelsPerTick = args.pixelsPerTick || 75;
        if (args.ticks) this.staticTicks = args.ticks;
        if (args.tickValues) this.tickValues = args.tickValues;

        this.tickSize = args.tickSize || 4;
        this.ticksTreatment = args.ticksTreatment || 'plain';

        this.tickFormat = args.tickFormat || function(y) { return Number(y) };
        this.axisLabel  = args.title ? args.title.text : args.label

        this.berthRate = 0.00;
        this.plotLines = args.plotLines || [];

        if(!(typeof args.showRef == 'undefined'))
            this.showRef(args.showRef)

        if(!(typeof args.showGrid == 'undefined'))
            this.showGrid(args.showGrid);

        if (args.element) {

            this.element = args.element;
            this.vis = d3.select(args.element)
                .append("svg:svg")
                .attr('class', 'htc_graph y_axis');

            this.element = this.vis[0][0];
            this.element.style.position = 'relative';

            this.setSize({ width: args.width, height: args.height });

        } else {
            this.vis = this.graph.vis;
            this.setSize({ width: args.width, height: args.height });
        }

        var self = this;
        this.id = htc.nameToId(args.name || 'Y Axis');
        this.graph.onUpdate(this.id, function() { self.render() } );
    }

    this.setSize = function(args) {

        args = args || {};

        if (typeof window !== 'undefined' && this.element && this.element.parentNode) {

            var style = window.getComputedStyle(this.element.parentNode, null);
            var elementWidth = parseInt(style.getPropertyValue('width'), 10);

            if (!args.auto) {
                var elementHeight = parseInt(style.getPropertyValue('height'), 10);
            }
        }

        this.width  = args.width || elementWidth || 30;
        this.height = args.height || elementHeight || this.graph.height;

        this.vis
            .attr('width', this.width)
            .attr('height', this.height * (1 + this.berthRate));

        var berth = this.height * this.berthRate;

        if (this.orientation == 'left' && this.element) {
            this.element.style.top = -1 * berth + 'px';
        }
    }

    this.render = function(){
        this.ticks = this.staticTicks || Math.floor(this.graph.height / this.pixelsPerTick);
        this._origin = this.graph.y(0);
        var axis;

        if(_showRef){
            axis = this.drawAxis(this.graph.y);
            this.drawLabel();
        }

        if(_showGrid){
            axis = axis || d3.svg.axis();
            this.drawGrid(axis);
        }

        this.drawPlotLines(axis);

    }

    this._hasTimeField  = function(){
        return false;
    }

    this.drawPlotLines = function(axis){
        if(this.plotLines.length){
            var Y = this.graph.y,
                xscale = this.graph.x.range();

            var _g = this.vis.selectAll('.hori-lines line')

            if(_g.empty())
                _g = this.vis.append("g").attr('class', 'hori-lines')

            var _gEnter = _g.selectAll('line').data(this.plotLines);

                _gEnter.enter()
                    .append("svg:line")
                    .attr("x1", xscale[0])
                    .attr("x2", xscale[1])
                    .attr("y1", function(d){ return Y(d.value) })
                    .attr("y2", function(d){ return Y(d.value) })
                    .attr('stroke', function(d){
                        return d.color || '#609ac1';
                    });

            _gEnter.exit().remove();
        }
    }

    this.drawLabel = function(){
        if(!this.axisLabel)
            return;

        var Y_AXIS_LABEL_CLASS = 'y-axis-label';
        var g = this.vis;
        var axisYLab = g.selectAll("text."+ Y_AXIS_LABEL_CLASS);

        if(axisYLab.empty()){
            axisYLab = g.append('text')
                .attr("transform", "translate(" + 10 + "," + this.graph.height/2 + "),rotate(-90)")
                .attr('class', Y_AXIS_LABEL_CLASS)
                .attr('text-anchor', 'middle')
                .text(this.axisLabel);
        } else {
             axisYLab.text(this.axisLabel);
        }

    }

    this.drawAxis = function(scale) {
        var axis = d3.svg.axis().scale(scale).orient(this.orientation);
        axis.tickFormat(this.tickFormat);
        if (this.tickValues) axis.tickValues(this.tickValues);

        var translateX = this.width,
            berth = 0;

        if(this.opposite)
            translateX += this.graph.width - this.margin.right - this.width;

        var transform = 'translate(' + translateX + ', ' + berth + ')';

        if (this.element) {
            this.vis.selectAll('*').remove();
        }

        this.vis
            .append("svg:g")
            .attr("class", ["y_ticks", this.ticksTreatment].join(" "))
            .attr("transform", transform)
            .call(axis.ticks(this.ticks).tickSubdivide(0).tickSize(this.tickSize));

        return axis;
    }

    this.drawGrid = function(axis) {
        var gridSize = (this.orientation == 'right' ? 1 : -1) * this.graph.width;

        if(this.opposite) gridSize -= this.width;

        var translateX = this.width;
        var graph = this.graph;

        if(graph.y.domain()[1] * graph.y.domain()[0] < 0 )
            _highlightZero = true;


        this.graph.vis
            .append("svg:g")
            .classed("y_grid", true)
            .attr("transform", "translate(" + translateX + ',0)')
            .call(axis.ticks(this.ticks).tickSubdivide(0)
                    .tickSize(gridSize).tickFormat(''));

        if(_highlightZero){
            var yticks = this.vis.selectAll('.y_grid .tick')
                .filter(function() {
                    //this is because sometimes the 0 tick is a very small fraction, TODO: think of cleaner technique
                    return !parseFloat(Math.round(this.__data__*100000)/1000000) && (this.__data__ !== undefined)
                });
            yticks.classed('zero', true);
        }


    }

    this.showRef = function(_){
        if(!arguments.length) return _showRef;
        _showRef = _;
        return this;
    }

    this.showGrid = function(_){
        if(!arguments.length) return _showGrid;
        _showGrid = _;
        return this;
    }

    this.showLabel = function(_){
        if(!arguments.length) return _showLabel;
        _showLabel = _;
        return this;
    }

    return this.initialize(args);
};
