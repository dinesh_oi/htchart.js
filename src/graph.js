htc.namespace('htc.Graph');

htc.Graph = function(args){
    if(!args.element) throw "htc.Graph needs a reference to an element";
    this.renderlets = {};
    this.element = args.element;

    var chartCount         = 0,
        spacing            = { top: 30, right: 0, bottom: 10, left: 0 },
        margin             = { top: 10, right: 10, bottom: 10, left: 10 },
        xAxisHeight        = 20,
        yAxisWidth         = 50,
        _zoomScale         = [-10, 100],
        _showRangeSelector = true,
        _mouseZoomable     = false,
        _buttonZoomable    = true,
        brushHeight        = 80,
        now                = new Date(),
        brushTimerAvg      = 50,
        onBrushTimer       = now,
        tooltips           = true,
        self               = this,
        configOptions      = {},
        _zoomButtons       = [{
            type: 'month',
            count: 1,
            text: '1m'
        }, {
            type: 'month',
            count: 3,
            text: '3m'
        }, {
            type: 'month',
            count: 6,
            text: '6m'
        }, {
            type: 'ytd',
            text: 'YTD'
        }, {
            type: 'year',
            count: 1,
            text: '1y'
        }, {
            type: 'all',
            text: 'All'
        }]

        ;

    this.initialize = function(args){
        if(!args.xAxis) throw "htc.Graph need atleast one xAxis."
        if(!args.yAxis) throw "htc.Graph need atleast one yAxis."
        if(!args.series) throw "htc.Graph need at least one series."

        this.vis = d3.select(this.element)
                        .attr('class', 'htc_graph').append("svg");

        // copy the initial configuration with cloning the data
        configOptions = initConfig(args);
        margin  = configOptions.chart.margin;
        spacing = configOptions.chart.spacing;

        setupYAxisHeight(args);
        setupXAxis(args);

        args.series.forEach(function(options, idx){
            options.data || (options.data = options.data || []);

            var xidx   = options['xAxis'] || 0,
                yidx   = options['yAxis'] || 0,
                topX   = options['top'] || 0,
                height = options['height'],
                name   =  htc.nameToId(options.name || "renderlet" + idx),

                svgEl      = self.vis.append("svg:g").attr("id", name)[0][0],
                xOptions   = htc.clone(args.xAxis[xidx]),
                yOptions   = args.yAxis[yidx],
                plotMargin = htc.clone(margin)
                ;

                xOptions.height = xAxisHeight;
                yOptions.width  = yAxisWidth;
                xOptions.name   = xOptions.name || 'x-axis-' + yidx;

                plotMargin.left     += yOptions.width;
                plotMargin.bottom   += xOptions.height;
                if(options.yAxis == 0)
                    plotMargin.top = margin.top

                var series = new htc.Series(options.data, options.scheme, htc.omit(options, 'data', 'scheme'));

                var renderlet = new htc.Graph.XY({
                    name     : name,
                    element  : svgEl,
                    xAxis    : xOptions,
                    yAxis    : yOptions,
                    renderer : 'multi',
                    margin   : plotMargin,
                    'series' : series,
                });

                renderlet.yIndex = yidx;
                renderlet.xIndex = xidx;
                renderlet.xAxis.enableLabel(false);
                self.renderlets[name] = renderlet;
        });


        chartCount += Object.keys(this.renderlets).length;
        this.setSize(args.chart);
        var idx = 0;

        for(var name in self.renderlets){
            var group = self.renderlets[name];

            var topX     = yAxisValue(idx, 'top'),
                width    = this.width,
                height   = yAxisValue(idx, 'height')
                ;

            group.setSize({ 'width' : width , 'height' : height })
                 .moveTo(0, Math.abs(topX))

            idx++;
        }

        if(configOptions.navigator.enabled)
            this.initNavigator(args);
        else {
            var lastYidx = args.yAxis.length-1;
            for(var key in this.renderlets)
                if(this.renderlets[key].yIndex == lastYidx){
                    this.renderlets[key].xAxis.enableLabel(true)
                }
        }


        if(configOptions.tooltip.enabled){
            this.showTooltips(configOptions.tooltip.enabled);
            this.interactiveLayer = new htc.InteractiveLayer({
                element: this.vis,
                height: this.height - brushHeight ,
            })

            this.interactiveLayer.dispatch
                .on("elementMousemove", this.showToolTip.bind(this))
                .on("elementMouseout",  function(){
                    if(tooltips) htc.tooltip.cleanup();
                });
        }

        if(configOptions.navigation.buttonOptions.enabled){
            this.zoomer = new htc.Zoom({
                graph: this,
                vis: this.vis
            });

            this.zoomer.dispatch.on('zoom', function(interval){
                setFocus(interval);
                self.onBrush();
                self.renderlets['range'].context.render();
            });
       } else {
            this.enableMouseZoom();
        }

        return this;
    }

    var commonXAxis = function(){
        return htc.values(self.renderlets)[0].xAxis;
    }

    var commonXScale = function(){
        return htc.values(self.renderlets)[0].x;
    }

    var initConfig = function(args){
        var ini = configOptions;
        var DEFAUL_NAVIGATOR = {"enabled": true },
            DEFAUL_NAVIGATION = {
                buttonOptions: {
                    enabled: true
                }
            },
            DEFAULT_RANGE_SELECTOR = { "enabled": true },
            DEFAULT_TOOLTIP = { "enabled": true },
            DEFAULT_YAXIS = {
                "ticks": 5
            },
            DEFAULT_XAXIS = {},
            DEFAULT_CHART = {
                "margin"  : margin,
                "spacing" : spacing,
                element   : null,
                animation : true }
            ;

        ini.navigator     = htc.deepMerge(DEFAUL_NAVIGATOR, args.navigator);
        ini.navigation    = htc.deepMerge(DEFAUL_NAVIGATION, args.navigation);
        ini.rangeSelector = htc.deepMerge(DEFAULT_RANGE_SELECTOR, args.rangeSelector);

        ini.tooltip       = htc.deepMerge(DEFAULT_TOOLTIP, args.tooltip);
        ini.chart         = htc.deepMerge(DEFAULT_CHART, args.chart || {});
        return ini;
    }


    this.enableMouseZoom = function() {
        if (_mouseZoomable) {
            self.element.call(d3.behavior.zoom()
                .x(commonXScale())
                .scaleExtent(_zoomScale)
                .on("zoom", function () {
                    setFocus();
                }));
        }
    }


    this.showToolTip = function(e){

        var rows = [], pointIndex, singlePoint, pointXLocation,

                pushRow = function(key, value, color){
                    rows.push({
                        'key': key.toUpperCase(),
                        'value': value.toFixed(2),
                        'color': color
                    })
                };

            htc.values(this.renderlets).forEach(function(plot){

                if(plot.showBrush()) return;

                var tolerance = 0.03 * Math.abs(plot.x.range()[0] - plot.x.range()[1]);

                plot.series.active().forEach(function(item){
                    var getX = function(d, i){
                        return plot.x(d.x);
                    }

                    pointIndex = htc.interactiveBisect(item.stack, e.pointXValue, getX, tolerance);

                    if(!pointIndex) return;

                    if(item._hasCategoryFields){
                        item._categoryFields.forEach(function(accessor){
                            var point = item.stack[pointIndex];

                            if (typeof pointXLocation === 'undefined') pointXLocation = getX(point, pointIndex);
                            if (typeof singlePoint === 'undefined') singlePoint = point.x;

                            pushRow(accessor, point[accessor], item.color);
                        })
                    } else {
                        var point = item.stack[pointIndex];

                        if (typeof pointXLocation === 'undefined') pointXLocation = getX(point, pointIndex);
                        if (typeof singlePoint === 'undefined') singlePoint = point.x;

                        pushRow(item.key, point.y, item.color);
                    }
                })
            });

          //Highlight the tooltip entry based on which point the mouse is closest to.
          if (rows.length > 2) {
            htc.values(this.renderlets).forEach(function(plot){
                var yValue = plot.y.invert(e.mouseY);
                var domainExtent = Math.abs(plot.y.domain()[0] - plot.y.domain()[1]);
                var threshold = 0.03 * domainExtent;
                var indexToHighlight = htc.nearestValueIndex(rows.map(function(d){ return d.value }), yValue, threshold);
                if (indexToHighlight !== null)
                  rows[indexToHighlight].highlight = true;
            })
          }

          if(rows.length == 0) return;

          var xValue = commonXAxis().tickFormat(singlePoint);
          var margin = { left: 20, top: 0 };

          this.interactiveLayer.tooltip
                  .position({left: pointXLocation + margin.left, top: e.mouseY + margin.top})
                  .chartContainer(this.vis.parentNode)
                  .enabled(tooltips)
                  .valueFormatter(function(d,i) {
                     return d;
                  })
                  .data(
                      {
                        value: xValue,
                        series: rows
                      }
                  )();

          this.interactiveLayer.renderGuideLine(pointXLocation);

    }

    this.showTooltips = function(_){
        if(!arguments.length) return tooltips;
        tooltips = _;
        return this;
    }

    var setFocus = function(interval){
        var navigator = configOptions.navigator;

        if(navigator.enabled){
            var brushG = self.renderlets['range'],
                domain   = brushG.x.domain(),
                x            = brushG.x,
                brush        = brushG.context.brush(),
                startD, endD;

            if(interval){
                if(htc.discreteTime){
                    startD = new Date(domain[1]-interval),
                    endD   = domain[1];
                    self.renderlets['range'].context.clearBrush();
                } else {
                    startD = htc.weekday(new Date(htc.weekday.invert(domain[1])-interval)),
                    endD   = domain[1];
                    self.renderlets['range'].context.clearBrush();
                }
            } else {
                if(htc.discreteTime){
                    var rangeSeconds = domain[1] - domain[0],
                        startOffset  = domain[0].getTime() + rangeSeconds * 0.3,
                        endOffset    = domain[1].getTime() - rangeSeconds * 0.4
                        ;

                    startD       = new Date(startOffset);
                    endD         = new Date(endOffset);
                }
                else {
                    var rangeSeconds = domain[1] - domain[0],
                        startOffset  = domain[0] + Math.floor(rangeSeconds * 0.3),
                        endOffset    = domain[1] - Math.floor(rangeSeconds * 0.4)
                        ;

                    startD   = startOffset;
                    endD     = endOffset;
                }
            }

            brush = brush.x(x);
            brush = brush.extent([startD, endD]);
        }
    }

    this.render = function(){
        setFocus();


        for(var name in this.renderlets){
            this.renderlets[name].render();
        }

        if(this.zoomer){
            this.zoomer.buttons(_zoomButtons)
                .extent(commonXScale().domain())
                .margin({ left: yAxisWidth, top: 0 , bottom: 0, right: 0});

            this.zoomer.render();
        }

        if(configOptions.navigator.enabled) this.onBrush();


    }

    this.setSize = function(args) {
        args = args || {};

        if (typeof window !== undefined) {
            var style = window.getComputedStyle(this.element, null);
            if(style){
                var elementWidth = parseInt(style.getPropertyValue('width'), 10);
                var elementHeight = parseInt(style.getPropertyValue('height'), 10);
            }
        }

        this.width  = (args.width  || elementWidth  || 400) - spacing.left - spacing.right;
        this.height = (args.height || elementHeight || 250) + brushHeight - spacing.top - spacing.bottom;

        this.vis && this.vis
            .attr('width', this.width)
            .attr('height', this.height);

    };

    this.initNavigator = function(args){
        var selectOpts = args.navigator;

        var svgEl     = self.vis.append("svg:g").attr("id", "navigator"),
            series    = htc.values(this.renderlets)[0].series[0],
            margin    = htc.clone(spacing),
            args      = args || {},
            width     = args.width  || this.width,
            lastYAxis = args.yAxis[args.yAxis.length-1],
            topY      = lastYAxis.top + lastYAxis.height;

        margin.left   += yAxisWidth;

        var values = series.data.map(function(d){
            return { x: d.time, y: d.y, y0: d.y0 }
        });

        var data  = [{
            key: 'range',
            values: values,
            renderer: 'stack',
            color: '#609ac1'
        }]

        var brushG = new htc.Graph.XY({
            name: 'range',
            width : width,
            height: brushHeight,
            element: svgEl[0][0],
            renderer: 'multi',
            margin : margin,
            xAxis: {
                name: 'x-axis-' + args.yAxis.length,
                width : width,
                height: xAxisHeight,
                labels: {
                    format: 'day',
                    enabled: true
                },
                grid: {
                    enabled: false
                }
            },
            yAxis: {
                ticks: 2,
                top: topY,
                height: brushHeight,
                width: yAxisWidth
            },
            showBrush: true,
            series: new htc.Series(data, null, {})
        });


        brushG.context.brush().on("brush", this.onBrush.bind(this));
        brushG.yAxis.showGrid(false).showRef(false).showLabel(false);
        self.renderlets[brushG.name] = brushG;
        brushG.setSize({ width: width, height: brushHeight })
            .moveTo(0, topY);

    }

    this.onBrush = function(){
        var charts = htc.values(this.renderlets),
            chartContext = this.renderlets["range"],
            series = chartContext.series[0],
            brush  = chartContext.context.brush();

        var startTime = Date.now();
        if (startTime - onBrushTimer > brushTimerAvg) {
            // Compute the context xDomain
            var filtered_data;
            // var xDomain;
            // if (brush.empty()) {
            //     filtered_data = series.data;
            // }
            // else {
            //     var brush_extent = brush.extent();
            //     filtered_data = series.data.filter(function (element, index, array) {
            //         return (brush_extent[0] <= series.getX(element) &&
            //                 series.getX(element) <= brush_extent[1]);
            //     });
            // }
            // xDomain = d3.extent(filtered_data.map(series.getX));

            // // Find the x index range of the chart data
            var xRange = [0, 0], xExtent = [0, 0];
            // var hasStart = false;
            // var hasEnd = false;

            // for (var i = 0; i < series.data.length; i++) {
            //     var x = series.getX(series.data[i]);
            //     if (!hasStart && xDomain[0] == x) {
            //         xRange[0]  = i;
            //         xExtent[0] = x;
            //         hasStart = true;
            //     }
            //     if (!hasEnd && xDomain[1] == x) {
            //         xRange[1] = i;
            //         xExtent[1] = x;
            //         hasEnd = true;
            //     }
            //     if (hasStart && hasEnd) {
            //         break;
            //     }
            // }

            xExtent = brush.empty() ? chartContext.x.domain() : brush.extent();

            for (var i = 0; i < charts.length; i++) {
                if(!(charts[i].name == chartContext.name)){
                    charts[i].onBrush.call(charts[i], brush, xExtent, xRange)
                }
            }

            var now = Date.now();
            brushTimerAvg = ((0.40 * brushTimerAvg) + (0.60 * (now - startTime))) / 1.95;
            onBrushTimer = now;
        }
    };

    var yAxisValue = function(idx, name, bydefault){
        if(!(typeof args.yAxis[idx] == 'undefined')){
            return args.yAxis[idx][name] || bydefault;
        } else {
            return bydefault;
        }
    }

    var setupXAxis = function(args){
        if(!(args.xAxis instanceof Array))
            args.xAxis = [args.xAxis];
    }

    var setupYAxisHeight = function(args){
        var totalHeight, unknownHeightIdx, unknownHeightCount = 0;

        if(!args.chart.height)
            args.chart.height = d3.sum(args.yAxis.map(function(y){
                if(!y.height)
                    throw "chart.height and y axis height is missing: " + JSON.stringify(y);
                return y.height + spacing.top + spacing.bottom
            }));

        totalHeight = args.chart.height - margin.top - margin.bottom - brushHeight;

        args.yAxis.forEach(function(axis, i){

            if(i==0){
                axis.top = spacing.top + margin.top;
                if(axis.height)
                    axis.height = axis.height - margin.top;
            }

            if(typeof axis.height == 'undefined'){
                unknownHeightIdx = i;
                unknownHeightCount += 1;
            }
        })

        if(unknownHeightCount > 1)
            throw JSON.stringify(args.yAxis[unknownHeightIdx]) + " should have height/top parameter."

        if(!totalHeight && unknownHeightIdx > 0)
            throw JSON.stringify(args.yAxis[unknownHeightIdx]) + " should have height/top parameter or graph total height should be given."

        if(totalHeight && unknownHeightCount == 1){
            var newHeight,
                prevTop = yAxisValue(unknownHeightIdx-1, 'top') ||
                         d3.sum(args.yAxis.slice(0, Math.max(unknownHeightIdx-1, 0)).map(function(x){ return x.height; })),

                nextTop = yAxisValue(unknownHeightIdx+1, 'top') ||
                          d3.sum(args.yAxis.slice(Math.min(unknownHeightIdx+1, args.yAxis.length), args.yAxis.length).map(function(x){ return x.height; }))
                    ;

            if(prevTop && nextTop)
                newHeight = nextTop - prevTop;
            else if(nextTop)
                newHeight = totalHeight - nextTop;
            else if(prevTop)
                newHeight = totalHeight - prevTop- brushHeight;

            args.yAxis[unknownHeightIdx].height = newHeight;
        }

        args.yAxis.forEach(function(axis, i){
            if(typeof axis.top == 'undefined' && i > 0){
                var prevY = args.yAxis[i-1];
                axis.top = prevY.top + prevY.height;
            }
        });

        self.totalHeight = totalHeight;

        return args;
    }

    return this.initialize(args);
}

