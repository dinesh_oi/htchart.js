
htc.namespace('htc.Renderer.Line');

htc.Renderer.Line = htc.Class.create( htc.Renderer, {

    name: 'line',

    defaults: function($super) {

        return htc.extend( $super(), {
            unstack: true,
            fill: false,
            stroke: true,
            dashed: false
        } );
    },

    seriesPathFactory: function() {

        var graph = this.graph;

        var factory = d3.svg.line()
            .x( function(d) { return graph.x(graph.mapDataX(d)) })
            .y( function(d) { return graph.y(d.y) } )
            .interpolate(this.graph.interpolation).tension(this.tension);

        factory.defined && factory.defined( function(d) { return d.y !== null } );
        return factory;
    }
} );


htc.namespace('htc.Renderer.LinePlot');

htc.Renderer.LinePlot = htc.Class.create( htc.Renderer, {

    name: 'lineplot',

    defaults: function($super) {
        return htc.extend( $super(), {
            unstack: true,
            fill: false,
            stroke: true,
            padding:{ top: 0.01, right: 0.01, bottom: 0.01, left: 0.01 },
            dotSize: 3,
            strokeWidth: 1
        } );
    },

    initialize: function($super, args) {
        $super(args);
    },

    seriesPathFactory: function() {
        var graph = this.graph;
        var factory = d3.svg.line()
            .x( function(d) { return graph.x(graph.mapDataX(d)); } )
            .y( function(d) { return graph.y(d.y) } )
            .interpolate(this.graph.interpolation)
            .tension(this.tension);

        factory.defined && factory.defined( function(d) { return d.y !== null } );
        return factory;
    },

    _renderDots: function(vis, series) {

        var graph = this.graph, self = this;

        series.forEach(function(series) {

            if (series.disabled) return;

            var nodes = vis.selectAll("circle")
                .data(series.stack.filter( function(d) { return d.y !== null } ), series.getX)

            nodes.attr("cx", function(d) { return graph.x(graph.mapDataX(d)); })
                 .attr("cy", function(d) { return graph.y(d.y) })
                 .attr("r", function(d) { return ("r" in d) ? d.r : self.dotSize })

            var nodesEnter = nodes
                .enter().append("svg:circle")
                .attr("cx", function(d) { return graph.x(graph.mapDataX(d)); })
                .attr("cy", function(d) { return graph.y(d.y) })
                .attr("r", function(d) { return ("r" in d) ? d.r : self.dotSize })
                .attr({
                    'data-color': series.color,
                    'fill': 'white',
                    'stroke': series.color,
                    'stroke-width': this.strokeWidth
                })

            nodes.exit().remove();

        }, this);

    },

    _renderLines: function(vis, series) {

        var graph = this.graph,
            data = series.active().map(function(s){
                return s.stack.filter( function(d) { return d.y !== null } )
            }),
            factory = this.seriesPathFactory()
            ;

        var nodes = vis.selectAll("path").data(data)

        var nodeEnter = nodes
                        .enter().append("svg:path")
                        .attr("d", factory);

        nodes.attr("d", factory);
        nodes.exit().remove();

        var i = 0;
        series.forEach(function(series) {
            if (series.disabled) return;
            series.path = nodes[0][i++];
            this._styleSeries(series);
        }, this);

    },

    render: function(args) {
        var vis   = args.vis;
        this._renderLines(vis, args.series);
        this._renderDots(vis, args.series);
    }
} );

