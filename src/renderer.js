htc.namespace("htc.Renderer");

htc.Renderer = htc.Class.create( {

    initialize: function(args) {
        this.graph = args.graph;
        this.tension = args.tension || this.tension;
        this.configure(args);
    },

    seriesPathFactory: function() {
        //implement in subclass
    },

    seriesStrokeFactory: function() {
        // implement in subclass
    },

    defaults: function() {
        return {
            tension: 0.8,
            strokeWidth: 2,
            unstack: true,
            padding: { top: 0.2, right: 0, bottom: 0.05, left: 0 },
            stroke: false,
            fill: false,
            width: 100,
            height: 10
        };
    },

    domain: function(data) {

        var stackedData = data || this.graph.stackedData || this.graph.stackData();
        var firstPoint = stackedData[0][0];

        if (firstPoint === undefined) {
            return { x: [null, null], y: [null, null] };
        }

        var xMin = firstPoint.x;
        var xMax = firstPoint.x;

        var yMin = firstPoint.y + firstPoint.y0;
        var yMax = firstPoint.y + firstPoint.y0;

        stackedData.forEach( function(series) {
            series.forEach( function(d) {
                if (d.y == null) return;
                var y = d.y + d.y0;
                if (y < yMin) yMin = y;
                if (y > yMax) yMax = y;
            });

            if (!series.length) return;

            if (series[0].x < xMin) xMin = series[0].x;
            if (series[series.length - 1].x > xMax) xMax = series[series.length - 1].x;
        } );

        return this.dataExtent({ x: [xMin, xMax], y: [yMin, yMax] });
    },

    dataExtent: function(extent){
        var yMin = extent.y[0], yMax = extent.y[1],
            xMin = extent.x[0], xMax = extent.x[1];

        // xMin -= (xMax - xMin) * this.padding.left;
        // xMax += (xMax - xMin) * this.padding.right;

        yMin = this.graph.min === 'auto' ? yMin : this.graph.min || 0;
        yMax = this.graph.max === undefined ? yMax : this.graph.max;

        if (this.graph.min === 'auto' || yMin < 0) {
            yMin -= (yMax - yMin) * this.padding.bottom;
        }

        if (this.graph.max === undefined) {
            yMax += (yMax - yMin) * this.padding.top;
        }

        return { x: [xMin, xMax], y: [yMin, yMax] };
    },

    render: function(args) {

        args = args || {};

        var graph = this.graph;
        var series = args.series || graph.series;

        var vis = args.vis || graph.vis;

        var data = series
            .filter(function(s) { return !s.disabled })
            .map(function(s) { return s.stack });

        var nodes = vis.selectAll("path").data(data);

        var factory = this.seriesPathFactory();

        nodes.attr("d", factory);

        var nodesEnter = nodes
            .enter().append("svg:path")
            .attr("d", factory);

        nodes.exit().remove();

        var i = 0;
        series.forEach( function(series) {
            if (series.disabled) return;
            series.path = nodes[0][i++];
            this._styleSeries(series);
        }, this );
    },

    _styleSeries: function(series) {

        var fill   = this.fill ? series.color : 'none';
        var stroke = this.stroke ? series.color : 'none';

        series.path.setAttribute('fill', fill);
        series.path.setAttribute('stroke', stroke);
        series.path.setAttribute('stroke-width', this.strokeWidth);
        if(series.dashed || this.dashed)
            series.path.setAttribute('stroke-dasharray', '1 1');
        series.path.setAttribute('class', series.className);
    },

    configure: function(args) {

        args = args || {};

        htc.keys(this.defaults()).forEach( function(key) {

            if (!args.hasOwnProperty(key)) {
                this[key] = this[key] || this.graph[key] || this.defaults()[key];
                return;
            }

            if (typeof this.defaults()[key] == 'object') {
                htc.keys(this.defaults()[key]).forEach( function(k) {
                    this[key] = this[key] || {};
                    this[key][k] =
                        args[key][k] !== undefined ? args[key][k] :
                        this[key][k] !== undefined ? this[key][k] :
                        this.defaults()[key][k];
                }, this );

            } else {
                this[key] =
                    args[key] !== undefined ? args[key] :
                    this[key] !== undefined ? this[key] :
                    this.graph[key] !== undefined ? this.graph[key] :
                    this.defaults()[key];
            }

        }, this );
    },

    setStrokeWidth: function(strokeWidth) {
        if (strokeWidth !== undefined) {
            this.strokeWidth = strokeWidth;
        }
    },

    setTension: function(tension) {
        if (tension !== undefined) {
            this.tension = tension;
        }
    }
} );


htc.namespace('htc.Renderer.Multi');

htc.Renderer.Multi = htc.Class.create( htc.Renderer, {

    name: 'multi',

    initialize: function($super, args) {

        $super(args);
    },

    defaults: function($super) {

        return htc.extend( $super(), {
            unstack: true,
            fill: false,
            stroke: true
        } );
    },

    domain: function($super) {
        this.graph.stackData();
        var domains = [];
        var groups = this._groups();

        groups.forEach( function(group) {
            var data = group.series
                .filter( function(s) { return !s.disabled } )
                .map( function(s) {
                    return s.stack;
                });

            if (!data.length) return;
            var domain = $super(data);
            domains.push(domain);
        });


        var xMin = d3.min(domains.map( function(d) { return d.x[0] } ));
        var xMax = d3.max(domains.map( function(d) { return d.x[1] } ));
        var yMin = d3.min(domains.map( function(d) { return d.y[0] } ));
        var yMax = d3.max(domains.map( function(d) { return d.y[1] } ));

        return { x: [xMin, xMax], y: [yMin, yMax] };
    },

    _groups: function() {

        var graph = this.graph;
        var renderGroups = {};

        graph.series.forEach( function(series) {

            if (series.disabled) return;

            if (!renderGroups[series.renderer]) {
                var className = series.renderer + '-' + 'wrap';
                var vis = graph.vis.select('g.chart-body.' + className)

                if(!vis[0][0])
                    vis = graph.vis
                                .append('svg:g')
                                .attr('class', 'chart-body ' + className)
                                // .attr("transform", "translate(" + graph.translateX + "," + graph.translateY + ")");

                var renderer = graph._renderers[series.renderer];

                renderGroups[series.renderer] = {
                    renderer: renderer,
                    series: [],
                    vis: vis
                };
            }

            renderGroups[series.renderer].series.push(series);

        }, this);

        var groups = [];

        Object.keys(renderGroups).forEach( function(key) {
            var group = renderGroups[key];
            groups.push(group);
        });

        return groups;
    },

    render: function() {

        this.graph.series.forEach( function(series) {
            if (!series.renderer) {
                throw new Error("Each series needs a renderer for graph 'multi' renderer");
            }
        });

        var groups = this._groups();
        this.groups = groups;

        groups.forEach( function(group) {

            var series = group.series
                .filter( function(series) { return !series.disabled } );

            series.active = function() { return series  };
            group.renderer.render({ series: series, vis: group.vis });
            series.forEach(function(s) {
                s.stack = s._stack || s.stack || s.data;
            });
        });
    }

} );
