function isObject(obj) {
    return typeof obj === "object" && obj !== null;
}

var htc = {
    namespace: function(namespace, obj) {

        var parts = namespace.split('.');

        var parent = htc;

        for(var i = 1, length = parts.length; i < length; i++) {
            var currentPart = parts[i];
            parent[currentPart] = parent[currentPart] || {};
            parent = parent[currentPart];
        }
        return parent;
    },

    keys: function(obj) {
        var keys = [];
        for (var key in obj) keys.push(key);
        return keys;
    },

    deepMerge: function(destination, source, clone) {
        if(clone)
            destination = htc.clone(destination);

        if(destination && source) {
            for(var key in source) {
                if(source.hasOwnProperty(key)) {
                    if(isObject(source[key]) && isObject(destination[key])) {
                        htc.deepMerge(destination[key], source[key], false);
                    } else {
                        destination[key] = source[key];
                    }
                }
            }
        }
        return destination;
    },
    extend: function(destination, source) {

        for (var property in source) {
            destination[property] = source[property];
        }
        return destination;
    },

    clone: function(obj) {
        return JSON.parse(JSON.stringify(obj));
    }
};

htc.formats = {
    millisecond : d3.time.format("%H:%M:%S.%L"),
    second      : d3.time.format("%H:%M:%S"),
    minute      : d3.time.format("%H:%M"),
    hour        : d3.time.format("%H:%M"),
    day         : d3.time.format("%e. %b"),
    week        : d3.time.format("%e. %b"),
    month       : d3.time.format("%b \'%y"),
    year        : d3.time.format('%Y')
}

if (typeof module !== 'undefined' && module.exports) {
    var d3 = require('d3');
    module.exports = htc;
}

