
htc.namespace('htc.Brush');

htc.Brush = function(args){
    var _width  = args.width,
        _height = args.height,
        _translateY = 0,
        _translateX = 0,
        _brush = d3.svg.brush(),
        onBrushHandleTouch = args.onBrushHandleTouch || function(ev){
            d3.event.preventDefault();
        },
        onBrushEvents = args.onBrushEvents || function(ev){
            d3.event.preventDefault();
        },
        self = this,
        _resizeHandlePath =  function (d) {
            var e = +(d == "e"), x = e ? 1 : -1, y = _height / 3;
            /*jshint -W014 */
            return "M" + (0.5 * x) + "," + y
                + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6)
                + "V" + (2 * y - 6)
                + "A6,6 0 0 " + e + " " + (0.5 * x) + "," + (2 * y)
                + "Z"
                + "M" + (2.5 * x) + "," + (y + 8)
                + "V" + (2 * y - 8)
                + "M" + (4.5 * x) + "," + (y + 8)
                + "V" + (2 * y - 8);
            /*jshint +W014 */
        }
        ;

    this.initialize = function(args){
        this.graph  = args.graph;
        this.graph.onUpdate(htc.safe_number(), function(){
            this.render();
        }.bind(this));

        _translateX = args.translateX;
        _translateY = args.translateY;
    }

    this.brush = function(_){
        if(!arguments.length) return _brush;
        _brush = _;
        return _brush;
    }

    this.clearBrush = function(){
        // d3.select('.x.brush').call(_brush.clear());
    }

    this.render = function(){

        _brush.x(self.graph.x);

        this.graph.vis.select('.x.brush').remove();

        var context =  this.graph.vis.append("g")
                                    .attr('class', 'x brush')

        context.call(_brush)
                .selectAll('rect')
                .attr('y', _translateY)
                .attr('height', _height);

        var contextHeight = _height,
            contextWidth  = 5;

        context.selectAll('.resize rect')
            .attr('width', contextWidth)
            .attr('x', -3)

        context.selectAll(".resize").append("path").attr("d", _resizeHandlePath);

        this.vis = context;
    }

    return this.initialize(args);
};
