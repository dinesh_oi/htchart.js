
(function(htc){

htc.namespace('htc.Series');

htc.Series = htc.Class.create( Array, {

    initialize: function (data, scheme, options) {

        options = options || {};

        this.palette = new htc.Color.Palette({ 'scheme': scheme });

        this.timeBase = typeof(options.timeBase) === 'undefined' ?
            Math.floor(new Date().getTime() / 1000) :
            options.timeBase;

        var timeInterval = typeof(options.timeInterval) == 'undefined' ?
            1000 :
            options.timeInterval;

        this.setTimeInterval(timeInterval);

        if (data && (typeof(data) == "object") && Array.isArray(data) ) {
            data.forEach( function(item) {
                this.addItem(item);
            }, this );
        }

    },

    addItem: function(item) {
        item.name || (item.name = item.key);
        item.data || (item.data = item.values || []);

        switch(item.renderer){
            case 'candlestick':
                ohlcData(item);
                break;
            case 'marker':
                markerData(item);
                break;
            default:
                xyData(item);
                break;
        }

        if (typeof(item.name) === 'undefined') {
            throw('addItem() needs a name');
        }

        item.color = (item.color || this.palette.color(item.name));
        item.palette = this.palette;

        if(item.backfill){
         // backfill, if necessary
            if ((item.data.length === 0) && this.length) {
                this[0].data.forEach( function(plot) {
                    item.data.push({ x: plot.x, y: 0 });
                } );
            } else if (item.data.length === 0) {
                item.data.push({ x: this.timeBase - (this.timeInterval || 0), y: 0 });
            }
        }

        this.push(item);

        if (this.legend) {
            this.legend.addLine(this.itemByName(item.name));
        }
    },

    setTimeInterval: function(iv) {
        this.timeInterval = iv / 1000;
    },

    setTimeBase: function (t) {
        this.timeBase = t;
    }
} );

var xyData = function(item, extraMappings){
    var mappings = {
        getX      : item.getX      || function(d){ return new Date(d.x || d.time) },
        getY      : item.getY      || function(d){ return d.y },
        getY0     : item.getY0     || function(d){ return 0 }
    }

    item._timeField = 'time';
    item._hasCategoryFields = false;

    validateXYData(item, mappings);
}

var ohlcData = function(item){
    var tt = item._timeField = item.timeField || 'time';

    var mappings = {
        'getTime'   : item.getTime   || function(d){ return new Date(d.x || d[tt]) },
        'getOpen'   : item.getOpen   || function(d){ return d.open },
        'getClose'  : item.getClose  || function(d){ return d.close },
        'getLow'    : item.getLow    || function(d){ return d.low },
        'getHigh'   : item.getHigh   || function(d){ return d.high },
        'getX'      : item.getX      || function(d){ return new Date(d[tt]) },
        'getY'      : item.getY      || function(d){ return d.low },
        'getY0'     : item.getY0     || function(d){ return 0 },
    }

    item._hasCategoryFields = true;
    item._categoryFields = ['open', 'close', 'high', 'low'];

    validateOHLCData(item, mappings);
    return item;
}

var markerData = function(item){
    xyData(item);
    return item;
}

var validateXYData = function(s, mappings){
    if (!(s instanceof Object)) {
        throw "series element is not an object: " + s;
    }
    if (!(s.data)) {
        throw "series has no data: " + JSON.stringify(s);
    }
    if (!Array.isArray(s.data)) {
        throw "series data is not an array: " + JSON.stringify(s.data);
    }

    s.data = s.data.map(function(d){
        var newd = {
            x    : htc.weekday(mappings.getX(d)),
            y    : mappings.getY(d),
            y0   : (mappings.getY0(d) || 0),
            time : mappings.getX(d)
        }
        if(d.title || d.text){
            newd.title = d.title || d.y;
            newd.text = d.text || d.title || d.y;
        }
        return newd;
    });

    if(s.data.length > 0){
        var x = s.data[0].x;
        var y = s.data[0].y;

        if((typeof x != 'number') && !(x instanceof Date)){
            throw "x property of points should be number or date instead of " + (typeof x);
        }

        if (typeof y != 'number' && y !== null) {
            throw "y property of points should be numbers instead of " + (typeof y);
        }
    }

    if (s.data.length >= 3) {
        // probe to sanity check sort order
        if (s.data[2].x < s.data[1].x || s.data[1].x < s.data[0].x || s.data[s.data.length - 1].x < s.data[0].x) {
            throw "series data needs to be sorted on x values for series name: " + s.name;
        }
    }

    if(s.data.length > 0){
        // set series's domain in the terms of x and y values
        var x0 = s.data[0].x,
            x1 = s.data[s.data.length-1].x,
            y0 = s.data[0].y,
            y1 = s.data[s.data.length-1].y

        s.domain = {x: [x0, x1], y: [y0, y1] };

    }
}

var validateOHLCData = function(s, mappings){
    if (!(s instanceof Object)) {
        throw "series element is not an object: " + s;
    }
    if (!(s.data)) {
        throw "series has no data: " + JSON.stringify(s);
    }
    if (!Array.isArray(s.data)) {
        throw "series data is not an array: " + JSON.stringify(s.data);
    }

    s.data = s.data.map(function(d){
        return {
            x       : htc.weekday(mappings.getX(d)),
            y       : mappings.getY(d),
            y0      : mappings.getY0(d),
            time    : mappings.getTime(d),
            open    : mappings.getOpen(d),
            close   : mappings.getClose(d),
            high    : mappings.getHigh(d),
            low     : mappings.getLow(d),
        }
    })

    var x = s.data[0].time;
    var o = s.data[0].open, l = s.data[0].low, h = s.data[0].high, c = s.data[0].close;

    var throwN = function(name, value){
        if (typeof value != 'number' && value !== null) {
            throw name + " property of points should be numbers instead of " + (typeof value);
        }
    }

    if((typeof x != 'number') && !(x instanceof Date)){
        throw "x property of points should be number or date instead of " + (typeof x);
    }

    throwN('open', o) || throwN('high', h) || throwN('low', l) || throwN('close', c);

    if (s.data.length >= 3) {
        // probe to sanity check sort order
        if (s.data[2].time < s.data[1].time || s.data[1].time < s.data[0].time || s.data[s.data.length - 1].time < s.data[0].time) {
            throw "series data needs to be sorted on time values for series name: " + s.name;
        }
    }

    // set series's domain in the terms of x and y values
    var x0 = s.data[0].x,
        x1 = s.data[s.data.length-1].x,
        y0 = s.data[0].y,
        y1 = s.data[s.data.length-1].y

    s.domain = {x: [x0, x1], y: [y0, y1] };

};

})(htc);