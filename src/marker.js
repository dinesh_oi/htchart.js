
// Possible shapes:
// ["circle", "cross", "diamond", "square", "triangle-down", "triangle-up"]

htc.Renderer.Marker = htc.Class.create(htc.Renderer, {
    name: 'marker',

    defaults: function($super){
        return htc.extend($super(), {
            dotSize: 3,
            strokeWidth: 1,
            shape: 'circle',
            markerBerth: 30,
            markerSize: 400
        });
    },

    render : function(args){
        var vis    = args.vis,
            graph  = this.graph,
            series = args.series,
            self = this
            ;

        series.active().forEach(function(s){
            var data = s.stack.filter(function(d) { return d.y !== null } ),
                shape = s.shape || self.shape || d3.svg.symbolTypes[0],
                symbol = d3.svg.symbol().type(shape).size(this.markerSize),
                berth  = s.markerBerth || this.markerBerth,
                className = 'dot-' + s.key
                ;


            var markers = vis.selectAll("g." + className)
                .data(data, function(d){ return d.x })

            markers.attr("transform", function(d, i) {
                return "translate(" + graph.x(d.x) + "," +  (graph.y(d.y) - berth)+ ")";
            });

            var elemEnter = markers
                            .enter().append('svg:g')
                            .attr('class', 'dot ' + className)
                            .attr("transform", function(d, i) {
                                return "translate(" + graph.x(d.x) + "," +  (graph.y(d.y) - berth)+ ")";
                            })

            var lines = elemEnter.append('svg:line')
                            .style("stroke", '#0d233a')
                            .style("stroke-width", this.strokeWidth)
                            .attr("x1", 0).attr("x2", 0).attr("y1", 0).attr("y2", function(d){
                                return berth;
                            })

            var paths = elemEnter.append("svg:path")
                        .attr("d", symbol)
                        .style("stroke", series.color || '#0d233a')
                        .on("mouseover", function(){
                            d3.select(this).classed('hover', true);
                        }).on('mouseout', function(){
                            d3.select(this).classed('hover', false);
                        })

            var texts = elemEnter.append("svg:text")
                .attr("alignment-baseline", "middle")
                .attr("text-anchor", "middle")
                .text(function(d){ return d.title })

            var markerExit = markers.exit();

            markerExit.remove();

        }, this)

    }
});
