

htc.nameToId = function(name){
    return name.toLowerCase().replace(/[\s]/g, "_").replace(/[\.']/g, "");
};

htc.each = function(obj, iterator, context) {
    if (obj == null) return;
    obj.forEach(iterator, context);
  };

htc.values = function(obj){
    var values = [];
    for(var key in obj)
        values.push(obj[key]);
    return values;
}

htc.safe_number = function(){
    return Math.floor(Math.random() * 10000);
}

/* Numbers that are undefined, null or NaN, convert them to zeros.
*/
htc.NaNtoZero = function(n) {
    if (typeof n !== 'number'
        || isNaN(n)
        || n === null
        || n === Infinity) return 0;

    return n;
};

htc.any = function(obj, iterator, context) {
    var result = false;
    if (obj == null) return result;
    htc.each(obj, function(value, index, list) {
      if (result || (result = iterator.call(context, value, index, list))) return {};
    });
    return !!result;
 };

// taken from underscore.js
htc.contains = function(obj, target) {
    if (obj == null) return false;
    var nativeIndexOf = Array.indexOf;
    if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
    return htc.any(obj, function(value) {
      return value === target;
    });
};

htc.omit = function(obj) {
    var copy = {};
    var keys = Array.prototype.slice.call(arguments, 1);
    for (var key in obj) {
      if (!htc.contains(keys, key)) copy[key] = obj[key];
    }
    return copy;
};


htc.windowSize = function() {
    // Sane defaults
    var size = {width: 640, height: 480};

    // Earlier IE uses Doc.body
    if (document.body && document.body.offsetWidth) {
        size.width = document.body.offsetWidth;
        size.height = document.body.offsetHeight;
    }

    // IE can use depending on mode it is in
    if (document.compatMode=='CSS1Compat' &&
        document.documentElement &&
        document.documentElement.offsetWidth ) {
        size.width = document.documentElement.offsetWidth;
        size.height = document.documentElement.offsetHeight;
    }

    // Most recent browsers use
    if (window.innerWidth && window.innerHeight) {
        size.width = window.innerWidth;
        size.height = window.innerHeight;
    }
    return (size);
};



// Easy way to bind multiple functions to window.onresize
// TODO: give a way to remove a function after its bound, other than removing all of them
htc.windowResize = function(fun){
  if (fun === undefined) return;
  var oldresize = window.onresize;

  window.onresize = function(e) {
    if (typeof oldresize == 'function') oldresize(e);
    fun(e);
  }
}
