
htc.namespace('htc.Zoom');

htc.Zoom = function(args){
    var graph   = args.graph,
        _parent = args.vis,
        _buttons = [],
        _g,
        _itemHeight = 30,
        fixedTimes = {
            millisecond: 1,
            second: 1000,
            minute: 60 * 1000,
            hour: 3600 * 1000,
            day: 24 * 3600 * 1000,
            week: 7 * 24 * 3600 * 1000,
            month: 30 * 24 * 3600 * 1000,
            year: 365 * 24 * 3600 * 1000
        },
        _margin = { top: 0, left: 0, bottom: 0, right: 0},
        _extent = [-10, 10],
        dispatch = d3.dispatch('zoom')
        ;


    this.render = function(){
        _g = _parent.append('g')
            .attr('class', 'htc-zoom')
            .attr("transform", "translate(" + _margin.left + "," + _margin.top + ")");

        var zoomButtons = _g.selectAll('.htc-zoom-item').data(_buttons);

        var zoomEnter = zoomButtons.enter()
                        .append('svg:g')
                        .attr("class", "htc-zoom-item")
                        .attr("transform", function(d, i){
                            return "translate(" + i * _itemHeight + "," + "0)"
                        })
                        .on("mouseover", function(){
                            d3.select(this).classed('hover', true);
                        })
                        .on("mouseout", function(){
                            d3.select(this).classed('hover', false);
                        })
                        .on("click", function(d){
                            var interval = d.count * fixedTimes[d.type];
                            dispatch.zoom(interval);
                        })

        zoomEnter.append('rect')
                .attr({ "rx":  0, "ry": 0 })
                .attr("width", _itemHeight)
                .attr("height", _itemHeight)

        zoomEnter.append("text")
            .attr('text-anchor', 'middle')
            .attr("x", _itemHeight/2)
            .attr("y", function(){return _itemHeight / 2 + (this.clientHeight?this.clientHeight:13) / 2 - 2;})
            .text(function(d){
                return d.text;
            });

        zoomButtons.exit().remove();

    }

    this.buttons = function(_){
        if(!arguments.length) return _buttons;
        _buttons = _;
        return this;
    }

    this.extent = function(_){
        if(!arguments.length) return _extent;
        _extent = _;
        return this;
    };

    this.margin = function(_){
        if(!arguments.length) return _margin;
        _margin = _;
        return this;
    };

    this.dispatch = dispatch;


}