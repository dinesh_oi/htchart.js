htc.namespace('htc.Renderer.Stack');

htc.Renderer.Stack = htc.Class.create( htc.Renderer, {

    name: 'stack',

    defaults: function($super) {

        return htc.extend( $super(), {
            fill: true,
            stroke: false,
            unstack: false
        } );
    },

    seriesPathFactory: function() {

        var graph = this.graph;

        var factory = d3.svg.area()
            .x( function(d) { return graph.x(graph.mapDataX(d)); } )
            .y0( function(d) { return graph.height;  } )
            .y1( function(d) { return graph.y(d.y + Number(d.y0)) } )
            .interpolate(this.graph.interpolation).tension(this.tension);

        factory.defined && factory.defined( function(d) { return d.y !== null } );
        return factory;
    }
} );

