htc.namespace('htc.Axis.X');

var DEFAULT_XAXIS_OPTIONS = {
        labels: {
            enabled : true,
            align   : true,
            format  : null,
            orientation: 'bottom'
        },
        title: {
            align: "middle",
            text: null,
        },
        grid : {
            enabled: true,
            color: '#E0E0E0',
            dashedStyle: 'solid',
            lineWidth: 2
        },
        id: "x",
        max : null,
        min : null,
        tickColor: '#C0D0E0',
        tickSize   : 4,
        tickValues : null,
        ticksTreatment : 'plain',
        pixelsPerTick  : 100,
        staticTicks: null,
        tickStep   : null,
        ordinal: true,
        width: null,
        height: null
    }

htc.Axis.X = function(graph, args) {

    var self = this;
    var berthRate = 0.00;
    this.graph = graph;

    this.initialize = function(args) {
        this.options(args);
        var opts = this.opts;

        if (args.element) {
            this.element = args.element;
            this._discoverSize(args.element, args);

            this.vis = d3.select(args.element)
                .append("svg:svg")
                .attr('height', this.height)
                .attr('width', this.width)
                .attr('class', 'htc_graph x_axis_d3');

            this.element = this.vis[0][0];
            this.element.style.position = 'relative';
            this.setSize({ width: opts.width, height: opts.height });
        } else {
            this.vis = this.graph.vis;
            this.setSize({ width: opts.width, height: opts.height });
        }

        this.opts.id = htc.nameToId(opts.name || 'X Axis');
        this.graph.onUpdate(this.opts.id, function() { self.render() });

    };


    this.setSize = function(args) {
        args = args || {};
        this._discoverSize(args);

        this.vis.attr('height', this.height)
                .attr('width',  this.width * (1 + berthRate));

        if(this.element){
            var berth = Math.floor(this.width * berthRate / 2);
            this.element.style.left = -1 * berth + 'px';
        }
    };

    this._hasTimeField = function(){
        return true;
    }

    this.options = function(args, refresh){
        if(!arguments.length) return this.opts;
        var cp =  htc.deepMerge(this.opts || DEFAULT_XAXIS_OPTIONS, args, true);
        this.opts = cp;

        if(cp.ordinal && cp.labels.format)
            this.tickFormat = function(x){
                return htc.formats[cp.labels.format](htc.weekday.invert(x));
            }
        else
            this.tickFormat = function(x){ return x };

        if(refresh)
            this.render();
        return this;
    }

    this.render = function() {
        var axis = d3.svg.axis().scale(this.graph.x).orient(this.opts.labels.orientation);
        axis.tickFormat(this.tickFormat);
        if (this.opts.tickValues) axis.tickValues(this.opts.tickValues);

        this.ticks   = this.opts.staticTicks || Math.floor(this.width/this.opts.pixelsPerTick);
        this._origin = this.opts.min || this.graph.x(0);

        var xberth = 0,
            yberth = this.graph.height,
            transform;

        if (this.opts.labels.orientation == 'top') {
            transform = 'translate(' + xberth + ',' + yberth + ')';
        } else {
            var yOffset = this.graph.height;
            transform = 'translate(' + xberth + ', ' +  yberth + ')';
        }

        if (this.element) {
            this.vis.selectAll('*').remove();
        }

        if(this.opts.labels.enabled)
          this.vis
            .append("svg:g")
            .attr("class", ["x_ticks_d3", this.opts.ticksTreatment].join(" "))
            .attr("transform", transform)
            .call(axis.ticks(this.ticks, this.opts.tickStep).tickSize(this.opts.tickSize)
                    .tickFormat(this.tickFormat));

        var gridSize = (this.opts.labels.orientation == 'bottom' ? 1 : -1) * this.graph.height;

        if(this.opts.grid.enabled)
              this.graph.vis.append("svg:g").attr("class", "x_grid_d3")
                  .call(axis.ticks(this.ticks).tickSubdivide(0)
                          .tickSize(gridSize).tickFormat(''));

    };

    this._discoverSize = function(args) {
        var element;
        this.element && ( element = this.element.parentNode);

        if (typeof window !== 'undefined' && element) {

            var style = window.getComputedStyle(element, null);
            var elementHeight = parseInt(style.getPropertyValue('height'), 10);

            if (!args.auto) {
                var elementWidth = parseInt(style.getPropertyValue('width'), 10);
            }
        }

        this.width = (args.width || elementWidth || this.graph.width) * (1 - berthRate);
        this.height = args.height || elementHeight || 40;
    };

    this.enableLabelGrid = function(flag){
        this.options({
            labels: {
                enabled: flag
            }, grid: {
                enabled: flag
            }
        });
    }

    this.enableLabel = function(flag){
        this.options({ labels: { enabled: flag }});
        return self;
    }

    this.enableGrid = function(flag){
        this.options({ grid: { enabled: flag }});
        return self;
    }

    this.initialize(args);
};

