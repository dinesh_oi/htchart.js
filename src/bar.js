htc.namespace('htc.Renderer.Bar');

htc.Renderer.Bar = htc.Class.create( htc.Renderer, {

    name: 'bar',

    defaults: function($super) {

        var defaults = htc.extend( $super(), {
            gapSize: 1,
            unstack: false
        } );

        delete defaults.tension;
        return defaults;
    },

    initialize: function($super, args) {
        args = args || {};
        this.gapSize  = args.gapSize || this.gapSize;
        this.dispatch = d3.dispatch('tooltipShow', 'tooltipHide')
        $super(args);
    },

    domain: function($super) {
        var domain = $super();
        var frequentInterval = this._frequentInterval(this.graph.stackedData.slice(-1).shift());
        domain.x[1] += Number(frequentInterval.magnitude);

        return domain;
    },

    barWidth: function(series) {
        return (this.gapSize + .4)*(this.width)/series.stack.length;
    },

    renderBars: function(vis, series){
        var graph      = this.graph;
        var barWidth   = this.barWidth(series.active()[0]);
        var barXOffset = 0;
        var margin     = this.padding;
        var self = this;


        series.forEach( function(series) {

            if (series.disabled) return;

            var p0 = series.stack[0],
                _ = series,
                X = graph.x,
                Y = graph.y,
                color = series.color,
                barXOffset = -barWidth * 0.5,
                data = series.stack || series.data;

            this.bars = vis.selectAll("rect").data(data)

            var baseY = Y.domain()[0];

            var mergeNodes = function(nodes){

                return nodes
                    .attr("x", function(d) {
                        return X(d.x) + barXOffset;
                    })
                    .attr("y", function(d) {
                        return Y(Math.max(0, d.y));
                    })
                    .attr("width", barWidth)
                    .attr("height", function(d) {
                        return Math.abs(Y(d.y) - Y(Math.max(0, baseY)) );
                    })
                    .attr('fill',function(d){
                        return d.y > 0 ? color : 'brown';
                    });
            }

            // UPDATE BARS
            mergeNodes(this.bars)

            // APPEND NEW BARS
            mergeNodes( this.bars.enter().append("svg:rect") )
                .on("mouseover", function(){
                    d3.select(this).classed('hover', true);
                })
                 .on("mouseout", function(){
                    d3.select(this).classed('hover', false);
                })

            // REMOVE OLD BARS
            this.bars.exit().remove();

        }, this );

    },

    onMouseLeave: function(chart){
         if (chart._tooltipGroup !== null && chart._tooltipGroup !== undefined) {
                chart._tooltipGroup.remove();
            }
    },

    render: function(args) {
        args = args || {};
        var graph = this.graph;
        var series = args.series || graph.series;
        var vis = args.vis || graph.vis;
        this.renderBars(vis, series);
    },

    _frequentInterval: function(data) {

        var intervalCounts = {};

        for (var i = 0; i < data.length - 1; i++) {
            var interval = data[i + 1].x - data[i].x;
            intervalCounts[interval] = intervalCounts[interval] || 0;
            intervalCounts[interval]++;
        }


        var frequentInterval = { count: 0, magnitude: 1 };

        htc.keys(intervalCounts).forEach( function(i) {
            if (frequentInterval.count < intervalCounts[i]) {
                frequentInterval = {
                    count: intervalCounts[i],
                    magnitude: i
                };
            }
        } );

        return frequentInterval;
    }
});

