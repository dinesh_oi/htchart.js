(function(globalContext){
'use strict';
var d3 = globalContext.d3;

function isObject(obj) {
    return typeof obj === "object" && obj !== null;
}

var htc = {
    namespace: function(namespace, obj) {

        var parts = namespace.split('.');

        var parent = htc;

        for(var i = 1, length = parts.length; i < length; i++) {
            var currentPart = parts[i];
            parent[currentPart] = parent[currentPart] || {};
            parent = parent[currentPart];
        }
        return parent;
    },

    keys: function(obj) {
        var keys = [];
        for (var key in obj) keys.push(key);
        return keys;
    },

    deepMerge: function(destination, source, clone) {
        if(clone)
            destination = htc.clone(destination);

        if(destination && source) {
            for(var key in source) {
                if(source.hasOwnProperty(key)) {
                    if(isObject(source[key]) && isObject(destination[key])) {
                        htc.deepMerge(destination[key], source[key], false);
                    } else {
                        destination[key] = source[key];
                    }
                }
            }
        }
        return destination;
    },
    extend: function(destination, source) {

        for (var property in source) {
            destination[property] = source[property];
        }
        return destination;
    },

    clone: function(obj) {
        return JSON.parse(JSON.stringify(obj));
    }
};

htc.formats = {
    millisecond : d3.time.format("%H:%M:%S.%L"),
    second      : d3.time.format("%H:%M:%S"),
    minute      : d3.time.format("%H:%M"),
    hour        : d3.time.format("%H:%M"),
    day         : d3.time.format("%e. %b"),
    week        : d3.time.format("%e. %b"),
    month       : d3.time.format("%b \'%y"),
    year        : d3.time.format('%Y')
}

if (typeof module !== 'undefined' && module.exports) {
    var d3 = require('d3');
    module.exports = htc;
}

/* Adapted from https://github.com/Jakobo/PTClass */

/*
Copyright (c) 2005-2010 Sam Stephenson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/* Based on Alex Arnell's inheritance implementation. */
/** section: Language
 * class Class
 *
 *  Manages Prototype's class-based OOP system.
 *
 *  Refer to Prototype's web site for a [tutorial on classes and
 *  inheritance](http://prototypejs.org/learn/class-inheritance).
**/
(function(globalContext) {
/* ------------------------------------ */
/* Import from object.js                */
/* ------------------------------------ */
var _toString = Object.prototype.toString,
    NULL_TYPE = 'Null',
    UNDEFINED_TYPE = 'Undefined',
    BOOLEAN_TYPE = 'Boolean',
    NUMBER_TYPE = 'Number',
    STRING_TYPE = 'String',
    OBJECT_TYPE = 'Object',
    FUNCTION_CLASS = '[object Function]';
function isFunction(object) {
  return _toString.call(object) === FUNCTION_CLASS;
}
function extend(destination, source) {
  for (var property in source) if (source.hasOwnProperty(property)) // modify protect primitive slaughter
    destination[property] = source[property];
  return destination;
}
function keys(object) {
  if (Type(object) !== OBJECT_TYPE) { throw new TypeError(); }
  var results = [];
  for (var property in object) {
    if (object.hasOwnProperty(property)) {
      results.push(property);
    }
  }
  return results;
}
function Type(o) {
  switch(o) {
    case null: return NULL_TYPE;
    case (void 0): return UNDEFINED_TYPE;
  }
  var type = typeof o;
  switch(type) {
    case 'boolean': return BOOLEAN_TYPE;
    case 'number':  return NUMBER_TYPE;
    case 'string':  return STRING_TYPE;
  }
  return OBJECT_TYPE;
}
function isUndefined(object) {
  return typeof object === "undefined";
}
/* ------------------------------------ */
/* Import from Function.js              */
/* ------------------------------------ */
var slice = Array.prototype.slice;
function argumentNames(fn) {
  var names = fn.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1]
    .replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, '')
    .replace(/\s+/g, '').split(',');
  return names.length == 1 && !names[0] ? [] : names;
}
function wrap(fn, wrapper) {
  var __method = fn;
  return function() {
    var a = update([bind(__method, this)], arguments);
    return wrapper.apply(this, a);
  }
}
function update(array, args) {
  var arrayLength = array.length, length = args.length;
  while (length--) array[arrayLength + length] = args[length];
  return array;
}
function merge(array, args) {
  array = slice.call(array, 0);
  return update(array, args);
}
function bind(fn, context) {
  if (arguments.length < 2 && isUndefined(arguments[0])) return this;
  var __method = fn, args = slice.call(arguments, 2);
  return function() {
    var a = merge(args, arguments);
    return __method.apply(context, a);
  }
}

/* ------------------------------------ */
/* Import from Prototype.js             */
/* ------------------------------------ */
var emptyFunction = function(){};

var Class = (function() {

  // Some versions of JScript fail to enumerate over properties, names of which
  // correspond to non-enumerable properties in the prototype chain
  var IS_DONTENUM_BUGGY = (function(){
    for (var p in { toString: 1 }) {
      // check actual property name, so that it works with augmented Object.prototype
      if (p === 'toString') return false;
    }
    return true;
  })();

  function subclass() {};
  function create() {
    var parent = null, properties = [].slice.apply(arguments);
    if (isFunction(properties[0]))
      parent = properties.shift();

    function klass() {
      this.initialize.apply(this, arguments);
    }

    extend(klass, Class.Methods);
    klass.superclass = parent;
    klass.subclasses = [];

    if (parent) {
      subclass.prototype = parent.prototype;
      klass.prototype = new subclass;
      try { parent.subclasses.push(klass) } catch(e) {}
    }

    for (var i = 0, length = properties.length; i < length; i++)
      klass.addMethods(properties[i]);

    if (!klass.prototype.initialize)
      klass.prototype.initialize = emptyFunction;

    klass.prototype.constructor = klass;
    return klass;
  }

  function addMethods(source) {
    var ancestor   = this.superclass && this.superclass.prototype,
        properties = keys(source);

    // IE6 doesn't enumerate `toString` and `valueOf` (among other built-in `Object.prototype`) properties,
    // Force copy if they're not Object.prototype ones.
    // Do not copy other Object.prototype.* for performance reasons
    if (IS_DONTENUM_BUGGY) {
      if (source.toString != Object.prototype.toString)
        properties.push("toString");
      if (source.valueOf != Object.prototype.valueOf)
        properties.push("valueOf");
    }

    for (var i = 0, length = properties.length; i < length; i++) {
      var property = properties[i], value = source[property];
      if (ancestor && isFunction(value) &&
          argumentNames(value)[0] == "$super") {
        var method = value;
        value = wrap((function(m) {
          return function() { return ancestor[m].apply(this, arguments); };
        })(property), method);

        value.valueOf = bind(method.valueOf, method);
        value.toString = bind(method.toString, method);
      }
      this.prototype[property] = value;
    }

    return this;
  }

  return {
    create: create,
    keys: keys,
    Methods: {
      addMethods: addMethods
    }
  };
})();

if (globalContext.exports) {
  globalContext.exports.Class = Class;
}
else {
  globalContext.Class = Class;
}
})(htc);



htc.namespace('htc.Compat.ClassList');

htc.Compat.ClassList = function() {

  /* adapted from http://purl.eligrey.com/github/classList.js/blob/master/classList.js */

  if (typeof document !== "undefined" && !("classList" in document.createElement("a"))) {

  (function (view) {

  "use strict";

  var
      classListProp = "classList"
    , protoProp = "prototype"
    , elemCtrProto = (view.HTMLElement || view.Element)[protoProp]
    , objCtr = Object
    , strTrim = String[protoProp].trim || function () {
      return this.replace(/^\s+|\s+$/g, "");
    }
    , arrIndexOf = Array[protoProp].indexOf || function (item) {
      var
          i = 0
        , len = this.length
      ;
      for (; i < len; i++) {
        if (i in this && this[i] === item) {
          return i;
        }
      }
      return -1;
    }
    // Vendors: please allow content code to instantiate DOMExceptions
    , DOMEx = function (type, message) {
      this.name = type;
      this.code = DOMException[type];
      this.message = message;
    }
    , checkTokenAndGetIndex = function (classList, token) {
      if (token === "") {
        throw new DOMEx(
            "SYNTAX_ERR"
          , "An invalid or illegal string was specified"
        );
      }
      if (/\s/.test(token)) {
        throw new DOMEx(
            "INVALID_CHARACTER_ERR"
          , "String contains an invalid character"
        );
      }
      return arrIndexOf.call(classList, token);
    }
    , ClassList = function (elem) {
      var
          trimmedClasses = strTrim.call(elem.className)
        , classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
        , i = 0
        , len = classes.length
      ;
      for (; i < len; i++) {
        this.push(classes[i]);
      }
      this._updateClassName = function () {
        elem.className = this.toString();
      };
    }
    , classListProto = ClassList[protoProp] = []
    , classListGetter = function () {
      return new ClassList(this);
    }
  ;
  // Most DOMException implementations don't allow calling DOMException's toString()
  // on non-DOMExceptions. Error's toString() is sufficient here.
  DOMEx[protoProp] = Error[protoProp];
  classListProto.item = function (i) {
    return this[i] || null;
  };
  classListProto.contains = function (token) {
    token += "";
    return checkTokenAndGetIndex(this, token) !== -1;
  };
  classListProto.add = function (token) {
    token += "";
    if (checkTokenAndGetIndex(this, token) === -1) {
      this.push(token);
      this._updateClassName();
    }
  };
  classListProto.remove = function (token) {
    token += "";
    var index = checkTokenAndGetIndex(this, token);
    if (index !== -1) {
      this.splice(index, 1);
      this._updateClassName();
    }
  };
  classListProto.toggle = function (token) {
    token += "";
    if (checkTokenAndGetIndex(this, token) === -1) {
      this.add(token);
    } else {
      this.remove(token);
    }
  };
  classListProto.toString = function () {
    return this.join(" ");
  };

  if (objCtr.defineProperty) {
    var classListPropDesc = {
        get: classListGetter
      , enumerable: true
      , configurable: true
    };
    try {
      objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
    } catch (ex) { // IE 8 doesn't support enumerable:true
      if (ex.number === -0x7FF5EC54) {
        classListPropDesc.enumerable = false;
        objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
      }
    }
  } else if (objCtr[protoProp].__defineGetter__) {
    elemCtrProto.__defineGetter__(classListProp, classListGetter);
  }

  }(window));

  }
};

if ( (typeof htc_NO_COMPAT !== "undefined" && !htc_NO_COMPAT) || typeof htc_NO_COMPAT === "undefined") {
  new htc.Compat.ClassList();
}

// Copied from https://gist.github.com/mbostock/5827353


htc.weekday = (function() {

  // Returns the weekday number for the given date relative to January 1, 1970.
  function weekday(date) {
    var weekdays = weekdayOfYear(date),
        year = date.getFullYear();
    while (--year >= 1970) weekdays += weekdaysInYear(year);
    return weekdays;
  }

  // Returns the date for the specified weekday number relative to January 1, 1970.
  weekday.invert = function(weekdays) {
    var year = 1970,
        yearWeekdays;

    // Compute the year.
    while ((yearWeekdays = weekdaysInYear(year)) <= weekdays) {
      ++year;
      weekdays -= yearWeekdays;
    }

    // Compute the date from the remaining weekdays.
    var days = weekdays % 5,
        day0 = ((new Date(year, 0, 1)).getDay() + 6) % 7;
    if (day0 + days > 4) days += 2;
    return new Date(year, 0, (weekdays / 5 | 0) * 7 + days + 1);
  };

  // Returns the number of weekdays in the specified year.
  function weekdaysInYear(year) {
    return weekdayOfYear(new Date(year, 11, 31)) + 1;
  }

  // Returns the weekday number for the given date relative to the start of the year.
  function weekdayOfYear(date) {
    var days = d3.time.dayOfYear(date),
        weeks = days / 7 | 0,
        day0 = (d3.time.year(date).getDay() + 6) % 7,
        day1 = day0 + days - weeks * 7;
    return Math.max(0, days - weeks * 2
        - (day0 <= 5 && day1 >= 5 || day0 <= 12 && day1 >= 12) // extra saturday
        - (day0 <= 6 && day1 >= 6 || day0 <= 13 && day1 >= 13)); // extra sunday
  }

  return weekday;

})();

htc.nameToId = function(name){
    return name.toLowerCase().replace(/[\s]/g, "_").replace(/[\.']/g, "");
};

htc.each = function(obj, iterator, context) {
    if (obj == null) return;
    obj.forEach(iterator, context);
  };

htc.values = function(obj){
    var values = [];
    for(var key in obj)
        values.push(obj[key]);
    return values;
}

htc.safe_number = function(){
    return Math.floor(Math.random() * 10000);
}

/* Numbers that are undefined, null or NaN, convert them to zeros.
*/
htc.NaNtoZero = function(n) {
    if (typeof n !== 'number'
        || isNaN(n)
        || n === null
        || n === Infinity) return 0;

    return n;
};

htc.any = function(obj, iterator, context) {
    var result = false;
    if (obj == null) return result;
    htc.each(obj, function(value, index, list) {
      if (result || (result = iterator.call(context, value, index, list))) return {};
    });
    return !!result;
 };

// taken from underscore.js
htc.contains = function(obj, target) {
    if (obj == null) return false;
    var nativeIndexOf = Array.indexOf;
    if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
    return htc.any(obj, function(value) {
      return value === target;
    });
};

htc.omit = function(obj) {
    var copy = {};
    var keys = Array.prototype.slice.call(arguments, 1);
    for (var key in obj) {
      if (!htc.contains(keys, key)) copy[key] = obj[key];
    }
    return copy;
};


htc.windowSize = function() {
    // Sane defaults
    var size = {width: 640, height: 480};

    // Earlier IE uses Doc.body
    if (document.body && document.body.offsetWidth) {
        size.width = document.body.offsetWidth;
        size.height = document.body.offsetHeight;
    }

    // IE can use depending on mode it is in
    if (document.compatMode=='CSS1Compat' &&
        document.documentElement &&
        document.documentElement.offsetWidth ) {
        size.width = document.documentElement.offsetWidth;
        size.height = document.documentElement.offsetHeight;
    }

    // Most recent browsers use
    if (window.innerWidth && window.innerHeight) {
        size.width = window.innerWidth;
        size.height = window.innerHeight;
    }
    return (size);
};



// Easy way to bind multiple functions to window.onresize
// TODO: give a way to remove a function after its bound, other than removing all of them
htc.windowResize = function(fun){
  if (fun === undefined) return;
  var oldresize = window.onresize;

  window.onresize = function(e) {
    if (typeof oldresize == 'function') oldresize(e);
    fun(e);
  }
}
htc.namespace('htc.Fixtures.Color');

htc.Fixtures.Color = function() {

    this.schemes = {};

    this.schemes.spectrum14 = [
        '#ecb796',
        '#dc8f70',
        '#b2a470',
        '#92875a',
        '#716c49',
        '#d2ed82',
        '#bbe468',
        '#a1d05d',
        '#e7cbe6',
        '#d8aad6',
        '#a888c2',
        '#9dc2d3',
        '#649eb9',
        '#387aa3'
    ].reverse();

    this.schemes.spectrum2000 = [
        '#57306f',
        '#514c76',
        '#646583',
        '#738394',
        '#6b9c7d',
        '#84b665',
        '#a7ca50',
        '#bfe746',
        '#e2f528',
        '#fff726',
        '#ecdd00',
        '#d4b11d',
        '#de8800',
        '#de4800',
        '#c91515',
        '#9a0000',
        '#7b0429',
        '#580839',
        '#31082b'
    ];

    this.schemes.spectrum2001 = [
        '#2f243f',
        '#3c2c55',
        '#4a3768',
        '#565270',
        '#6b6b7c',
        '#72957f',
        '#86ad6e',
        '#a1bc5e',
        '#b8d954',
        '#d3e04e',
        '#ccad2a',
        '#cc8412',
        '#c1521d',
        '#ad3821',
        '#8a1010',
        '#681717',
        '#531e1e',
        '#3d1818',
        '#320a1b'
    ];

    this.schemes.classic9 = [
        '#423d4f',
        '#4a6860',
        '#848f39',
        '#a2b73c',
        '#ddcb53',
        '#c5a32f',
        '#7d5836',
        '#963b20',
        '#7c2626',
        '#491d37',
        '#2f254a'
    ].reverse();

    this.schemes.httpStatus = {
        503: '#ea5029',
        502: '#d23f14',
        500: '#bf3613',
        410: '#efacea',
        409: '#e291dc',
        403: '#f457e8',
        408: '#e121d2',
        401: '#b92dae',
        405: '#f47ceb',
        404: '#a82a9f',
        400: '#b263c6',
        301: '#6fa024',
        302: '#87c32b',
        307: '#a0d84c',
        304: '#28b55c',
        200: '#1a4f74',
        206: '#27839f',
        201: '#52adc9',
        202: '#7c979f',
        203: '#a5b8bd',
        204: '#c1cdd1'
    };

    this.schemes.colorwheel = [
        '#b5b6a9',
        '#858772',
        '#785f43',
        '#96557e',
        '#4682b4',
        '#65b9ac',
        '#73c03a',
        '#cb513a'
    ].reverse();

    this.schemes.cool = [
        '#5e9d2f',
        '#73c03a',
        '#4682b4',
        '#7bc3b8',
        '#a9884e',
        '#c1b266',
        '#a47493',
        '#c09fb5'
    ];

    this.schemes.munin = [
        '#00cc00',
        '#0066b3',
        '#ff8000',
        '#ffcc00',
        '#330099',
        '#990099',
        '#ccff00',
        '#ff0000',
        '#808080',
        '#008f00',
        '#00487d',
        '#b35a00',
        '#b38f00',
        '#6b006b',
        '#8fb300',
        '#b30000',
        '#bebebe',
        '#80ff80',
        '#80c9ff',
        '#ffc080',
        '#ffe680',
        '#aa80ff',
        '#ee00cc',
        '#ff8080',
        '#666600',
        '#ffbfff',
        '#00ffcc',
        '#cc6699',
        '#999900'
    ];
};


htc.namespace("htc.Color.Palette");

htc.Color.Palette = function(args) {

    var color = new htc.Fixtures.Color();

    args = args || {};
    this.schemes = {};

    this.scheme = color.schemes[args.scheme] || args.scheme || color.schemes.colorwheel;
    this.runningIndex = 0;
    this.generatorIndex = 0;

    if (args.interpolatedStopCount) {
        var schemeCount = this.scheme.length - 1;
        var i, j, scheme = [];
        for (i = 0; i < schemeCount; i++) {
            scheme.push(this.scheme[i]);
            var generator = d3.interpolateHsl(this.scheme[i], this.scheme[i + 1]);
            for (j = 1; j < args.interpolatedStopCount; j++) {
                scheme.push(generator((1 / args.interpolatedStopCount) * j));
            }
        }
        scheme.push(this.scheme[this.scheme.length - 1]);
        this.scheme = scheme;
    }
    this.rotateCount = this.scheme.length;

    this.color = function(key) {
        return this.scheme[key] || this.scheme[this.runningIndex++] || this.interpolateColor() || '#808080';
    };

    this.interpolateColor = function() {
        if (!Array.isArray(this.scheme)) return;
        var color;
        if (this.generatorIndex == this.rotateCount * 2 - 1) {
            color = d3.interpolateHsl(this.scheme[this.generatorIndex], this.scheme[0])(0.5);
            this.generatorIndex = 0;
            this.rotateCount *= 2;
        } else {
            color = d3.interpolateHsl(this.scheme[this.generatorIndex], this.scheme[this.generatorIndex + 1])(0.5);
            this.generatorIndex++;
        }
        this.scheme.push(color);
        return color;
    };

};
htc.namespace('htc.Axis.X');

var DEFAULT_XAXIS_OPTIONS = {
        labels: {
            enabled : true,
            align   : true,
            format  : null,
            orientation: 'bottom'
        },
        title: {
            align: "middle",
            text: null,
        },
        grid : {
            enabled: true,
            color: '#E0E0E0',
            dashedStyle: 'solid',
            lineWidth: 2
        },
        id: "x",
        max : null,
        min : null,
        tickColor: '#C0D0E0',
        tickSize   : 4,
        tickValues : null,
        ticksTreatment : 'plain',
        pixelsPerTick  : 100,
        staticTicks: null,
        tickStep   : null,
        ordinal: true,
        width: null,
        height: null
    }

htc.Axis.X = function(graph, args) {

    var self = this;
    var berthRate = 0.00;
    this.graph = graph;

    this.initialize = function(args) {
        this.options(args);
        var opts = this.opts;

        if (args.element) {
            this.element = args.element;
            this._discoverSize(args.element, args);

            this.vis = d3.select(args.element)
                .append("svg:svg")
                .attr('height', this.height)
                .attr('width', this.width)
                .attr('class', 'htc_graph x_axis_d3');

            this.element = this.vis[0][0];
            this.element.style.position = 'relative';
            this.setSize({ width: opts.width, height: opts.height });
        } else {
            this.vis = this.graph.vis;
            this.setSize({ width: opts.width, height: opts.height });
        }

        this.opts.id = htc.nameToId(opts.name || 'X Axis');
        this.graph.onUpdate(this.opts.id, function() { self.render() });

    };


    this.setSize = function(args) {
        args = args || {};
        this._discoverSize(args);

        this.vis.attr('height', this.height)
                .attr('width',  this.width * (1 + berthRate));

        if(this.element){
            var berth = Math.floor(this.width * berthRate / 2);
            this.element.style.left = -1 * berth + 'px';
        }
    };

    this._hasTimeField = function(){
        return true;
    }

    this.options = function(args, refresh){
        if(!arguments.length) return this.opts;
        var cp =  htc.deepMerge(this.opts || DEFAULT_XAXIS_OPTIONS, args, true);
        this.opts = cp;

        if(cp.ordinal && cp.labels.format)
            this.tickFormat = function(x){
                return htc.formats[cp.labels.format](htc.weekday.invert(x));
            }
        else
            this.tickFormat = function(x){ return x };

        if(refresh)
            this.render();
        return this;
    }

    this.render = function() {
        var axis = d3.svg.axis().scale(this.graph.x).orient(this.opts.labels.orientation);
        axis.tickFormat(this.tickFormat);
        if (this.opts.tickValues) axis.tickValues(this.opts.tickValues);

        this.ticks   = this.opts.staticTicks || Math.floor(this.width/this.opts.pixelsPerTick);
        this._origin = this.opts.min || this.graph.x(0);

        var xberth = 0,
            yberth = this.graph.height,
            transform;

        if (this.opts.labels.orientation == 'top') {
            transform = 'translate(' + xberth + ',' + yberth + ')';
        } else {
            var yOffset = this.graph.height;
            transform = 'translate(' + xberth + ', ' +  yberth + ')';
        }

        if (this.element) {
            this.vis.selectAll('*').remove();
        }

        if(this.opts.labels.enabled)
          this.vis
            .append("svg:g")
            .attr("class", ["x_ticks_d3", this.opts.ticksTreatment].join(" "))
            .attr("transform", transform)
            .call(axis.ticks(this.ticks, this.opts.tickStep).tickSize(this.opts.tickSize)
                    .tickFormat(this.tickFormat));

        var gridSize = (this.opts.labels.orientation == 'bottom' ? 1 : -1) * this.graph.height;

        if(this.opts.grid.enabled)
              this.graph.vis.append("svg:g").attr("class", "x_grid_d3")
                  .call(axis.ticks(this.ticks).tickSubdivide(0)
                          .tickSize(gridSize).tickFormat(''));

    };

    this._discoverSize = function(args) {
        var element;
        this.element && ( element = this.element.parentNode);

        if (typeof window !== 'undefined' && element) {

            var style = window.getComputedStyle(element, null);
            var elementHeight = parseInt(style.getPropertyValue('height'), 10);

            if (!args.auto) {
                var elementWidth = parseInt(style.getPropertyValue('width'), 10);
            }
        }

        this.width = (args.width || elementWidth || this.graph.width) * (1 - berthRate);
        this.height = args.height || elementHeight || 40;
    };

    this.enableLabelGrid = function(flag){
        this.options({
            labels: {
                enabled: flag
            }, grid: {
                enabled: flag
            }
        });
    }

    this.enableLabel = function(flag){
        this.options({ labels: { enabled: flag }});
        return self;
    }

    this.enableGrid = function(flag){
        this.options({ grid: { enabled: flag }});
        return self;
    }

    this.initialize(args);
};

htc.namespace('htc.Axis.Y');

htc.Axis.Y = function(graph, args){

    var _showGrid = true,
        _showLabel = true,
        _showRef = true,
        _highlightZero = false;

    this.graph = graph;

    this.initialize = function(args) {

        this.orientation = args.orientation || 'left';
        this.opposite = args.opposite || false;

        this.margin = args.margin || { top: 0, bottom: 0, right: 0, left: 0};

        this.pixelsPerTick = args.pixelsPerTick || 75;
        if (args.ticks) this.staticTicks = args.ticks;
        if (args.tickValues) this.tickValues = args.tickValues;

        this.tickSize = args.tickSize || 4;
        this.ticksTreatment = args.ticksTreatment || 'plain';

        this.tickFormat = args.tickFormat || function(y) { return Number(y) };
        this.axisLabel  = args.title ? args.title.text : args.label

        this.berthRate = 0.00;
        this.plotLines = args.plotLines || [];

        if(!(typeof args.showRef == 'undefined'))
            this.showRef(args.showRef)

        if(!(typeof args.showGrid == 'undefined'))
            this.showGrid(args.showGrid);

        if (args.element) {

            this.element = args.element;
            this.vis = d3.select(args.element)
                .append("svg:svg")
                .attr('class', 'htc_graph y_axis');

            this.element = this.vis[0][0];
            this.element.style.position = 'relative';

            this.setSize({ width: args.width, height: args.height });

        } else {
            this.vis = this.graph.vis;
            this.setSize({ width: args.width, height: args.height });
        }

        var self = this;
        this.id = htc.nameToId(args.name || 'Y Axis');
        this.graph.onUpdate(this.id, function() { self.render() } );
    }

    this.setSize = function(args) {

        args = args || {};

        if (typeof window !== 'undefined' && this.element && this.element.parentNode) {

            var style = window.getComputedStyle(this.element.parentNode, null);
            var elementWidth = parseInt(style.getPropertyValue('width'), 10);

            if (!args.auto) {
                var elementHeight = parseInt(style.getPropertyValue('height'), 10);
            }
        }

        this.width  = args.width || elementWidth || 30;
        this.height = args.height || elementHeight || this.graph.height;

        this.vis
            .attr('width', this.width)
            .attr('height', this.height * (1 + this.berthRate));

        var berth = this.height * this.berthRate;

        if (this.orientation == 'left' && this.element) {
            this.element.style.top = -1 * berth + 'px';
        }
    }

    this.render = function(){
        this.ticks = this.staticTicks || Math.floor(this.graph.height / this.pixelsPerTick);
        this._origin = this.graph.y(0);
        var axis;

        if(_showRef){
            axis = this.drawAxis(this.graph.y);
            this.drawLabel();
        }

        if(_showGrid){
            axis = axis || d3.svg.axis();
            this.drawGrid(axis);
        }

        this.drawPlotLines(axis);

    }

    this._hasTimeField  = function(){
        return false;
    }

    this.drawPlotLines = function(axis){
        if(this.plotLines.length){
            var Y = this.graph.y,
                xscale = this.graph.x.range();

            var _g = this.vis.selectAll('.hori-lines line')

            if(_g.empty())
                _g = this.vis.append("g").attr('class', 'hori-lines')

            var _gEnter = _g.selectAll('line').data(this.plotLines);

                _gEnter.enter()
                    .append("svg:line")
                    .attr("x1", xscale[0])
                    .attr("x2", xscale[1])
                    .attr("y1", function(d){ return Y(d.value) })
                    .attr("y2", function(d){ return Y(d.value) })
                    .attr('stroke', function(d){
                        return d.color || '#609ac1';
                    });

            _gEnter.exit().remove();
        }
    }

    this.drawLabel = function(){
        if(!this.axisLabel)
            return;

        var Y_AXIS_LABEL_CLASS = 'y-axis-label';
        var g = this.vis;
        var axisYLab = g.selectAll("text."+ Y_AXIS_LABEL_CLASS);

        if(axisYLab.empty()){
            axisYLab = g.append('text')
                .attr("transform", "translate(" + 10 + "," + this.graph.height/2 + "),rotate(-90)")
                .attr('class', Y_AXIS_LABEL_CLASS)
                .attr('text-anchor', 'middle')
                .text(this.axisLabel);
        } else {
             axisYLab.text(this.axisLabel);
        }

    }

    this.drawAxis = function(scale) {
        var axis = d3.svg.axis().scale(scale).orient(this.orientation);
        axis.tickFormat(this.tickFormat);
        if (this.tickValues) axis.tickValues(this.tickValues);

        var translateX = this.width,
            berth = 0;

        if(this.opposite)
            translateX += this.graph.width - this.margin.right - this.width;

        var transform = 'translate(' + translateX + ', ' + berth + ')';

        if (this.element) {
            this.vis.selectAll('*').remove();
        }

        this.vis
            .append("svg:g")
            .attr("class", ["y_ticks", this.ticksTreatment].join(" "))
            .attr("transform", transform)
            .call(axis.ticks(this.ticks).tickSubdivide(0).tickSize(this.tickSize));

        return axis;
    }

    this.drawGrid = function(axis) {
        var gridSize = (this.orientation == 'right' ? 1 : -1) * this.graph.width;

        if(this.opposite) gridSize -= this.width;

        var translateX = this.width;
        var graph = this.graph;

        if(graph.y.domain()[1] * graph.y.domain()[0] < 0 )
            _highlightZero = true;


        this.graph.vis
            .append("svg:g")
            .classed("y_grid", true)
            .attr("transform", "translate(" + translateX + ',0)')
            .call(axis.ticks(this.ticks).tickSubdivide(0)
                    .tickSize(gridSize).tickFormat(''));

        if(_highlightZero){
            var yticks = this.vis.selectAll('.y_grid .tick')
                .filter(function() {
                    //this is because sometimes the 0 tick is a very small fraction, TODO: think of cleaner technique
                    return !parseFloat(Math.round(this.__data__*100000)/1000000) && (this.__data__ !== undefined)
                });
            yticks.classed('zero', true);
        }


    }

    this.showRef = function(_){
        if(!arguments.length) return _showRef;
        _showRef = _;
        return this;
    }

    this.showGrid = function(_){
        if(!arguments.length) return _showGrid;
        _showGrid = _;
        return this;
    }

    this.showLabel = function(_){
        if(!arguments.length) return _showLabel;
        _showLabel = _;
        return this;
    }

    return this.initialize(args);
};
htc.namespace("htc.Renderer");

htc.Renderer = htc.Class.create( {

    initialize: function(args) {
        this.graph = args.graph;
        this.tension = args.tension || this.tension;
        this.configure(args);
    },

    seriesPathFactory: function() {
        //implement in subclass
    },

    seriesStrokeFactory: function() {
        // implement in subclass
    },

    defaults: function() {
        return {
            tension: 0.8,
            strokeWidth: 2,
            unstack: true,
            padding: { top: 0.2, right: 0, bottom: 0.05, left: 0 },
            stroke: false,
            fill: false,
            width: 100,
            height: 10
        };
    },

    domain: function(data) {

        var stackedData = data || this.graph.stackedData || this.graph.stackData();
        var firstPoint = stackedData[0][0];

        if (firstPoint === undefined) {
            return { x: [null, null], y: [null, null] };
        }

        var xMin = firstPoint.x;
        var xMax = firstPoint.x;

        var yMin = firstPoint.y + firstPoint.y0;
        var yMax = firstPoint.y + firstPoint.y0;

        stackedData.forEach( function(series) {
            series.forEach( function(d) {
                if (d.y == null) return;
                var y = d.y + d.y0;
                if (y < yMin) yMin = y;
                if (y > yMax) yMax = y;
            });

            if (!series.length) return;

            if (series[0].x < xMin) xMin = series[0].x;
            if (series[series.length - 1].x > xMax) xMax = series[series.length - 1].x;
        } );

        return this.dataExtent({ x: [xMin, xMax], y: [yMin, yMax] });
    },

    dataExtent: function(extent){
        var yMin = extent.y[0], yMax = extent.y[1],
            xMin = extent.x[0], xMax = extent.x[1];

        // xMin -= (xMax - xMin) * this.padding.left;
        // xMax += (xMax - xMin) * this.padding.right;

        yMin = this.graph.min === 'auto' ? yMin : this.graph.min || 0;
        yMax = this.graph.max === undefined ? yMax : this.graph.max;

        if (this.graph.min === 'auto' || yMin < 0) {
            yMin -= (yMax - yMin) * this.padding.bottom;
        }

        if (this.graph.max === undefined) {
            yMax += (yMax - yMin) * this.padding.top;
        }

        return { x: [xMin, xMax], y: [yMin, yMax] };
    },

    render: function(args) {

        args = args || {};

        var graph = this.graph;
        var series = args.series || graph.series;

        var vis = args.vis || graph.vis;

        var data = series
            .filter(function(s) { return !s.disabled })
            .map(function(s) { return s.stack });

        var nodes = vis.selectAll("path").data(data);

        var factory = this.seriesPathFactory();

        nodes.attr("d", factory);

        var nodesEnter = nodes
            .enter().append("svg:path")
            .attr("d", factory);

        nodes.exit().remove();

        var i = 0;
        series.forEach( function(series) {
            if (series.disabled) return;
            series.path = nodes[0][i++];
            this._styleSeries(series);
        }, this );
    },

    _styleSeries: function(series) {

        var fill   = this.fill ? series.color : 'none';
        var stroke = this.stroke ? series.color : 'none';

        series.path.setAttribute('fill', fill);
        series.path.setAttribute('stroke', stroke);
        series.path.setAttribute('stroke-width', this.strokeWidth);
        if(series.dashed || this.dashed)
            series.path.setAttribute('stroke-dasharray', '1 1');
        series.path.setAttribute('class', series.className);
    },

    configure: function(args) {

        args = args || {};

        htc.keys(this.defaults()).forEach( function(key) {

            if (!args.hasOwnProperty(key)) {
                this[key] = this[key] || this.graph[key] || this.defaults()[key];
                return;
            }

            if (typeof this.defaults()[key] == 'object') {
                htc.keys(this.defaults()[key]).forEach( function(k) {
                    this[key] = this[key] || {};
                    this[key][k] =
                        args[key][k] !== undefined ? args[key][k] :
                        this[key][k] !== undefined ? this[key][k] :
                        this.defaults()[key][k];
                }, this );

            } else {
                this[key] =
                    args[key] !== undefined ? args[key] :
                    this[key] !== undefined ? this[key] :
                    this.graph[key] !== undefined ? this.graph[key] :
                    this.defaults()[key];
            }

        }, this );
    },

    setStrokeWidth: function(strokeWidth) {
        if (strokeWidth !== undefined) {
            this.strokeWidth = strokeWidth;
        }
    },

    setTension: function(tension) {
        if (tension !== undefined) {
            this.tension = tension;
        }
    }
} );


htc.namespace('htc.Renderer.Multi');

htc.Renderer.Multi = htc.Class.create( htc.Renderer, {

    name: 'multi',

    initialize: function($super, args) {

        $super(args);
    },

    defaults: function($super) {

        return htc.extend( $super(), {
            unstack: true,
            fill: false,
            stroke: true
        } );
    },

    domain: function($super) {
        this.graph.stackData();
        var domains = [];
        var groups = this._groups();

        groups.forEach( function(group) {
            var data = group.series
                .filter( function(s) { return !s.disabled } )
                .map( function(s) {
                    return s.stack;
                });

            if (!data.length) return;
            var domain = $super(data);
            domains.push(domain);
        });


        var xMin = d3.min(domains.map( function(d) { return d.x[0] } ));
        var xMax = d3.max(domains.map( function(d) { return d.x[1] } ));
        var yMin = d3.min(domains.map( function(d) { return d.y[0] } ));
        var yMax = d3.max(domains.map( function(d) { return d.y[1] } ));

        return { x: [xMin, xMax], y: [yMin, yMax] };
    },

    _groups: function() {

        var graph = this.graph;
        var renderGroups = {};

        graph.series.forEach( function(series) {

            if (series.disabled) return;

            if (!renderGroups[series.renderer]) {
                var className = series.renderer + '-' + 'wrap';
                var vis = graph.vis.select('g.chart-body.' + className)

                if(!vis[0][0])
                    vis = graph.vis
                                .append('svg:g')
                                .attr('class', 'chart-body ' + className)
                                // .attr("transform", "translate(" + graph.translateX + "," + graph.translateY + ")");

                var renderer = graph._renderers[series.renderer];

                renderGroups[series.renderer] = {
                    renderer: renderer,
                    series: [],
                    vis: vis
                };
            }

            renderGroups[series.renderer].series.push(series);

        }, this);

        var groups = [];

        Object.keys(renderGroups).forEach( function(key) {
            var group = renderGroups[key];
            groups.push(group);
        });

        return groups;
    },

    render: function() {

        this.graph.series.forEach( function(series) {
            if (!series.renderer) {
                throw new Error("Each series needs a renderer for graph 'multi' renderer");
            }
        });

        var groups = this._groups();
        this.groups = groups;

        groups.forEach( function(group) {

            var series = group.series
                .filter( function(series) { return !series.disabled } );

            series.active = function() { return series  };
            group.renderer.render({ series: series, vis: group.vis });
            series.forEach(function(s) {
                s.stack = s._stack || s.stack || s.data;
            });
        });
    }

} );

(function(htc){

htc.namespace('htc.Series');

htc.Series = htc.Class.create( Array, {

    initialize: function (data, scheme, options) {

        options = options || {};

        this.palette = new htc.Color.Palette({ 'scheme': scheme });

        this.timeBase = typeof(options.timeBase) === 'undefined' ?
            Math.floor(new Date().getTime() / 1000) :
            options.timeBase;

        var timeInterval = typeof(options.timeInterval) == 'undefined' ?
            1000 :
            options.timeInterval;

        this.setTimeInterval(timeInterval);

        if (data && (typeof(data) == "object") && Array.isArray(data) ) {
            data.forEach( function(item) {
                this.addItem(item);
            }, this );
        }

    },

    addItem: function(item) {
        item.name || (item.name = item.key);
        item.data || (item.data = item.values || []);

        switch(item.renderer){
            case 'candlestick':
                ohlcData(item);
                break;
            case 'marker':
                markerData(item);
                break;
            default:
                xyData(item);
                break;
        }

        if (typeof(item.name) === 'undefined') {
            throw('addItem() needs a name');
        }

        item.color = (item.color || this.palette.color(item.name));
        item.palette = this.palette;

        if(item.backfill){
         // backfill, if necessary
            if ((item.data.length === 0) && this.length) {
                this[0].data.forEach( function(plot) {
                    item.data.push({ x: plot.x, y: 0 });
                } );
            } else if (item.data.length === 0) {
                item.data.push({ x: this.timeBase - (this.timeInterval || 0), y: 0 });
            }
        }

        this.push(item);

        if (this.legend) {
            this.legend.addLine(this.itemByName(item.name));
        }
    },

    setTimeInterval: function(iv) {
        this.timeInterval = iv / 1000;
    },

    setTimeBase: function (t) {
        this.timeBase = t;
    }
} );

var xyData = function(item, extraMappings){
    var mappings = {
        getX      : item.getX      || function(d){ return new Date(d.x || d.time) },
        getY      : item.getY      || function(d){ return d.y },
        getY0     : item.getY0     || function(d){ return 0 }
    }

    item._timeField = 'time';
    item._hasCategoryFields = false;

    validateXYData(item, mappings);
}

var ohlcData = function(item){
    var tt = item._timeField = item.timeField || 'time';

    var mappings = {
        'getTime'   : item.getTime   || function(d){ return new Date(d.x || d[tt]) },
        'getOpen'   : item.getOpen   || function(d){ return d.open },
        'getClose'  : item.getClose  || function(d){ return d.close },
        'getLow'    : item.getLow    || function(d){ return d.low },
        'getHigh'   : item.getHigh   || function(d){ return d.high },
        'getX'      : item.getX      || function(d){ return new Date(d[tt]) },
        'getY'      : item.getY      || function(d){ return d.low },
        'getY0'     : item.getY0     || function(d){ return 0 },
    }

    item._hasCategoryFields = true;
    item._categoryFields = ['open', 'close', 'high', 'low'];

    validateOHLCData(item, mappings);
    return item;
}

var markerData = function(item){
    xyData(item);
    return item;
}

var validateXYData = function(s, mappings){
    if (!(s instanceof Object)) {
        throw "series element is not an object: " + s;
    }
    if (!(s.data)) {
        throw "series has no data: " + JSON.stringify(s);
    }
    if (!Array.isArray(s.data)) {
        throw "series data is not an array: " + JSON.stringify(s.data);
    }

    s.data = s.data.map(function(d){
        var newd = {
            x    : htc.weekday(mappings.getX(d)),
            y    : mappings.getY(d),
            y0   : (mappings.getY0(d) || 0),
            time : mappings.getX(d)
        }
        if(d.title || d.text){
            newd.title = d.title || d.y;
            newd.text = d.text || d.title || d.y;
        }
        return newd;
    });

    if(s.data.length > 0){
        var x = s.data[0].x;
        var y = s.data[0].y;

        if((typeof x != 'number') && !(x instanceof Date)){
            throw "x property of points should be number or date instead of " + (typeof x);
        }

        if (typeof y != 'number' && y !== null) {
            throw "y property of points should be numbers instead of " + (typeof y);
        }
    }

    if (s.data.length >= 3) {
        // probe to sanity check sort order
        if (s.data[2].x < s.data[1].x || s.data[1].x < s.data[0].x || s.data[s.data.length - 1].x < s.data[0].x) {
            throw "series data needs to be sorted on x values for series name: " + s.name;
        }
    }

    if(s.data.length > 0){
        // set series's domain in the terms of x and y values
        var x0 = s.data[0].x,
            x1 = s.data[s.data.length-1].x,
            y0 = s.data[0].y,
            y1 = s.data[s.data.length-1].y

        s.domain = {x: [x0, x1], y: [y0, y1] };

    }
}

var validateOHLCData = function(s, mappings){
    if (!(s instanceof Object)) {
        throw "series element is not an object: " + s;
    }
    if (!(s.data)) {
        throw "series has no data: " + JSON.stringify(s);
    }
    if (!Array.isArray(s.data)) {
        throw "series data is not an array: " + JSON.stringify(s.data);
    }

    s.data = s.data.map(function(d){
        return {
            x       : htc.weekday(mappings.getX(d)),
            y       : mappings.getY(d),
            y0      : mappings.getY0(d),
            time    : mappings.getTime(d),
            open    : mappings.getOpen(d),
            close   : mappings.getClose(d),
            high    : mappings.getHigh(d),
            low     : mappings.getLow(d),
        }
    })

    var x = s.data[0].time;
    var o = s.data[0].open, l = s.data[0].low, h = s.data[0].high, c = s.data[0].close;

    var throwN = function(name, value){
        if (typeof value != 'number' && value !== null) {
            throw name + " property of points should be numbers instead of " + (typeof value);
        }
    }

    if((typeof x != 'number') && !(x instanceof Date)){
        throw "x property of points should be number or date instead of " + (typeof x);
    }

    throwN('open', o) || throwN('high', h) || throwN('low', l) || throwN('close', c);

    if (s.data.length >= 3) {
        // probe to sanity check sort order
        if (s.data[2].time < s.data[1].time || s.data[1].time < s.data[0].time || s.data[s.data.length - 1].time < s.data[0].time) {
            throw "series data needs to be sorted on time values for series name: " + s.name;
        }
    }

    // set series's domain in the terms of x and y values
    var x0 = s.data[0].x,
        x1 = s.data[s.data.length-1].x,
        y0 = s.data[0].y,
        y1 = s.data[s.data.length-1].y

    s.domain = {x: [x0, x1], y: [y0, y1] };

};

})(htc);
htc.namespace('htc.Renderer.Line');

htc.Renderer.Line = htc.Class.create( htc.Renderer, {

    name: 'line',

    defaults: function($super) {

        return htc.extend( $super(), {
            unstack: true,
            fill: false,
            stroke: true,
            dashed: false
        } );
    },

    seriesPathFactory: function() {

        var graph = this.graph;

        var factory = d3.svg.line()
            .x( function(d) { return graph.x(graph.mapDataX(d)) })
            .y( function(d) { return graph.y(d.y) } )
            .interpolate(this.graph.interpolation).tension(this.tension);

        factory.defined && factory.defined( function(d) { return d.y !== null } );
        return factory;
    }
} );


htc.namespace('htc.Renderer.LinePlot');

htc.Renderer.LinePlot = htc.Class.create( htc.Renderer, {

    name: 'lineplot',

    defaults: function($super) {
        return htc.extend( $super(), {
            unstack: true,
            fill: false,
            stroke: true,
            padding:{ top: 0.01, right: 0.01, bottom: 0.01, left: 0.01 },
            dotSize: 3,
            strokeWidth: 1
        } );
    },

    initialize: function($super, args) {
        $super(args);
    },

    seriesPathFactory: function() {
        var graph = this.graph;
        var factory = d3.svg.line()
            .x( function(d) { return graph.x(graph.mapDataX(d)); } )
            .y( function(d) { return graph.y(d.y) } )
            .interpolate(this.graph.interpolation)
            .tension(this.tension);

        factory.defined && factory.defined( function(d) { return d.y !== null } );
        return factory;
    },

    _renderDots: function(vis, series) {

        var graph = this.graph, self = this;

        series.forEach(function(series) {

            if (series.disabled) return;

            var nodes = vis.selectAll("circle")
                .data(series.stack.filter( function(d) { return d.y !== null } ), series.getX)

            nodes.attr("cx", function(d) { return graph.x(graph.mapDataX(d)); })
                 .attr("cy", function(d) { return graph.y(d.y) })
                 .attr("r", function(d) { return ("r" in d) ? d.r : self.dotSize })

            var nodesEnter = nodes
                .enter().append("svg:circle")
                .attr("cx", function(d) { return graph.x(graph.mapDataX(d)); })
                .attr("cy", function(d) { return graph.y(d.y) })
                .attr("r", function(d) { return ("r" in d) ? d.r : self.dotSize })
                .attr({
                    'data-color': series.color,
                    'fill': 'white',
                    'stroke': series.color,
                    'stroke-width': this.strokeWidth
                })

            nodes.exit().remove();

        }, this);

    },

    _renderLines: function(vis, series) {

        var graph = this.graph,
            data = series.active().map(function(s){
                return s.stack.filter( function(d) { return d.y !== null } )
            }),
            factory = this.seriesPathFactory()
            ;

        var nodes = vis.selectAll("path").data(data)

        var nodeEnter = nodes
                        .enter().append("svg:path")
                        .attr("d", factory);

        nodes.attr("d", factory);
        nodes.exit().remove();

        var i = 0;
        series.forEach(function(series) {
            if (series.disabled) return;
            series.path = nodes[0][i++];
            this._styleSeries(series);
        }, this);

    },

    render: function(args) {
        var vis   = args.vis;
        this._renderLines(vis, args.series);
        this._renderDots(vis, args.series);
    }
} );

htc.namespace('htc.Renderer.Bar');

htc.Renderer.Bar = htc.Class.create( htc.Renderer, {

    name: 'bar',

    defaults: function($super) {

        var defaults = htc.extend( $super(), {
            gapSize: 1,
            unstack: false
        } );

        delete defaults.tension;
        return defaults;
    },

    initialize: function($super, args) {
        args = args || {};
        this.gapSize  = args.gapSize || this.gapSize;
        this.dispatch = d3.dispatch('tooltipShow', 'tooltipHide')
        $super(args);
    },

    domain: function($super) {
        var domain = $super();
        var frequentInterval = this._frequentInterval(this.graph.stackedData.slice(-1).shift());
        domain.x[1] += Number(frequentInterval.magnitude);

        return domain;
    },

    barWidth: function(series) {
        return (this.gapSize + .4)*(this.width)/series.stack.length;
    },

    renderBars: function(vis, series){
        var graph      = this.graph;
        var barWidth   = this.barWidth(series.active()[0]);
        var barXOffset = 0;
        var margin     = this.padding;
        var self = this;


        series.forEach( function(series) {

            if (series.disabled) return;

            var p0 = series.stack[0],
                _ = series,
                X = graph.x,
                Y = graph.y,
                color = series.color,
                barXOffset = -barWidth * 0.5,
                data = series.stack || series.data;

            this.bars = vis.selectAll("rect").data(data)

            var baseY = Y.domain()[0];

            var mergeNodes = function(nodes){

                return nodes
                    .attr("x", function(d) {
                        return X(d.x) + barXOffset;
                    })
                    .attr("y", function(d) {
                        return Y(Math.max(0, d.y));
                    })
                    .attr("width", barWidth)
                    .attr("height", function(d) {
                        return Math.abs(Y(d.y) - Y(Math.max(0, baseY)) );
                    })
                    .attr('fill',function(d){
                        return d.y > 0 ? color : 'brown';
                    });
            }

            // UPDATE BARS
            mergeNodes(this.bars)

            // APPEND NEW BARS
            mergeNodes( this.bars.enter().append("svg:rect") )
                .on("mouseover", function(){
                    d3.select(this).classed('hover', true);
                })
                 .on("mouseout", function(){
                    d3.select(this).classed('hover', false);
                })

            // REMOVE OLD BARS
            this.bars.exit().remove();

        }, this );

    },

    onMouseLeave: function(chart){
         if (chart._tooltipGroup !== null && chart._tooltipGroup !== undefined) {
                chart._tooltipGroup.remove();
            }
    },

    render: function(args) {
        args = args || {};
        var graph = this.graph;
        var series = args.series || graph.series;
        var vis = args.vis || graph.vis;
        this.renderBars(vis, series);
    },

    _frequentInterval: function(data) {

        var intervalCounts = {};

        for (var i = 0; i < data.length - 1; i++) {
            var interval = data[i + 1].x - data[i].x;
            intervalCounts[interval] = intervalCounts[interval] || 0;
            intervalCounts[interval]++;
        }


        var frequentInterval = { count: 0, magnitude: 1 };

        htc.keys(intervalCounts).forEach( function(i) {
            if (frequentInterval.count < intervalCounts[i]) {
                frequentInterval = {
                    count: intervalCounts[i],
                    magnitude: i
                };
            }
        } );

        return frequentInterval;
    }
});


// Possible shapes:
// ["circle", "cross", "diamond", "square", "triangle-down", "triangle-up"]

htc.Renderer.Marker = htc.Class.create(htc.Renderer, {
    name: 'marker',

    defaults: function($super){
        return htc.extend($super(), {
            dotSize: 3,
            strokeWidth: 1,
            shape: 'circle',
            markerBerth: 30,
            markerSize: 400
        });
    },

    render : function(args){
        var vis    = args.vis,
            graph  = this.graph,
            series = args.series,
            self = this
            ;

        series.active().forEach(function(s){
            var data = s.stack.filter(function(d) { return d.y !== null } ),
                shape = s.shape || self.shape || d3.svg.symbolTypes[0],
                symbol = d3.svg.symbol().type(shape).size(this.markerSize),
                berth  = s.markerBerth || this.markerBerth,
                className = 'dot-' + s.key
                ;


            var markers = vis.selectAll("g." + className)
                .data(data, function(d){ return d.x })

            markers.attr("transform", function(d, i) {
                return "translate(" + graph.x(d.x) + "," +  (graph.y(d.y) - berth)+ ")";
            });

            var elemEnter = markers
                            .enter().append('svg:g')
                            .attr('class', 'dot ' + className)
                            .attr("transform", function(d, i) {
                                return "translate(" + graph.x(d.x) + "," +  (graph.y(d.y) - berth)+ ")";
                            })

            var lines = elemEnter.append('svg:line')
                            .style("stroke", '#0d233a')
                            .style("stroke-width", this.strokeWidth)
                            .attr("x1", 0).attr("x2", 0).attr("y1", 0).attr("y2", function(d){
                                return berth;
                            })

            var paths = elemEnter.append("svg:path")
                        .attr("d", symbol)
                        .style("stroke", series.color || '#0d233a')
                        .on("mouseover", function(){
                            d3.select(this).classed('hover', true);
                        }).on('mouseout', function(){
                            d3.select(this).classed('hover', false);
                        })

            var texts = elemEnter.append("svg:text")
                .attr("alignment-baseline", "middle")
                .attr("text-anchor", "middle")
                .text(function(d){ return d.title })

            var markerExit = markers.exit();

            markerExit.remove();

        }, this)

    }
});
htc.namespace('htc.Renderer.Stack');

htc.Renderer.Stack = htc.Class.create( htc.Renderer, {

    name: 'stack',

    defaults: function($super) {

        return htc.extend( $super(), {
            fill: true,
            stroke: false,
            unstack: false
        } );
    },

    seriesPathFactory: function() {

        var graph = this.graph;

        var factory = d3.svg.area()
            .x( function(d) { return graph.x(graph.mapDataX(d)); } )
            .y0( function(d) { return graph.height;  } )
            .y1( function(d) { return graph.y(d.y + Number(d.y0)) } )
            .interpolate(this.graph.interpolation).tension(this.tension);

        factory.defined && factory.defined( function(d) { return d.y !== null } );
        return factory;
    }
} );

htc.namespace('htc.Renderer.Candlestick')

htc.Renderer.Candlestick = htc.Class.create(htc.Renderer, {

    name: 'candlestick',

    render: function(args){
        var series = args.series[0],
            data             = series.stack || series.data,
            graph            = this.graph || args.graph,
            vis              = args.vis || graph.vis,
            X                = graph.x,
            Y                = graph.y,
            _                = series,
            margin           = this.padding,
            whiteCandleColor = '#6ba583' || series.palette.color(),
            whiteBorderColor = '#225437',
            blackCandleColor = '#d75442' || series.palette.color(),
            blackBorderColor = '#5b1a13'
            ;

        var p = data[0];
        var barWidth = 0.5*(graph.width-margin.left-margin.right)/data.length;

        this.bars = vis.selectAll('rect').data(data, function(d){ return d.x; });

        var mergeNodes = function(nodes){
           return nodes
                .attr("x", function(d){
                    return  X(d.x) - barWidth/2;
                })
                .attr("y", function(d){
                    return Y(Math.max(d.open, d.close))
                })
                .attr("width", function(d){
                    return barWidth;
                })
                .attr("height", function(d){
                    return Y(Math.min(d.open, d.close)) - Y(Math.max(d.open, d.close));
                })
        }

        mergeNodes(this.bars)

        mergeNodes(this.bars.enter().append('svg:rect'))
            .attr("fill",function(d) {
                return d.open > d.close ? whiteCandleColor : blackCandleColor
            })
            .attr("stroke", function(d) {
                return d.open > d.close ? whiteBorderColor : blackBorderColor
            })
            .on("mouseover", function(){
                d3.select(this).classed('hover', true);
            })
            .on("mouseout", function(){
                d3.select(this).classed('hover', false);
            });

        this.bars.exit().remove();

        this.stems = vis.selectAll("line.stem").data(data, function(d){ return d.x })

        var mergeStems = function(nodes){
            return nodes
                        .attr("x1", function(d) { return X(d.x); })
                        .attr("x2", function(d) { return X(d.x); })
                        .attr("y1", function(d) { return Y(d.high) })
                        .attr("y2", function(d) { return Y(d.low) })
        }

        // Update stems
        mergeStems(this.stems)

        // ENTER stems
        mergeStems(this.stems.enter().append("svg:line").attr("class", "stem"))
          .attr("stroke", function(d){
            return d.open > d.close ? whiteCandleColor : blackCandleColor
        });

        this.stems.exit().remove();

    }
});
htc.namespace('htc.Legend');

htc.Legend = function(args) {
    var LABEL_GAP = 2;

    var _legend     = {},
        graph       = args.graph,
        _parent     = args.parent,
        _data       = args.data,
        _x          = 0,
        _y          = 0,
        width       = 960,
        height      = 20,
        _itemHeight = 12,
        _gap        = 5,
        _margin     = { top: -20, bottom: 0, right: 0, left: 0 },
        align       = false,
        rightAlign  = false;

    var _g;

    _legend.parent = function (p) {
        if (!arguments.length) return _parent;
        _parent = p;
        return _legend;
    };

    _legend.render = function () {

        width       = graph.width  || 960;
        height      = graph.height || 20;

        _g = _parent.append("g")
            .attr("class", "htc-legend")
            .attr("transform", "translate(" + _x + "," + _y + ")");

        var items = _g.selectAll('g.htc-legend-item').data(_data);

        var itemEnter = items.enter()
                .append("g")
                .attr("class", "htc-legend-item")
                .attr("transform", function (d, i) {
                    return "translate(0," + i * legendItemHeight() + ")";
                })
                .on("mouseover", function(d){
                        graph.legendHighlight(d);
                })
                .on("mouseout", function (d) {
                        graph.legendReset(d);
                });

        itemEnter.append("rect")
            .attr("width", _itemHeight)
            .attr("height", _itemHeight)
            .attr("fill", function(d){return d.color;});

        itemEnter.append("text")
            .text(function(d){return d.name;})
            .attr("x", _itemHeight + LABEL_GAP)
            .attr("y", function(){return _itemHeight / 2 + (this.clientHeight?this.clientHeight:13) / 2 - 2;});

        items.exit().remove();

        if(align){

        } else {
            var ypos = 5,  newxpos = 5, maxwidth = 0, xpos;

            items
                .attr('transform', function(d, i) {
                  var length = d3.select(this).select('text').node().getComputedTextLength() + 28;
                  xpos = newxpos;

                  if (width < _margin.left + _margin.right + xpos + length) {
                    newxpos = xpos = 5;
                    ypos += 20;
                  }

                  newxpos += length;
                  if (newxpos > maxwidth) maxwidth = newxpos;

                  return 'translate(' + xpos + ',' + ypos + ')';
                });

            //position legend as far right as possible within the total width
            _g.attr('transform', 'translate(' + (width - _margin.right - maxwidth) + ',' + _margin.top + ')');

            height = _margin.top + _margin.bottom + ypos + 15;
        }
    };

    function legendItemHeight() {
        return _gap + _itemHeight;
    }

    /**
    #### .x([value])
    Set or get x coordinate for legend widget. Default value: 0.
    **/
    _legend.x = function (x) {
        if (!arguments.length) return _x;
        _x = x;
        return _legend;
    };

    /**
    #### .y([value])
    Set or get y coordinate for legend widget. Default value: 0.
    **/
    _legend.y = function (y) {
        if (!arguments.length) return _y;
        _y = y;
        return _legend;
    };

    /**
    #### .gap([value])
    Set or get gap between legend items. Default value: 5.
    **/
    _legend.gap = function (gap) {
        if (!arguments.length) return _gap;
        _gap = gap;
        return _legend;
    };

    /**
    #### .itemHeight([value])
    Set or get legend item height. Default value: 12.
    **/
    _legend.itemHeight = function (h) {
        if (!arguments.length) return _itemHeight;
        _itemHeight = h;
        return _legend;
    };

    graph.onUpdate("legend-1", _legend.render);

    return _legend;
};

htc.namespace('htc.Brush');

htc.Brush = function(args){
    var _width  = args.width,
        _height = args.height,
        _translateY = 0,
        _translateX = 0,
        _brush = d3.svg.brush(),
        onBrushHandleTouch = args.onBrushHandleTouch || function(ev){
            d3.event.preventDefault();
        },
        onBrushEvents = args.onBrushEvents || function(ev){
            d3.event.preventDefault();
        },
        self = this,
        _resizeHandlePath =  function (d) {
            var e = +(d == "e"), x = e ? 1 : -1, y = _height / 3;
            /*jshint -W014 */
            return "M" + (0.5 * x) + "," + y
                + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6)
                + "V" + (2 * y - 6)
                + "A6,6 0 0 " + e + " " + (0.5 * x) + "," + (2 * y)
                + "Z"
                + "M" + (2.5 * x) + "," + (y + 8)
                + "V" + (2 * y - 8)
                + "M" + (4.5 * x) + "," + (y + 8)
                + "V" + (2 * y - 8);
            /*jshint +W014 */
        }
        ;

    this.initialize = function(args){
        this.graph  = args.graph;
        this.graph.onUpdate(htc.safe_number(), function(){
            this.render();
        }.bind(this));

        _translateX = args.translateX;
        _translateY = args.translateY;
    }

    this.brush = function(_){
        if(!arguments.length) return _brush;
        _brush = _;
        return _brush;
    }

    this.clearBrush = function(){
        // d3.select('.x.brush').call(_brush.clear());
    }

    this.render = function(){

        _brush.x(self.graph.x);

        this.graph.vis.select('.x.brush').remove();

        var context =  this.graph.vis.append("g")
                                    .attr('class', 'x brush')

        context.call(_brush)
                .selectAll('rect')
                .attr('y', _translateY)
                .attr('height', _height);

        var contextHeight = _height,
            contextWidth  = 5;

        context.selectAll('.resize rect')
            .attr('width', contextWidth)
            .attr('x', -3)

        context.selectAll(".resize").append("path").attr("d", _resizeHandlePath);

        this.vis = context;
    }

    return this.initialize(args);
};

htc.namespace('htc.tooltip');

htc.tooltip = function() {
    var content = null    //HTML contents of the tooltip.  If null, the content is generated via the data variable.
    ,   data = null     /* Tooltip data. If data is given in the proper format, a consistent tooltip is generated.

    Format of data:
    {
        key: "Date",
        value: "August 2009",
        series: [
                {
                    key: "Series 1",
                    value: "Value 1",
                    color: "#000"
                },
                {
                    key: "Series 2",
                    value: "Value 2",
                    color: "#00f"
                }
        ]

    }

    */
    ,   gravity = 'w'   //Can be 'n','s','e','w'. Determines how tooltip is positioned.
    ,   distance = 50   //Distance to offset tooltip from the mouse location.
    ,   snapDistance = 25   //Tolerance allowed before tooltip is moved from its current position (creates 'snapping' effect)
    ,   fixedTop = null //If not null, this fixes the top position of the tooltip.
    ,   classes = null  //Attaches additional CSS classes to the tooltip DIV that is created.
    ,   chartContainer = null   //Parent DIV, of the SVG Container that holds the chart.
    ,   tooltipElem = null  //actual DOM element representing the tooltip.
    ,   position = {left: null, top: null}      //Relative position of the tooltip inside chartContainer.
    ,   enabled = true  //True -> tooltips are rendered. False -> don't render tooltips.
    //Generates a unique id when you create a new tooltip() object
    ,   id = "nvtooltip-" + Math.floor(Math.random() * 100000)
    ;

    //CSS class to specify whether element should not have mouse events.
    var  nvPointerEventsClass = "nv-pointer-events-none";

    //Format function for the tooltip values column
    var valueFormatter = function(d,i) {
        return d;
    };

    //Format function for the tooltip header value.
    var headerFormatter = function(d) {
        return d;
    };

    //By default, the tooltip model renders a beautiful table inside a DIV.
    //You can override this function if a custom tooltip is desired.
    var contentGenerator = function(d) {
        if (content != null) return content;

        if (d == null) return '';

        var table = d3.select(document.createElement("table"));
        var theadEnter = table.selectAll("thead")
            .data([d])
            .enter().append("thead");
        theadEnter.append("tr")
            .append("td")
            .attr("colspan",3)
            .append("strong")
                .classed("x-value",true)
                .html(headerFormatter(d.value));

        var tbodyEnter = table.selectAll("tbody")
            .data([d])
            .enter().append("tbody");
        var trowEnter = tbodyEnter.selectAll("tr")
            .data(function(p) { return p.series})
            .enter()
            .append("tr")
            .classed("highlight", function(p) { return p.highlight})
            ;

        trowEnter.append("td")
            .classed("legend-color-guide",true)
            .append("div")
                .style("background-color", function(p) { return p.color});
        trowEnter.append("td")
            .classed("key",true)
            .html(function(p) {return p.key});
        trowEnter.append("td")
            .classed("value",true)
            .html(function(p,i) { return valueFormatter(p.value,i) });


        trowEnter.selectAll("td").each(function(p) {
            if (p.highlight) {
                var opacityScale = d3.scale.linear().domain([0,1]).range(["#fff",p.color]);
                var opacity = 0.6;
                d3.select(this)
                    .style("border-bottom-color", opacityScale(opacity))
                    .style("border-top-color", opacityScale(opacity))
                    ;
            }
        });

        var html = table.node().outerHTML;
        if (d.footer !== undefined)
            html += "<div class='footer'>" + d.footer + "</div>";
        return html;

    };

    var dataSeriesExists = function(d) {
        if (d && d.series && d.series.length > 0) return true;

        return false;
    };

    //In situations where the chart is in a 'viewBox', re-position the tooltip based on how far chart is zoomed.
    function convertViewBoxRatio() {
        if (chartContainer) {
          var svg = d3.select(chartContainer);
          if (svg.node().tagName !== "svg") {
             svg = svg.select("svg");
          }
          var viewBox = (svg.node()) ? svg.attr('viewBox') : null;
          if (viewBox) {
            viewBox = viewBox.split(' ');
            var ratio = parseInt(svg.style('width')) / viewBox[2];

            position.left = position.left * ratio;
            position.top  = position.top * ratio;
          }
        }
    }

    //Creates new tooltip container, or uses existing one on DOM.
    function getTooltipContainer(newContent) {
        var body;
        if (chartContainer)
            body = d3.select(chartContainer);
        else
            body = d3.select("body");

        var container = body.select(".nvtooltip");
        if (container.node() === null) {
            //Create new tooltip div if it doesn't exist on DOM.
            container = body.append("div")
                .attr("class", "nvtooltip " + (classes? classes: "xy-tooltip"))
                .attr("id",id)
                ;
        }


        container.node().innerHTML = newContent;
        container.style("top",0).style("left",0).style("opacity",0);
        container.selectAll("div, table, td, tr").classed(nvPointerEventsClass,true)
        container.classed(nvPointerEventsClass,true);
        return container.node();
    }



    //Draw the tooltip onto the DOM.
    function nvtooltip() {
        if (!enabled) return;
        if (!dataSeriesExists(data)) return;

        convertViewBoxRatio();

        var left = position.left;
        var top = (fixedTop != null) ? fixedTop : position.top;
        var container = getTooltipContainer(contentGenerator(data));
        tooltipElem = container;
        if (chartContainer) {
            var svgComp = chartContainer.getElementsByTagName("svg")[0];
            var boundRect = (svgComp) ? svgComp.getBoundingClientRect() : chartContainer.getBoundingClientRect();
            var svgOffset = {left:0,top:0};
            if (svgComp) {
                var svgBound = svgComp.getBoundingClientRect();
                var chartBound = chartContainer.getBoundingClientRect();
                var svgBoundTop = svgBound.top;

                //Defensive code. Sometimes, svgBoundTop can be a really negative
                //  number, like -134254. That's a bug.
                //  If such a number is found, use zero instead. FireFox bug only
                if (svgBoundTop < 0) {
                    var containerBound = chartContainer.getBoundingClientRect();
                    svgBoundTop = (Math.abs(svgBoundTop) > containerBound.height) ? 0 : svgBoundTop;
                }
                svgOffset.top = Math.abs(svgBoundTop - chartBound.top);
                svgOffset.left = Math.abs(svgBound.left - chartBound.left);
            }
            //If the parent container is an overflow <div> with scrollbars, subtract the scroll offsets.
            //You need to also add any offset between the <svg> element and its containing <div>
            //Finally, add any offset of the containing <div> on the whole page.
            left += chartContainer.offsetLeft + svgOffset.left - 2*chartContainer.scrollLeft;
            top += chartContainer.offsetTop + svgOffset.top - 2*chartContainer.scrollTop;
        }

        if (snapDistance && snapDistance > 0) {
            top = Math.floor(top/snapDistance) * snapDistance;
        }

        htc.tooltip.calcTooltipPosition([left,top], gravity, distance, container);
        return nvtooltip;
    };

    nvtooltip.nvPointerEventsClass = nvPointerEventsClass;

    nvtooltip.content = function(_) {
        if (!arguments.length) return content;
        content = _;
        return nvtooltip;
    };

    //Returns tooltipElem...not able to set it.
    nvtooltip.tooltipElem = function() {
        return tooltipElem;
    };

    nvtooltip.contentGenerator = function(_) {
        if (!arguments.length) return contentGenerator;
        if (typeof _ === 'function') {
            contentGenerator = _;
        }
        return nvtooltip;
    };

    nvtooltip.data = function(_) {
        if (!arguments.length) return data;
        data = _;
        return nvtooltip;
    };

    nvtooltip.gravity = function(_) {
        if (!arguments.length) return gravity;
        gravity = _;
        return nvtooltip;
    };

    nvtooltip.distance = function(_) {
        if (!arguments.length) return distance;
        distance = _;
        return nvtooltip;
    };

    nvtooltip.snapDistance = function(_) {
        if (!arguments.length) return snapDistance;
        snapDistance = _;
        return nvtooltip;
    };

    nvtooltip.classes = function(_) {
        if (!arguments.length) return classes;
        classes = _;
        return nvtooltip;
    };

    nvtooltip.chartContainer = function(_) {
        if (!arguments.length) return chartContainer;
        chartContainer = _;
        return nvtooltip;
    };

    nvtooltip.position = function(_) {
        if (!arguments.length) return position;
        position.left = (typeof _.left !== 'undefined') ? _.left : position.left;
        position.top = (typeof _.top !== 'undefined') ? _.top : position.top;
        return nvtooltip;
    };

    nvtooltip.fixedTop = function(_) {
        if (!arguments.length) return fixedTop;
        fixedTop = _;
        return nvtooltip;
    };

    nvtooltip.enabled = function(_) {
        if (!arguments.length) return enabled;
        enabled = _;
        return nvtooltip;
    };

    nvtooltip.valueFormatter = function(_) {
        if (!arguments.length) return valueFormatter;
        if (typeof _ === 'function') {
            valueFormatter = _;
        }
        return nvtooltip;
    };

    nvtooltip.headerFormatter = function(_) {
        if (!arguments.length) return headerFormatter;
        if (typeof _ === 'function') {
            headerFormatter = _;
        }
        return nvtooltip;
    };

    //id() is a read-only function. You can't use it to set the id.
    nvtooltip.id = function() {
        return id;
    };


    return nvtooltip;
};


//Original tooltip.show function. Kept for backward compatibility.
// pos = [left,top]
htc.tooltip.show = function(pos, content, gravity, dist, parentContainer, classes) {

    //Create new tooltip div if it doesn't exist on DOM.
    var   container = document.createElement('div');
    container.className = 'nvtooltip ' + (classes ? classes : 'xy-tooltip');

    var body = parentContainer;
    if ( !parentContainer || parentContainer.tagName.match(/g|svg/i)) {
        //If the parent element is an SVG element, place tooltip in the <body> element.
        body = document.getElementsByTagName('body')[0];
    }

    container.style.left = 0;
    container.style.top = 0;
    container.style.opacity = 0;
    container.innerHTML = content;
    body.appendChild(container);

    //If the parent container is an overflow <div> with scrollbars, subtract the scroll offsets.
    if (parentContainer) {
       pos[0] = pos[0] - parentContainer.scrollLeft;
       pos[1] = pos[1] - parentContainer.scrollTop;
    }
    htc.tooltip.calcTooltipPosition(pos, gravity, dist, container);
};

//Looks up the ancestry of a DOM element, and returns the first NON-svg node.
htc.tooltip.findFirstNonSVGParent = function(Elem) {
        while(Elem.tagName.match(/^g|svg$/i) !== null) {
            Elem = Elem.parentNode;
        }
        return Elem;
};

//Finds the total offsetTop of a given DOM element.
//Looks up the entire ancestry of an element, up to the first relatively positioned element.
htc.tooltip.findTotalOffsetTop = function ( Elem, initialTop ) {
            var offsetTop = initialTop;

            do {
                if( !isNaN( Elem.offsetTop ) ) {
                    offsetTop += (Elem.offsetTop);
                }
            } while( Elem = Elem.offsetParent );
            return offsetTop;
};

//Finds the total offsetLeft of a given DOM element.
//Looks up the entire ancestry of an element, up to the first relatively positioned element.
htc.tooltip.findTotalOffsetLeft = function ( Elem, initialLeft) {
            var offsetLeft = initialLeft;

            do {
                if( !isNaN( Elem.offsetLeft ) ) {
                    offsetLeft += (Elem.offsetLeft);
                }
            } while( Elem = Elem.offsetParent );
            return offsetLeft;
};

//Global utility function to render a tooltip on the DOM.
//pos = [left,top] coordinates of where to place the tooltip, relative to the SVG chart container.
//gravity = how to orient the tooltip
//dist = how far away from the mouse to place tooltip
//container = tooltip DIV
htc.tooltip.calcTooltipPosition = function(pos, gravity, dist, container) {

        var height = parseInt(container.offsetHeight),
            width = parseInt(container.offsetWidth),
            windowWidth = htc.windowSize().width,
            windowHeight = htc.windowSize().height,
            scrollTop = window.pageYOffset,
            scrollLeft = window.pageXOffset,
            left, top;

        windowHeight = window.innerWidth >= document.body.scrollWidth ? windowHeight : windowHeight - 16;
        windowWidth = window.innerHeight >= document.body.scrollHeight ? windowWidth : windowWidth - 16;

        gravity = gravity || 's';
        dist = dist || 20;

        var tooltipTop = function ( Elem ) {
            return htc.tooltip.findTotalOffsetTop(Elem, top);
        };

        var tooltipLeft = function ( Elem ) {
            return htc.tooltip.findTotalOffsetLeft(Elem,left);
        };

        switch (gravity) {
          case 'e':
            left = pos[0] - width - dist;
            top = pos[1] - (height / 2);
            var tLeft = tooltipLeft(container);
            var tTop = tooltipTop(container);
            if (tLeft < scrollLeft) left = pos[0] + dist > scrollLeft ? pos[0] + dist : scrollLeft - tLeft + left;
            if (tTop < scrollTop) top = scrollTop - tTop + top;
            if (tTop + height > scrollTop + windowHeight) top = scrollTop + windowHeight - tTop + top - height;
            break;
          case 'w':
            left = pos[0] + dist;
            top = pos[1] - (height / 2);
            var tLeft = tooltipLeft(container);
            var tTop = tooltipTop(container);
            if (tLeft + width > windowWidth) left = pos[0] - width - dist;
            if (tTop < scrollTop) top = scrollTop + 5;
            if (tTop + height > scrollTop + windowHeight) top = scrollTop + windowHeight - tTop + top - height;
            break;
          case 'n':
            left = pos[0] - (width / 2) - 5;
            top = pos[1] + dist;
            var tLeft = tooltipLeft(container);
            var tTop = tooltipTop(container);
            if (tLeft < scrollLeft) left = scrollLeft + 5;
            if (tLeft + width > windowWidth) left = left - width/2 + 5;
            if (tTop + height > scrollTop + windowHeight) top = scrollTop + windowHeight - tTop + top - height;
            break;
          case 's':
            left = pos[0] - (width / 2);
            top = pos[1] - height - dist;
            var tLeft = tooltipLeft(container);
            var tTop = tooltipTop(container);
            if (tLeft < scrollLeft) left = scrollLeft + 5;
            if (tLeft + width > windowWidth) left = left - width/2 + 5;
            if (scrollTop > tTop) top = scrollTop;
            break;
          case 'none':
            left = pos[0];
            top = pos[1] - dist;
            var tLeft = tooltipLeft(container);
            var tTop = tooltipTop(container);
            break;
        }


        container.style.left = left+'px';
        container.style.top = top+'px';
        container.style.opacity = 1;
        container.style.position = 'absolute';

        return container;
};

//Global utility function to remove tooltips from the DOM.
htc.tooltip.cleanup = function() {

          // Find the tooltips, mark them for removal by this class (so others cleanups won't find it)
          var tooltips = document.getElementsByClassName('nvtooltip');
          var purging = [];
          while(tooltips.length) {
            purging.push(tooltips[0]);
            tooltips[0].style.transitionDelay = '0 !important';
            tooltips[0].style.opacity = 0;
            tooltips[0].className = 'nvtooltip-pending-removal';
          }

          setTimeout(function() {

              while (purging.length) {
                 var removeMe = purging.pop();
                  removeMe.parentNode.removeChild(removeMe);
              }
        }, 500);
};

htc.namespace('htc.Zoom');

htc.Zoom = function(args){
    var graph   = args.graph,
        _parent = args.vis,
        _buttons = [],
        _g,
        _itemHeight = 30,
        fixedTimes = {
            millisecond: 1,
            second: 1000,
            minute: 60 * 1000,
            hour: 3600 * 1000,
            day: 24 * 3600 * 1000,
            week: 7 * 24 * 3600 * 1000,
            month: 30 * 24 * 3600 * 1000,
            year: 365 * 24 * 3600 * 1000
        },
        _margin = { top: 0, left: 0, bottom: 0, right: 0},
        _extent = [-10, 10],
        dispatch = d3.dispatch('zoom')
        ;


    this.render = function(){
        _g = _parent.append('g')
            .attr('class', 'htc-zoom')
            .attr("transform", "translate(" + _margin.left + "," + _margin.top + ")");

        var zoomButtons = _g.selectAll('.htc-zoom-item').data(_buttons);

        var zoomEnter = zoomButtons.enter()
                        .append('svg:g')
                        .attr("class", "htc-zoom-item")
                        .attr("transform", function(d, i){
                            return "translate(" + i * _itemHeight + "," + "0)"
                        })
                        .on("mouseover", function(){
                            d3.select(this).classed('hover', true);
                        })
                        .on("mouseout", function(){
                            d3.select(this).classed('hover', false);
                        })
                        .on("click", function(d){
                            var interval = d.count * fixedTimes[d.type];
                            dispatch.zoom(interval);
                        })

        zoomEnter.append('rect')
                .attr({ "rx":  0, "ry": 0 })
                .attr("width", _itemHeight)
                .attr("height", _itemHeight)

        zoomEnter.append("text")
            .attr('text-anchor', 'middle')
            .attr("x", _itemHeight/2)
            .attr("y", function(){return _itemHeight / 2 + (this.clientHeight?this.clientHeight:13) / 2 - 2;})
            .text(function(d){
                return d.text;
            });

        zoomButtons.exit().remove();

    }

    this.buttons = function(_){
        if(!arguments.length) return _buttons;
        _buttons = _;
        return this;
    }

    this.extent = function(_){
        if(!arguments.length) return _extent;
        _extent = _;
        return this;
    };

    this.margin = function(_){
        if(!arguments.length) return _margin;
        _margin = _;
        return this;
    };

    this.dispatch = dispatch;


}
htc.namespace('htc.InteractiveLayer');

htc.InteractiveLayer = function(args){

    var width = args.width || null
    , height = args.height || null
    , margin = { left: 0, top: 0 }
    , xScale = d3.scale.linear()
    , yScale = d3.scale.linear()
    , dispatch = d3.dispatch('elementMousemove', 'elementMouseout','elementDblclick')
    , showGuideLine = true
    , svgContainer = args.element
    , layer = this
    , data = []
    , tooltip = htc.tooltip()
    ;

    var isMSIE = navigator.userAgent.indexOf("MSIE") !== -1  //Check user-agent for Microsoft Internet Explorer.;

    function mouseHandler() {
        var d3mouse = d3.mouse(this);
        var mouseX = d3mouse[0];
        var mouseY = d3mouse[1];
        var subtractMargin = true;
        var mouseOutAnyReason = false;
        if (isMSIE) {
         /*
            D3.js (or maybe SVG.getScreenCTM) has a nasty bug in Internet Explorer 10.
            d3.mouse() returns incorrect X,Y mouse coordinates when mouse moving
            over a rect in IE 10.
            However, d3.event.offsetX/Y also returns the mouse coordinates
            relative to the triggering <rect>. So we use offsetX/Y on IE.
         */
         mouseX = d3.event.offsetX;
         mouseY = d3.event.offsetY;

         /*
            On IE, if you attach a mouse event listener to the <svg> container,
            it will actually trigger it for all the child elements (like <path>, <circle>, etc).
            When this happens on IE, the offsetX/Y is set to where ever the child element
            is located.
            As a result, we do NOT need to subtract margins to figure out the mouse X/Y
            position under this scenario. Removing the line below *will* cause
            the interactive layer to not work right on IE.
         */
         if(d3.event.target.tagName !== "svg")
            subtractMargin = false;

         if (d3.event.target.className.baseVal.match("nv-legend"))
            mouseOutAnyReason = true;

        }

        if(subtractMargin) {
         mouseX -= margin.left;
         mouseY -= margin.top;
        }

        /* If mouseX/Y is outside of the chart's bounds,
        trigger a mouseOut event.
        */
        if (mouseX < 0 || mouseY < 0
        || mouseX > availableWidth || mouseY > availableHeight
        || (d3.event.relatedTarget && d3.event.relatedTarget.ownerSVGElement === undefined)
        || mouseOutAnyReason
        )
        {
            if (isMSIE) {
                if (d3.event.relatedTarget
                    && d3.event.relatedTarget.ownerSVGElement === undefined
                    && d3.event.relatedTarget.className.match(tooltip.nvPointerEventsClass)) {
                    return;
                }
            }
            dispatch.elementMouseout({
               mouseX: mouseX,
               mouseY: mouseY
            });
            layer.renderGuideLine(null); //hide the guideline
            return;
        }

        var pointXValue = xScale.invert(mouseX);
        dispatch.elementMousemove({
            mouseX: mouseX,
            mouseY: mouseY,
            pointXValue: pointXValue
        });

        //If user double clicks the layer, fire a elementDblclick dispatch.
        if (d3.event.type === "dblclick") {
            dispatch.elementDblclick({
                mouseX: mouseX,
                mouseY: mouseY,
                pointXValue: pointXValue
            });
        }
    }


    if(svgContainer){

        var availableWidth = (width || 960), availableHeight = (height || 400);

        var wrap = svgContainer.selectAll("g.nv-wrap.nv-interactiveLineLayer").data([data]);
        var wrapEnter = wrap.enter()
                    .append("g").attr("class", " nv-wrap nv-interactiveLineLayer");

        wrapEnter.append("g").attr("class","nv-interactiveGuideLine");

        svgContainer
          .on("mousemove",mouseHandler, true)
          .on("mouseout" ,mouseHandler,true)
          .on("dblclick" ,mouseHandler);

        //Draws a vertical guideline at the given X postion.
        layer.renderGuideLine = function(x) {
            if (!showGuideLine) return;
            var line = wrap.select(".nv-interactiveGuideLine")
                  .selectAll("line")
                  .data((x != null) ? [htc.NaNtoZero(x)] : [], String);

            line.enter()
                .append("line")
                .attr("class", "nv-guideline")
                .attr("x1", function(d) { return d;})
                .attr("x2", function(d) { return d;})
                .attr("y1", availableHeight)
                .attr("y2",0)
                ;
            line.exit().remove();

        }
    }

    layer.dispatch = dispatch;
    layer.tooltip  = tooltip;

    return layer;

}


htc.interactiveBisect = function (values, searchVal, xAccessor, tolerance) {
      "use strict";
      if (! values instanceof Array) return null;
      if (! values.length) return null;

      if (typeof xAccessor !== 'function') xAccessor = function(d,i) { return d.x;}
      var bisect = d3.bisector(xAccessor).left;
      var index = d3.max([0, bisect(values,searchVal) - 1]);
      var currentValue = xAccessor(values[index], index);

      if (typeof currentValue === 'undefined') currentValue = index;

      if (currentValue === searchVal) return index;  //found exact match

      var nextIndex = d3.min([index+1, values.length - 1]);
      var nextValue = xAccessor(values[nextIndex], nextIndex);
      if (typeof nextValue === 'undefined') nextValue = nextIndex;

      if (Math.abs(nextValue - searchVal) >= Math.abs(currentValue - searchVal))
          index = index;
      else
          index = nextIndex;

      if(!tolerance)
          return index;

      if( index < values.length && Math.abs(searchVal- xAccessor(values[index])) <= tolerance)
          return index;
};

/*
Returns the index in the array "values" that is closest to searchVal.
Only returns an index if searchVal is within some "threshold".
Otherwise, returns null.
*/

htc.nearestValueIndex = function (values, searchVal, threshold) {
      "use strict";
      var yDistMax = Infinity, indexToHighlight = null;
      values.forEach(function(d,i) {
         var delta = Math.abs(searchVal - d);
         if ( delta <= yDistMax && delta < threshold) {
            yDistMax = delta;
            indexToHighlight = i;
         }
      });
      return indexToHighlight;
};

htc.namespace('htc.Graph');

htc.Graph = function(args){
    if(!args.element) throw "htc.Graph needs a reference to an element";
    this.renderlets = {};
    this.element = args.element;

    var chartCount         = 0,
        spacing            = { top: 30, right: 0, bottom: 10, left: 0 },
        margin             = { top: 10, right: 10, bottom: 10, left: 10 },
        xAxisHeight        = 20,
        yAxisWidth         = 50,
        _zoomScale         = [-10, 100],
        _showRangeSelector = true,
        _mouseZoomable     = false,
        _buttonZoomable    = true,
        brushHeight        = 80,
        now                = new Date(),
        brushTimerAvg      = 50,
        onBrushTimer       = now,
        tooltips           = true,
        self               = this,
        configOptions      = {},
        _zoomButtons       = [{
            type: 'month',
            count: 1,
            text: '1m'
        }, {
            type: 'month',
            count: 3,
            text: '3m'
        }, {
            type: 'month',
            count: 6,
            text: '6m'
        }, {
            type: 'ytd',
            text: 'YTD'
        }, {
            type: 'year',
            count: 1,
            text: '1y'
        }, {
            type: 'all',
            text: 'All'
        }]

        ;

    this.initialize = function(args){
        if(!args.xAxis) throw "htc.Graph need atleast one xAxis."
        if(!args.yAxis) throw "htc.Graph need atleast one yAxis."
        if(!args.series) throw "htc.Graph need at least one series."

        this.vis = d3.select(this.element)
                        .attr('class', 'htc_graph').append("svg");

        // copy the initial configuration with cloning the data
        configOptions = initConfig(args);
        margin  = configOptions.chart.margin;
        spacing = configOptions.chart.spacing;

        setupYAxisHeight(args);
        setupXAxis(args);

        args.series.forEach(function(options, idx){
            options.data || (options.data = options.data || []);

            var xidx   = options['xAxis'] || 0,
                yidx   = options['yAxis'] || 0,
                topX   = options['top'] || 0,
                height = options['height'],
                name   =  htc.nameToId(options.name || "renderlet" + idx),

                svgEl      = self.vis.append("svg:g").attr("id", name)[0][0],
                xOptions   = htc.clone(args.xAxis[xidx]),
                yOptions   = args.yAxis[yidx],
                plotMargin = htc.clone(margin)
                ;

                xOptions.height = xAxisHeight;
                yOptions.width  = yAxisWidth;
                xOptions.name   = xOptions.name || 'x-axis-' + yidx;

                plotMargin.left     += yOptions.width;
                plotMargin.bottom   += xOptions.height;
                if(options.yAxis == 0)
                    plotMargin.top = margin.top

                var series = new htc.Series(options.data, options.scheme, htc.omit(options, 'data', 'scheme'));

                var renderlet = new htc.Graph.XY({
                    name     : name,
                    element  : svgEl,
                    xAxis    : xOptions,
                    yAxis    : yOptions,
                    renderer : 'multi',
                    margin   : plotMargin,
                    'series' : series,
                });

                renderlet.yIndex = yidx;
                renderlet.xIndex = xidx;
                renderlet.xAxis.enableLabel(false);
                self.renderlets[name] = renderlet;
        });


        chartCount += Object.keys(this.renderlets).length;
        this.setSize(args.chart);
        var idx = 0;

        for(var name in self.renderlets){
            var group = self.renderlets[name];

            var topX     = yAxisValue(idx, 'top'),
                width    = this.width,
                height   = yAxisValue(idx, 'height')
                ;

            group.setSize({ 'width' : width , 'height' : height })
                 .moveTo(0, Math.abs(topX))

            idx++;
        }

        if(configOptions.navigator.enabled)
            this.initNavigator(args);
        else {
            var lastYidx = args.yAxis.length-1;
            for(var key in this.renderlets)
                if(this.renderlets[key].yIndex == lastYidx){
                    this.renderlets[key].xAxis.enableLabel(true)
                }
        }


        if(configOptions.tooltip.enabled){
            this.showTooltips(configOptions.tooltip.enabled);
            this.interactiveLayer = new htc.InteractiveLayer({
                element: this.vis,
                height: this.height - brushHeight ,
            })

            this.interactiveLayer.dispatch
                .on("elementMousemove", this.showToolTip.bind(this))
                .on("elementMouseout",  function(){
                    if(tooltips) htc.tooltip.cleanup();
                });
        }

        if(configOptions.navigation.buttonOptions.enabled){
            this.zoomer = new htc.Zoom({
                graph: this,
                vis: this.vis
            });

            this.zoomer.dispatch.on('zoom', function(interval){
                setFocus(interval);
                self.onBrush();
                self.renderlets['range'].context.render();
            });
       } else {
            this.enableMouseZoom();
        }

        return this;
    }

    var commonXAxis = function(){
        return htc.values(self.renderlets)[0].xAxis;
    }

    var commonXScale = function(){
        return htc.values(self.renderlets)[0].x;
    }

    var initConfig = function(args){
        var ini = configOptions;
        var DEFAUL_NAVIGATOR = {"enabled": true },
            DEFAUL_NAVIGATION = {
                buttonOptions: {
                    enabled: true
                }
            },
            DEFAULT_RANGE_SELECTOR = { "enabled": true },
            DEFAULT_TOOLTIP = { "enabled": true },
            DEFAULT_YAXIS = {
                "ticks": 5
            },
            DEFAULT_XAXIS = {},
            DEFAULT_CHART = {
                "margin"  : margin,
                "spacing" : spacing,
                element   : null,
                animation : true }
            ;

        ini.navigator     = htc.deepMerge(DEFAUL_NAVIGATOR, args.navigator);
        ini.navigation    = htc.deepMerge(DEFAUL_NAVIGATION, args.navigation);
        ini.rangeSelector = htc.deepMerge(DEFAULT_RANGE_SELECTOR, args.rangeSelector);

        ini.tooltip       = htc.deepMerge(DEFAULT_TOOLTIP, args.tooltip);
        ini.chart         = htc.deepMerge(DEFAULT_CHART, args.chart || {});
        return ini;
    }


    this.enableMouseZoom = function() {
        if (_mouseZoomable) {
            self.element.call(d3.behavior.zoom()
                .x(commonXScale())
                .scaleExtent(_zoomScale)
                .on("zoom", function () {
                    setFocus();
                }));
        }
    }


    this.showToolTip = function(e){

        var rows = [], pointIndex, singlePoint, pointXLocation,

                pushRow = function(key, value, color){
                    rows.push({
                        'key': key.toUpperCase(),
                        'value': value.toFixed(2),
                        'color': color
                    })
                };

            htc.values(this.renderlets).forEach(function(plot){

                if(plot.showBrush()) return;

                var tolerance = 0.03 * Math.abs(plot.x.range()[0] - plot.x.range()[1]);

                plot.series.active().forEach(function(item){
                    var getX = function(d, i){
                        return plot.x(d.x);
                    }

                    pointIndex = htc.interactiveBisect(item.stack, e.pointXValue, getX, tolerance);

                    if(!pointIndex) return;

                    if(item._hasCategoryFields){
                        item._categoryFields.forEach(function(accessor){
                            var point = item.stack[pointIndex];

                            if (typeof pointXLocation === 'undefined') pointXLocation = getX(point, pointIndex);
                            if (typeof singlePoint === 'undefined') singlePoint = point.x;

                            pushRow(accessor, point[accessor], item.color);
                        })
                    } else {
                        var point = item.stack[pointIndex];

                        if (typeof pointXLocation === 'undefined') pointXLocation = getX(point, pointIndex);
                        if (typeof singlePoint === 'undefined') singlePoint = point.x;

                        pushRow(item.key, point.y, item.color);
                    }
                })
            });

          //Highlight the tooltip entry based on which point the mouse is closest to.
          if (rows.length > 2) {
            htc.values(this.renderlets).forEach(function(plot){
                var yValue = plot.y.invert(e.mouseY);
                var domainExtent = Math.abs(plot.y.domain()[0] - plot.y.domain()[1]);
                var threshold = 0.03 * domainExtent;
                var indexToHighlight = htc.nearestValueIndex(rows.map(function(d){ return d.value }), yValue, threshold);
                if (indexToHighlight !== null)
                  rows[indexToHighlight].highlight = true;
            })
          }

          if(rows.length == 0) return;

          var xValue = commonXAxis().tickFormat(singlePoint);
          var margin = { left: 20, top: 0 };

          this.interactiveLayer.tooltip
                  .position({left: pointXLocation + margin.left, top: e.mouseY + margin.top})
                  .chartContainer(this.vis.parentNode)
                  .enabled(tooltips)
                  .valueFormatter(function(d,i) {
                     return d;
                  })
                  .data(
                      {
                        value: xValue,
                        series: rows
                      }
                  )();

          this.interactiveLayer.renderGuideLine(pointXLocation);

    }

    this.showTooltips = function(_){
        if(!arguments.length) return tooltips;
        tooltips = _;
        return this;
    }

    var setFocus = function(interval){
        var navigator = configOptions.navigator;

        if(navigator.enabled){
            var brushG = self.renderlets['range'],
                domain   = brushG.x.domain(),
                x            = brushG.x,
                brush        = brushG.context.brush(),
                startD, endD;

            if(interval){
                if(htc.discreteTime){
                    startD = new Date(domain[1]-interval),
                    endD   = domain[1];
                    self.renderlets['range'].context.clearBrush();
                } else {
                    startD = htc.weekday(new Date(htc.weekday.invert(domain[1])-interval)),
                    endD   = domain[1];
                    self.renderlets['range'].context.clearBrush();
                }
            } else {
                if(htc.discreteTime){
                    var rangeSeconds = domain[1] - domain[0],
                        startOffset  = domain[0].getTime() + rangeSeconds * 0.3,
                        endOffset    = domain[1].getTime() - rangeSeconds * 0.4
                        ;

                    startD       = new Date(startOffset);
                    endD         = new Date(endOffset);
                }
                else {
                    var rangeSeconds = domain[1] - domain[0],
                        startOffset  = domain[0] + Math.floor(rangeSeconds * 0.3),
                        endOffset    = domain[1] - Math.floor(rangeSeconds * 0.4)
                        ;

                    startD   = startOffset;
                    endD     = endOffset;
                }
            }

            brush = brush.x(x);
            brush = brush.extent([startD, endD]);
        }
    }

    this.render = function(){
        setFocus();


        for(var name in this.renderlets){
            this.renderlets[name].render();
        }

        if(this.zoomer){
            this.zoomer.buttons(_zoomButtons)
                .extent(commonXScale().domain())
                .margin({ left: yAxisWidth, top: 0 , bottom: 0, right: 0});

            this.zoomer.render();
        }

        if(configOptions.navigator.enabled) this.onBrush();


    }

    this.setSize = function(args) {
        args = args || {};

        if (typeof window !== undefined) {
            var style = window.getComputedStyle(this.element, null);
            if(style){
                var elementWidth = parseInt(style.getPropertyValue('width'), 10);
                var elementHeight = parseInt(style.getPropertyValue('height'), 10);
            }
        }

        this.width  = (args.width  || elementWidth  || 400) - spacing.left - spacing.right;
        this.height = (args.height || elementHeight || 250) + brushHeight - spacing.top - spacing.bottom;

        this.vis && this.vis
            .attr('width', this.width)
            .attr('height', this.height);

    };

    this.initNavigator = function(args){
        var selectOpts = args.navigator;

        var svgEl     = self.vis.append("svg:g").attr("id", "navigator"),
            series    = htc.values(this.renderlets)[0].series[0],
            margin    = htc.clone(spacing),
            args      = args || {},
            width     = args.width  || this.width,
            lastYAxis = args.yAxis[args.yAxis.length-1],
            topY      = lastYAxis.top + lastYAxis.height;

        margin.left   += yAxisWidth;

        var values = series.data.map(function(d){
            return { x: d.time, y: d.y, y0: d.y0 }
        });

        var data  = [{
            key: 'range',
            values: values,
            renderer: 'stack',
            color: '#609ac1'
        }]

        var brushG = new htc.Graph.XY({
            name: 'range',
            width : width,
            height: brushHeight,
            element: svgEl[0][0],
            renderer: 'multi',
            margin : margin,
            xAxis: {
                name: 'x-axis-' + args.yAxis.length,
                width : width,
                height: xAxisHeight,
                labels: {
                    format: 'day',
                    enabled: true
                },
                grid: {
                    enabled: false
                }
            },
            yAxis: {
                ticks: 2,
                top: topY,
                height: brushHeight,
                width: yAxisWidth
            },
            showBrush: true,
            series: new htc.Series(data, null, {})
        });


        brushG.context.brush().on("brush", this.onBrush.bind(this));
        brushG.yAxis.showGrid(false).showRef(false).showLabel(false);
        self.renderlets[brushG.name] = brushG;
        brushG.setSize({ width: width, height: brushHeight })
            .moveTo(0, topY);

    }

    this.onBrush = function(){
        var charts = htc.values(this.renderlets),
            chartContext = this.renderlets["range"],
            series = chartContext.series[0],
            brush  = chartContext.context.brush();

        var startTime = Date.now();
        if (startTime - onBrushTimer > brushTimerAvg) {
            // Compute the context xDomain
            var filtered_data;
            // var xDomain;
            // if (brush.empty()) {
            //     filtered_data = series.data;
            // }
            // else {
            //     var brush_extent = brush.extent();
            //     filtered_data = series.data.filter(function (element, index, array) {
            //         return (brush_extent[0] <= series.getX(element) &&
            //                 series.getX(element) <= brush_extent[1]);
            //     });
            // }
            // xDomain = d3.extent(filtered_data.map(series.getX));

            // // Find the x index range of the chart data
            var xRange = [0, 0], xExtent = [0, 0];
            // var hasStart = false;
            // var hasEnd = false;

            // for (var i = 0; i < series.data.length; i++) {
            //     var x = series.getX(series.data[i]);
            //     if (!hasStart && xDomain[0] == x) {
            //         xRange[0]  = i;
            //         xExtent[0] = x;
            //         hasStart = true;
            //     }
            //     if (!hasEnd && xDomain[1] == x) {
            //         xRange[1] = i;
            //         xExtent[1] = x;
            //         hasEnd = true;
            //     }
            //     if (hasStart && hasEnd) {
            //         break;
            //     }
            // }

            xExtent = brush.empty() ? chartContext.x.domain() : brush.extent();

            for (var i = 0; i < charts.length; i++) {
                if(!(charts[i].name == chartContext.name)){
                    charts[i].onBrush.call(charts[i], brush, xExtent, xRange)
                }
            }

            var now = Date.now();
            brushTimerAvg = ((0.40 * brushTimerAvg) + (0.60 * (now - startTime))) / 1.95;
            onBrushTimer = now;
        }
    };

    var yAxisValue = function(idx, name, bydefault){
        if(!(typeof args.yAxis[idx] == 'undefined')){
            return args.yAxis[idx][name] || bydefault;
        } else {
            return bydefault;
        }
    }

    var setupXAxis = function(args){
        if(!(args.xAxis instanceof Array))
            args.xAxis = [args.xAxis];
    }

    var setupYAxisHeight = function(args){
        var totalHeight, unknownHeightIdx, unknownHeightCount = 0;

        if(!args.chart.height)
            args.chart.height = d3.sum(args.yAxis.map(function(y){
                if(!y.height)
                    throw "chart.height and y axis height is missing: " + JSON.stringify(y);
                return y.height + spacing.top + spacing.bottom
            }));

        totalHeight = args.chart.height - margin.top - margin.bottom - brushHeight;

        args.yAxis.forEach(function(axis, i){

            if(i==0){
                axis.top = spacing.top + margin.top;
                if(axis.height)
                    axis.height = axis.height - margin.top;
            }

            if(typeof axis.height == 'undefined'){
                unknownHeightIdx = i;
                unknownHeightCount += 1;
            }
        })

        if(unknownHeightCount > 1)
            throw JSON.stringify(args.yAxis[unknownHeightIdx]) + " should have height/top parameter."

        if(!totalHeight && unknownHeightIdx > 0)
            throw JSON.stringify(args.yAxis[unknownHeightIdx]) + " should have height/top parameter or graph total height should be given."

        if(totalHeight && unknownHeightCount == 1){
            var newHeight,
                prevTop = yAxisValue(unknownHeightIdx-1, 'top') ||
                         d3.sum(args.yAxis.slice(0, Math.max(unknownHeightIdx-1, 0)).map(function(x){ return x.height; })),

                nextTop = yAxisValue(unknownHeightIdx+1, 'top') ||
                          d3.sum(args.yAxis.slice(Math.min(unknownHeightIdx+1, args.yAxis.length), args.yAxis.length).map(function(x){ return x.height; }))
                    ;

            if(prevTop && nextTop)
                newHeight = nextTop - prevTop;
            else if(nextTop)
                newHeight = totalHeight - nextTop;
            else if(prevTop)
                newHeight = totalHeight - prevTop- brushHeight;

            args.yAxis[unknownHeightIdx].height = newHeight;
        }

        args.yAxis.forEach(function(axis, i){
            if(typeof axis.top == 'undefined' && i > 0){
                var prevY = args.yAxis[i-1];
                axis.top = prevY.top + prevY.height;
            }
        });

        self.totalHeight = totalHeight;

        return args;
    }

    return this.initialize(args);
}

htc.namespace('htc.Graph.XY');

htc.Graph.XY = function(args) {
    if (!args.element) throw "htc.Graph needs a reference to an element";

    this.element = args.element;
    this.series  = args.series;
    this.name = args.name;

    this.defaults = {
        interpolation: 'cardinal',
        offset: 'zero',
        min: 'auto',
        max: undefined,
        preserve: false,
        margin: { left: 0, right: 0, top: 0, bottom: 0},
    };

    htc.keys(this.defaults).forEach( function(k) {
        this[k] = args[k] || this.defaults[k];
    }, this );

    this.window = {};

    this.updateCallbacks = {};

    this.mapDataX = args.mapDataX || function(d){
        if(d instanceof Date)
            return htc.discreteTime ? d : htc.weekday(d);
        else if(d instanceof Object && (d.x instanceof Date))
            return htc.discreteTime ? d.x : htc.weekday(d.x);
        else if(d instanceof Object && (d.time instanceof Date))
            return htc.discreteTime ? d.time : htc.weekday(d.time);
        else
            return d;
    };

    var self = this;

    var showXAxis      = true,
        showYAxis      = true,
        _showBrush     = args.showBrush || false,
        _showLegend    = true,
        _showTooltip   = true,
        _showZoom      = true,
        _legend_margin = 60;

    self.translateX    = 0;
    self.translateY    = 0;

    var offUpdate = function(id){
        delete self.updateCallbacks[id];
    }


    this.initialize = function(args) {

        this.validateSeries(args.series);
        this.series.active = function() { return self.series.filter( function(s) { return !s.disabled } ) };

        this.setSize(args);

        var wr = this.margin.left + this.margin.right,
            hr = this.margin.top  + this.margin.bottom;

        this.vis = d3.select(this.element)
            .attr('width', this.width)
            .attr('height', this.height);

        for (var name in htc.Renderer) {
            if (!name || !htc.Renderer.hasOwnProperty(name)) continue;
            var r = htc.Renderer[name];
            if (!r || !r.prototype || !r.prototype.render) continue;
            self.registerRenderer(new r( { graph: self } ));
        }

       this.xAxis = new htc.Axis.X(this, args.xAxis);
       this.yAxis = new htc.Axis.Y(this, args.yAxis);

       if(_showLegend)
           this.legendables = new htc.Legend({
                graph: this,
                parent: this.vis,
                data: args.series.filter(function(d){
                        // filter the showable legends which are markers or tooltips
                        return ['marker', 'tooltip'].indexOf(d.renderer) < 0;
                    }).map(function(d){
                        return {
                            name: (d.name || d.key).toUpperCase(),
                            color: d.color
                        }
                    })
            });

       if(args.showBrush){
            this.context = new htc.Brush({
                graph: this,
                width: this.width - this.margin.left,
                height: this.height,
                translateX: this.margin.left,
                translateY: 0
            });
        }

       this.setRenderer(args.renderer || 'stack', args);
       this.discoverRange();

    };

    this.showYAxis = function(_, options){
        if(!arguments.length) return showYAxis;
        showYAxis = _;
        if(showYAxis) {
           this.yAxis = new htc.Axis.Y(htc.extend({ graph: self }, options));
        } else {
            offUpdate(this.yAxis.id);
        }
        return this;
    }

    this.showXAxis = function(_, options){
        if(!arguments.length) return showXAxis;
        showXAxis = _;
        if(showXAxis) {
           this.xAxis = new htc.Axis.X(htc.extend({ graph: self }, options));
        } else {
            offUpdate(this.xAxis.id);
        }
        return this;
    }

    this.showBrush = function(_){
        if(!arguments.length) return _showBrush;
        _showBrush = _;
        return this;
    }

    this.xDomain = function(extent){
        var newX = this.x.copy().domain(extent);
        this.x = newX;
        this.xAxis.vis.select('.x_ticks_d3').remove();
        this.xAxis.vis.select('.x_grid_d3').remove();
        this.xAxis.render.call(this.xAxis);
    }

    this.yDomain = function(yextent){
        var extent = this.renderer.dataExtent({ x: [], y: yextent })
        var newY = this.y.copy().domain(extent.y);
        this.y = newY;
        this.yAxis.vis.select('.y_ticks').remove();
        this.yAxis.vis.select('.y_grid').remove();
        this.yAxis.render.call(this.yAxis);
    }

    this.onBrush = function(brush, extent, range){
        // UPDATE THE XDOMAIN
        this.xDomain(extent);

        var yMin = Infinity, yMax = -Infinity;

        var startD = extent[0], endD = extent[1];

        this.renderer.groups.forEach(function(group){
            var series = group.series;
            series.active = function() { return series };

            series.forEach(function(item){

                var data = item.data.filter(function(d){
                    return (startD <= d.x && d.x <= endD)
                });

                var y1 = d3.min(data, function(d){ return d.y + d.y0 }),
                    y2 = d3.max(data, function(d){ return d.y + d.y0 });

                if(y1) yMin = Math.min(yMin, y1);
                if(y2) yMax = Math.max(yMax, y2);
                item.stack = data;
            })
        }, this);

        // UPDATE Y AXIS DOMAIN IF NECESSARY
        if(!(yMax < 0))
            this.yDomain([yMin, yMax]);

        this.renderer.groups.forEach(function(group){
            var series = group.series;
            group.renderer.render({
                series: series,
                vis: group.vis
            });
        });

    }

    this.moveTo = function(x,y){
        this.translateX = x, this.translateY = y;
        this.vis && this.vis
            .attr("transform", "translate(" + x + "," + y + ")")
        return this;
    }

    this.validateSeries = function(series) {
        if (!Array.isArray(series) && !(series instanceof htc.Series)) {
            var seriesSignature = Object.prototype.toString.apply(series);
            throw "series is not an array: " + seriesSignature;
        }
    };

    this.dropLineOrigin = function(){
        return { x: this.x(0), y: this.y(0) }
    }

    this.discoverRange = function() {

        var domain = this.renderer.domain();
        var margin = this.margin;
        var xDomain = [domain.x[0], domain.x[1]];

        this.x = d3.scale.linear().domain(xDomain)
                    .range([margin.left, this.width]);

        this.y = d3.scale.linear().domain(domain.y)
                    .range([this.height, 0]);

        this.y.magnitude = d3.scale.linear()
            .domain([domain.y[0] - domain.y[0], domain.y[1] - domain.y[0]])
            .range([0, this.height]);
    };

    this.render = function() {

        var stackedData = this.stackData();
        this.discoverRange();

        this.renderer.render();

        if(this.zoomer)
            this.zoomer.extent(this.x.domain()).buttons(_zoomButtons);

        for(var i in this.updateCallbacks) {
            var callback = this.updateCallbacks[i];
            callback();
        }
    };

    this.legendHighlight = function(d){};
    this.legendReset     = function(d){};

    this.update = this.render;

    this.stackData = function() {

        var data = this.series.active()
            .map( function(d) { return d.data } )
            .map( function(d) { return d.filter( function(d) { return this._slice(d) }, this ) }, this);

        var preserve = this.preserve;
        if (!preserve) {
            this.series.forEach( function(series) {
                if (series.scale) {
                    // data must be preserved when a scale is used
                    preserve = true;
                }
            } );
        }

        data = preserve ? htc.clone(data) : data;

        this.series.active().forEach( function(series, index) {
            if (series.scale) {
                // apply scale to each series
                var seriesData = data[index];
                if(seriesData) {
                    seriesData.forEach( function(d) {
                        d.y = series.scale(d.y);
                    } );
                }
            }
        } );

        this.stackData.hooks.data.forEach( function(entry) {
            data = entry.f.apply(self, [data]);
        } );

        var stackedData;

        if (!this.renderer.unstack) {

            this._validateStackable();

            var layout = d3.layout.stack();
            layout.offset( self.offset );
            stackedData = layout(data);
        }

        stackedData = stackedData || data;

        this.stackData.hooks.after.forEach( function(entry) {
            stackedData = entry.f.apply(self, [data]);
        } );


        var i = 0;
        this.series.forEach( function(series) {
            if (series.disabled) return;
            series.stack = stackedData[i++];
        } );

        this.stackedData = stackedData;
        return stackedData;
    };

    this._validateStackable = function() {

        var series = this.series;
        var pointsCount;

        series.forEach( function(s) {

            pointsCount = pointsCount || s.data.length;

            if (pointsCount && s.data.length != pointsCount) {
                throw "stacked series cannot have differing numbers of points: " +
                    pointsCount + " vs " + s.data.length + "; see htc.Series.fill()";
            }

        }, this );
    };

    this.stackData.hooks = { data: [], after: [] };

    this._slice = function(d) {

        if (this.window.xMin || this.window.xMax) {

            var isInRange = true;

            if (this.window.xMin && d.x < this.window.xMin) isInRange = false;
            if (this.window.xMax && d.x > this.window.xMax) isInRange = false;

            return isInRange;
        }

        return true;
    };

    this.onUpdate = function(id, callback) {
        if(!id) throw "id should be empty for render callback."
        if(!callback) throw " callback is empty for " + id;
        this.updateCallbacks[id] = callback;
    };

    this.registerRenderer = function(renderer) {
        this._renderers = this._renderers || {};
        this._renderers[renderer.name] = renderer;
    };

    this.configure = function(args) {

        if (args.width || args.height) {
            this.setSize(args);
        }

        htc.keys(this.defaults).forEach( function(k) {
            this[k] = k in args ? args[k]
                : k in this ? this[k]
                : this.defaults[k];
        }, this );

        this.setRenderer(args.renderer || this.renderer.name, args);
    };

    this.setRenderer = function(r, args) {
        if (typeof r == 'function') {
            this.renderer = new r( { graph: self } );
            this.registerRenderer(this.renderer);
        } else {
            if (!this._renderers[r]) {
                throw "couldn't find renderer " + r;
            }
            this.renderer = this._renderers[r];
        }

        if (typeof args == 'object') {
            args.width = this.width - this.margin.left - this.margin.left;
            args.height = this.height - this.margin.bottom - this.margin.top;
            this.renderer.configure(args);
        }
    };

    this.setSize = function(args) {

        args = args || {};

        if (typeof window !== undefined) {
            var style = window.getComputedStyle(this.element, null);
            if(style){
                var elementWidth = parseInt(style.getPropertyValue('width'), 10);
                var elementHeight = parseInt(style.getPropertyValue('height'), 10);
            }
        }

        var hplus =  (showXAxis ? ( args.xAxis ? args.xAxis.height : this.xAxis.height) : 0) || 0,
            wplus =  (this.margin.left + this.margin.right) || 0,
            wplus = 0;

        this.width  = (args.width || elementWidth || 400) - wplus;
        this.height = (args.height || elementHeight || 250) - hplus;

        this.vis && this.vis
            .attr('width', this.width + wplus)
            .attr('height', this.height + hplus);

        return this;
    };

    return this.initialize(args);
};

(function(htc){

    var statAnalysis = {
        // Calculate Simple Moving Average
        sma: function (data, smaPeriod) {
            var currSum = 0;
            var sma = [];
            for (var i = 0; i < data.length; i++) {
                currSum = 0;
                var k = 0;
                var sumArr = [];
                for (var j = i; j < data.length; j++) {
                    sumArr.push(data[i]);
                    if (k < smaPeriod) {
                        currSum += data[j];
                        k++;
                    }
                }
                var smaValue = currSum / smaPeriod;
                if (sumArr.length >= smaPeriod) {
                    sma.push(smaValue);
                }
            }
            return sma;
        },

        ema: function (data, period) {
            /*SMA: 10 period sum / 10
             Multiplier: (2 / (Time periods + 1) ) = (2 / (10 + 1) ) = 0.1818 (18.18%)
             EMA: {Close - EMA(previous day)} x multiplier + EMA(previous day). */
            var sma = [];
            var multiplier = [];
            var ema = [];
            var currSum = 0;

            var periodData = data.slice(period - 1, data.length);
            for (var i = 0; i < data.length; i++) {
                currSum = 0;
                var k = 0;
                var sumArr = [];
                for (var j = i; j < data.length; j++) {
                    sumArr.push(data[i]);

                    if (k < period) {

                        currSum += data[j];
                        k++;
                    }
                }
                var smaValue = currSum / period
                if (sumArr.length >= period) {
                    sma.push(smaValue);
                }
            }

            for (var i = 1; i < sma.length; i++) {
                multiplier.push(2 / (period + 1));
            }

            for (var i = 0; i < sma.length; i++) {
                if (i == 0) {
                    ema.push(sma[i]);
                } else {
                    ema.push(multiplier[i - 1] * (periodData[i] - ema[i - 1]) + ema[i - 1]);
                }
            }
            return ema;
        },

        // Calculate stdDev for BBands
        stdsma: function(data, sma, smaPeriod) {
            var startPoint = 0,
                prevData = [],
                stdDev = [],
                smaPeriod = smaPeriod;

            for (var i = smaPeriod; i <= data.length; i++) {
                prevData = data.slice(startPoint, i);
                var currSum = 0;
                for (var k = 0; k < prevData.length; k++) {
                    currSum = currSum + Math.pow(prevData[k] - sma[i - smaPeriod], 2);
                }
                stdDev.push(Math.sqrt(currSum / smaPeriod));
                startPoint++;
            }
            return stdDev;
        },

        // Calculate Bollinger bands data
        bbands: function(data, smaPeriod, std) {
            var upperBand = [],
                lowerBand = [],
                bbData = [],
                middleBand = [],
                smaPeriod = smaPeriod + 1,
                sma = this.sma(data, smaPeriod),
                stdDev = this.stdsma(data, sma, smaPeriod),
                stdFactor = std || 2;
            //console.log(sma);
            for (var i = 0; i < sma.length; i++) {
                upperBand.push(sma[i] + (stdDev[i] * stdFactor));
                middleBand = sma;
                lowerBand.push(sma[i] - (stdDev[i] * stdFactor));

            }
            bbData.push(upperBand, middleBand, lowerBand);
            return bbData;
        },

        wma: function(data, period) {
            var wma = [],
                currSum = 0,
                periodSum = 0,
                periodData = data.slice(period - 1, data.length);

            for (var i = 0; i < data.length; i++) {
                currSum = 0;
                var k = 0;
                var sumArr = [];
                for (var j = i; j < data.length; j++) {
                    sumArr.push(data[i]);
                    if (k < period) {
                        currSum += data[j] * (k + 1);
                        k++;
                    }
                }
                var wmaValue = currSum / ((period * (period + 1)) / 2);
                if (sumArr.length >= period) {
                    wma.push(wmaValue);
                }
            }
            return wma;
        },

        macd: function(data, fastPeriod, slowPeriod, signalPeriod) {
            //MACD Line: (12-day EMA - 26-day EMA)
            //Signal Line: 9-day EMA of MACD Line
            //MACD Histogram: MACD Line - Signal Line
            var macd = [],
                normalizedEmaFast = [],
                normalizedEmaSlow = [],
                normalizedMacdLine = [],
                emaFast = [],
                emaSlow = [],
                macdLine = [],
                signal = [],
                macdHistogram = [],
                emaFast = this.ema(data, fastPeriod), // count 47
                emaSlow = this.ema(data, slowPeriod), // count 33
                normalizedEmaFast = emaFast.slice(slowPeriod-fastPeriod, emaFast.length);

            for (var i = 0; i < normalizedEmaFast.length; i++) {
                macdLine.push(normalizedEmaFast[i] - emaSlow[i]);
            }
            signal = this.ema(macdLine, signalPeriod);
            normalizedMacdLine = macdLine.slice(signalPeriod-1, macdLine.length);
            for (var i = 0; i < normalizedMacdLine.length; i++) {
                macdHistogram.push(normalizedMacdLine[i] - signal[i]);
            }
            macd.push(macdHistogram, normalizedMacdLine, signal);
            return macd;
        },
        //<TICKER>,<PER>,<DATE>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>
        isFirstUptrend: function(data, checkTrendPointCount) {
            var upper = 0,
                lower = 0;
            for (var i = 0; i < checkTrendPointCount; i++) {
                if (data[i].close > data[i + 1].close) {
                    lower++;
                } else {
                    upper++;
                }
            }
            return upper > lower;
        },

        psar: function(data) {
            //console.log(data);
            var defaultInitAcceleration = 0.02,

            // Max value of acceleration
                defaultMaxAcceleration = 0.2,

            // Number of points to check before deside trend direction
                checkTrendPointCount = 5,

                initAcceleration = defaultInitAcceleration,
                uptrend = this.isFirstUptrend(data, checkTrendPointCount),
                prevSar = uptrend ? data[0].low : data[0].high,
                accelaration = initAcceleration,
                extremum = uptrend ? data[0].high : data[0].low,
                result = [];

            for (var i = 0; i < data.length; i++) {

                // extremum = highest period High for uptrend
                if (uptrend && (extremum < data[i].high)) {
                    extremum = data[i].high;
                    accelaration = _.min([defaultMaxAcceleration, accelaration + initAcceleration]);
                }

                // extremum = lowest period Low for downtrend
                else if (!uptrend && (extremum > data[i].low)) {
                    extremum = data[i].low;
                    accelaration = _.min([defaultMaxAcceleration, accelaration + initAcceleration]);
                }

                var probableSar = prevSar + accelaration * (extremum - prevSar);

                // trend switch
                if (data[i].low < probableSar && probableSar < data[i].high) {
                    uptrend = !uptrend;
                    accelaration = initAcceleration;
                    probableSar = extremum;
                    extremum = uptrend ? data[i].high : data[i].low;
                }

                result.push(probableSar);
                prevSar = probableSar;
            }
            return result;
        },


        // REFACTOR !!!
        calcBuyingPressure: function(data) {
            var buyPressure = [],
                prevCloseLowArr = [];
            for (var i = 1; i < data.length; i++) {
                prevCloseLowArr.push(data[i - 1].close);
                prevCloseLowArr.push(data[i].low);
                buyPressure.push(data[i].close - _.min(prevCloseLowArr));
                prevCloseLowArr = [];
            }
            return buyPressure;
        },

        atr: function(data) {
            var prevCloseHighArr = [],
                prevCloseLowArr = [],
                trueRange = [];

            for (var i = 1; i < data.length; i++) {
                prevCloseHighArr.push(data[i - 1].close);
                prevCloseHighArr.push(data[i].high);
                prevCloseLowArr.push(data[i - 1].close);
                prevCloseLowArr.push(data[i].low);
                trueRange.push(_.max(prevCloseHighArr) - _.min(prevCloseLowArr));
                prevCloseHighArr = [];
                prevCloseLowArr = [];
            }
            return trueRange;
        },

        avgForPeriod: function(data, period) {
            var startPoint = 0;
            var prevBuyPressure = [];
            var prevTrueRange = [];
            var buyingPressure = this.calcBuyingPressure(data);
            var trueRange = this.calcTrueRange(data);
            var avgForPeriod = [];

            for (var i = period; i < data.length; i++) {
                prevBuyPressure = buyingPressure.slice(startPoint, i);
                prevTrueRange = trueRange.slice(startPoint, i);
                var currSum = 0;
                var buyPressureSum = 0;
                var trueRangeSum = 0;
                for (var k = 0; k < prevBuyPressure.length; k++) {
                    buyPressureSum = buyPressureSum + prevBuyPressure[k];
                    trueRangeSum = trueRangeSum + prevTrueRange[k];
                }
                avgForPeriod.push(buyPressureSum / trueRangeSum);
                startPoint++;
            }
            return avgForPeriod;
        },

        ultimateOscillator: function(data) {


            var ultOsc = [],
                avgSevenPeriod = this.calcAvgForPeriod(data, 7),
                avgFourteenPeriod = this.calcAvgForPeriod(data, 14),
                avgTwentyEightPeriod = this.calcAvgForPeriod(data, 28);



            for (var i = 0; i < avgTwentyEightPeriod.length; i++) {
                ultOsc.push((100 * ( (4 * avgSevenPeriod[21 + i]) + (2 * avgFourteenPeriod[14 + i]) + avgTwentyEightPeriod[i] )) / (4 + 2 + 1));
            }

            return [ultOsc];
        },

    };

    htc.talib = statAnalysis;
    return statAnalysis;

})(htc);
  globalContext.htc = htc;
})(window);
